<div class="tab-pane <?php echo e($active_tab == 3 ? 'active' : ''); ?>" id="form_3">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Identificación Regional</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            <?php echo e(Form::open(['route' => ['update_buque_artesanal_path', $buque->id], 'method' => 'PUT'])); ?>

            <?php echo e(csrf_field()); ?>

            <input type="hidden" name="form_tab" value="3">
            <div class="box-body">

                <div class="form-group">
                    <small class="text-muted"><span class="text-danger">*</span> Campos obligatorios</small>
                </div>

                <div class="form-group <?php echo e($errors->has('eslora') ? 'has-error' : ''); ?>">
                    <label for=""><small class="text-danger">*</small> Eslora</label>
                    <?php echo e(Form::text('eslora', $buque->eslora, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('eslora')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('manga') ? 'has-error' : ''); ?>">
                    <label for=""><small class="text-danger">*</small> Manga</label>
                    <?php echo e(Form::text('manga', $buque->manga, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('manga')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('puntal') ? 'has-error' : ''); ?>">
                    <label for=""><small class="text-danger">*</small> Puntal</label>
                    <?php echo e(Form::text('puntal', $buque->puntal, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('puntal')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('calado') ? 'has-error' : ''); ?>">
                    <label for="">Calado</label>
                    <?php echo e(Form::text('calado', $buque->calado, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('calado')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('trn') ? 'has-error' : ''); ?>">
                    <label for="">Tonelaje de Registro Neto (TRN)</label>
                    <?php echo e(Form::text('trn', $buque->trn, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('trn')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('trb') ? 'has-error' : ''); ?>">
                    <label for="">Tonelaje de Registro Bruto (TRB)</label>
                    <?php echo e(Form::text('trb', $buque->trb, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('trb')); ?></span>
                </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            <?php echo e(Form::close()); ?>

        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

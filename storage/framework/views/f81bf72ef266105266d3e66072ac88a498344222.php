<div class="tab-pane <?php echo e($active_tab == 2 ? 'active' : ''); ?>" id="form_2">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Identificación Regional</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            <?php echo e(Form::open(['route' => ['update_buque_artesanal_path', $buque->id], 'method' => 'PUT'])); ?>

            <?php echo e(csrf_field()); ?>

            <input type="hidden" name="form_tab" value="2">
            <div class="box-body">
                <div class="form-group">
                    <small class="text-muted"><span class="text-danger">*</span> Campos obligatorios</small>
                </div>

                <div class="form-group <?php echo e($errors->has('bandera_id') ? 'has-error' : ''); ?>">
                    <label for="">Bandera</label>
                    <?php echo e(Form::select('bandera_id', $refs['countries'], $buque->bandera_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%'])); ?>

                    <span class="help-block"><?php echo e($errors->first('bandera_id')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('puerto_id') ? 'has-error' : ''); ?>">
                    <label for="">Puerto de Registro</label>
                    <?php echo e(Form::select('puerto_id', $refs['registration_ports'], $buque->puerto_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%'])); ?>

                    <span class="help-block"><?php echo e($errors->first('puerto_id')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('tipo_id') ? 'has-error' : ''); ?>">
                    <label for="">Tipo de embarcación</label>
                    <?php echo e(Form::select('tipo_id', $refs['tipo_embarcacion'], $buque->tipo_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%'])); ?>

                    <span class="help-block"><?php echo e($errors->first('tipo_id')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('estado_operacional') ? 'has-error' : ''); ?>">
                    <label for=""><small class="text-danger">*</small> Estado operacional</label>

                    <div class="radio">
                      <label> <?php echo e(Form::radio('estado_operacional', '1', (is_null($buque->estado_operacional) || $buque->estado_operacional == 1))); ?> Sí </label>
                    </div>

                    <div class="radio">
                      <label> <?php echo e(Form::radio('estado_operacional', '0', (is_numeric($buque->estado_operacional) && $buque->estado_operacional == 0))); ?> No </label>
                    </div>

                    <span class="help-block"><?php echo e($errors->first('estado_operacional')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('cuenca_id') ? 'has-error' : ''); ?>">
                    <label for=""><small class="text-danger">*</small> Cuenca donde opera</label>
                    <?php echo e(Form::select('cuenca_id', $refs['cuencas'], $buque->cuenca_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%'])); ?>

                    <span class="help-block"><?php echo e($errors->first('cuenca_id')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('departamento_id') ? 'has-error' : ''); ?>">
                    <label for=""><small class="text-danger">*</small> Departamento donde opera</label>
                    <?php echo e(Form::select('departamento_id', $refs['colombia_departamentos'], $buque->departamento_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%'])); ?>

                    <span class="help-block"><?php echo e($errors->first('departamento_id')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('municipio_id') ? 'has-error' : ''); ?>">
                    <label for=""><small class="text-danger">*</small> Municipio donde opera</label>
                    <?php echo e(Form::select('municipio_id', $refs['colombia_municipios'], $buque->municipio_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%'])); ?>

                    <span class="help-block"><?php echo e($errors->first('municipio_id')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('localidad') ? 'has-error' : ''); ?>">
                    <label for=""><small class="text-danger">*</small> Localidad donde opera</label>
                    <?php echo e(Form::text('localidad', $buque->localidad, ['class' => 'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('localidad')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('lugar_desembarco') ? 'has-error' : ''); ?>">
                    <label for=""><small class="text-danger">*</small> Lugar de desembarco</label>
                    <?php echo e(Form::text('lugar_desembarco', $buque->lugar_desembarco, ['class' => 'form-control'])); ?>

                    <!--<?php echo e(Form::select('lugar_desembarco_id', $refs['X'], $buque->lugar_desembarco_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%'])); ?>-->
                    <span class="help-block"><?php echo e($errors->first('lugar_desembarco')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('permiso_pesca') ? 'has-error' : ''); ?>">
                    <label for=""><small class="text-danger">*</small> Permiso de pesca</label>

                    <div class="radio">
                      <label> <?php echo e(Form::radio('permiso_pesca', '1', (is_null($buque->permiso_pesca) || $buque->permiso_pesca == 1))); ?> Sí </label>
                    </div>

                    <div class="radio">
                      <label> <?php echo e(Form::radio('permiso_pesca', '0', (is_numeric($buque->permiso_pesca) && $buque->permiso_pesca == 0))); ?> No </label>
                    </div>

                    <span class="help-block"><?php echo e($errors->first('permiso_pesca')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('numero_resolucion') ? 'has-error' : ''); ?>">
                    <label for="">Número de Resolución</label>
                    <?php echo e(Form::text('numero_resolucion', $buque->numero_resolucion, ['class' => 'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('numero_resolucion')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('vigencia_permiso') ? 'has-error' : ''); ?>">
                    <label for="">Vigencia del permiso (años)</label>
                    <?php echo e(Form::text('vigencia_permiso', $buque->vigencia_permiso, ['class' => 'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('vigencia_permiso')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('fecha_inicio') ? 'has-error' : ''); ?>">
                    <label for="">Fecha de Inicio</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <?php echo e(Form::text('fecha_inicio', $buque->fecha_inicio, ['class' => 'form-control pull-right datepicker', 'readonly' => 'true'])); ?>

                    </div>
                    <span class="help-block"><?php echo e($errors->first('fecha_inicio')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('fecha_finalizacion') ? 'has-error' : ''); ?>">
                    <label for="">Fecha de finalización</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <?php echo e(Form::text('fecha_finalizacion', $buque->fecha_finalizacion, ['class' => 'form-control pull-right datepicker', 'readonly' => 'true'])); ?>

                    </div>
                    <span class="help-block"><?php echo e($errors->first('fecha_finalizacion')); ?></span>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            <?php echo e(Form::close()); ?>

        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

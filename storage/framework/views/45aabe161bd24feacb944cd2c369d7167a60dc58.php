<?php 
    $request_tab = app('request')->input('tab');
    $active_tab =  is_null(old('form_tab', null)) ? (!is_null($request_tab) ? ($request_tab > 9 ? 1 : $request_tab) : 1) : old('form_tab');
 ?>
<div class="row">
    <div class="col-md-3">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs" style="padding-left: 0; padding-right:0" id="tabs-left">
                <li class="<?php echo e($active_tab == 1 ? 'active' : ''); ?> col-xs-12"><a href="#form_1" data-toggle="tab">1. Identificación</a></li>
                <?php if($buque->exists): ?>
                    <li class="<?php echo e($active_tab == 2 ? 'active' : ''); ?> col-xs-12"><a href="#form_2" data-toggle="tab">2. Registro</a></li>
                    <li class="<?php echo e($active_tab == 3 ? 'active' : ''); ?> col-xs-12"><a href="#form_3" data-toggle="tab">3. Dimensiones</a></li>
                    <li class="<?php echo e($active_tab == 4 ? 'active' : ''); ?> col-xs-12"><a href="#form_4" data-toggle="tab">4. Construcción</a></li>
                    <li class="<?php echo e($active_tab == 5 ? 'active' : ''); ?> col-xs-12"><a href="#form_5" data-toggle="tab">5. Propulsión</a></li>
                    <li class="<?php echo e($active_tab == 6 ? 'active' : ''); ?> col-xs-12"><a href="#form_6" data-toggle="tab">6. Propietario y tripulación</a></li>
                    <li class="<?php echo e($active_tab == 7 ? 'active' : ''); ?> col-xs-12"><a href="#form_7" data-toggle="tab">7. Equipos de navegación y comunicación</a></li>
                    <li class="<?php echo e($active_tab == 8 ? 'active' : ''); ?> col-xs-12"><a href="#form_8" data-toggle="tab">8. Artes y/o métodos de pesca autorizados</a></li>
                    <li class="<?php echo e($active_tab == 9 ? 'active' : ''); ?> col-xs-12"><a href="#form_9" data-toggle="tab">9. Fotografías Embarcación</a></li>
                <?php endif; ?>
            </ul>
        </div>
    </div>

    <div class="col-md-9">

            <div class="tab-content">
                <?php echo $__env->make('buques_artesanales.forms.partial_forms.form_1', ['buque' => $buque, 'refs' =>$refs], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php if($buque->exists): ?>
                    <?php echo $__env->make('buques_artesanales.forms.partial_forms.form_2', ['buque' => $buque, 'refs' => $refs], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php echo $__env->make('buques_artesanales.forms.partial_forms.form_3', ['buque' => $buque, 'refs' => $refs], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php echo $__env->make('buques_artesanales.forms.partial_forms.form_4', ['buque' => $buque, 'refs' => $refs], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php echo $__env->make('buques_artesanales.forms.partial_forms.form_5', ['buque' => $buque, 'refs' => $refs], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php echo $__env->make('buques_artesanales.forms.partial_forms.form_6', ['buque' => $buque, 'refs' => $refs], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php echo $__env->make('buques_artesanales.forms.partial_forms.form_7', ['buque' => $buque, 'refs' => $refs], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php echo $__env->make('buques_artesanales.forms.partial_forms.form_8', ['buque' => $buque, 'refs' => $refs], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php echo $__env->make('buques_artesanales.forms.partial_forms.form_9', ['buque' => $buque, 'refs' => $refs], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php endif; ?>
            </div>
            <!-- .tab-content -->
    </div>
    <!-- .div-col-md-9-->
</div>

<?php $__env->startSection('plugins_css'); ?>
    <?php echo e(Html::style('plugins/datepicker/datepicker3.css')); ?>

    <?php echo e(Html::style('plugins/select2/select2.min.css')); ?>

    <style media="screen">
        .nav-tabs-custom>.nav-tabs>li.active>a, .nav-tabs-custom>.nav-tabs>li.active:hover>a
        {
            font-weight: bold;
            color: #3c8dbc !important;
            border: none !important;
        }
        span[id$="-files-help-block"]
        {
            color: #a94442 !important;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('plugins_js'); ?>
    <?php echo e(Html::script('plugins/select2/select2.min.js')); ?>

    <?php echo e(Html::script('plugins/datepicker/bootstrap-datepicker.js')); ?>

    <?php echo e(Html::script('plugins/datepicker/locales/bootstrap-datepicker.es.js')); ?>

    <script type="text/javascript">
        $(document).ready(inicio);

        function inicio()
        {
            $('.datepicker').datepicker({
                language: 'es',
                format: 'yyyy-mm-dd',
                autoclose: true
            });

            $('.datepicker-years').datepicker( {
                language: 'es',
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years",
                autoclose: true
            });

            $("select").select2();

            $("span.delete-buques-artesanales-img").click(deleteImg);

            if(window.FileReader)
            {
                $("#buque-artesanal-artes-files").change(function(e){
                    verifyFiles(e, $("#submit-f8"), $("#buque-artesanal-artes-files-help-block"), $("#f8-total-files"));
                });

                $("#buque-artesanal-files").change(function(e){
                    console.log("AQUI");
                    verifyFiles(e, $("#submit-f9"), $("#buque-artesanal-files-help-block"), $("#f9-total-files"));
                });
            }
        }

        function verifyFiles(e, buttonSubmit, helpBlock, spanCount)
        {
            var available_tipes = ['jpg', 'jpeg', 'png'];
            var max_size = 2097152;

            var files = e.originalEvent.target.files;
            var submit_disabled = false;
            var message = '';

            if(files.length > 0)
            {
                $(buttonSubmit).prop('disabled', true);

                var regex_extension = /(?:\.([^.]+))?$/;

                for(var i=0, len=files.length; i<len; i++)
                {
                    var n = files[i].name, s = files[i].size, t = files[i].type;

                    var ext = regex_extension.exec(n)[1];

                    if(typeof ext === 'undefined' || available_tipes.indexOf(ext.toLowerCase()) === -1 || s > max_size)
                    {
                        submit_disabled = true;
                        message = 'El archivo: "' + n + '" no cumple con los requisitos, intente quitarlo o cambiarlo';
                        break;
                    }
                }
            }

            $(spanCount).text(files.length);
            $(buttonSubmit).prop('disabled', submit_disabled);
            $(helpBlock).text(message);
        }

        function deleteImg()
        {
            var id = $(this).attr("data-id");
            var r = confirm("¿Eliminar la imagen?");

            if(r)
            {
                var datos = {id : id};

                var ajax_url = "<?php echo e(url('/buques_artesanales/')); ?>"+'/';
                ajax_url += parseInt($(this).attr('data-form')) == 8 ? 'delete_image_arte/' : 'delete_image/';
                ajax_url += id;

                var container = parseInt($(this).attr('data-form')) == 8 ? 'buque-artesanal-arte-img-container' : 'buque-artesanal-img-container';

                console.log(id, ajax_url);

                $.ajax({
                   url: ajax_url,
                   type: 'get',
                   dataType: 'json',
                   data: datos,
                   beforeSend: function(){},
                   success: function(response){

                       if(response.success)
                       {
                           $("div."+ container +"[data-id='"+id+"']").remove();
                           alert("Imagen eliminada correctamente");
                       }
                       else
                       {
                           alert("No se puede eliminar la imagen por el momento");
                       }
                   },
                   error: function(request, error, status){
                       console.log("AJAX Error: ");
                       console.log($(request.responseText).text());
                    }
                });
            }
        }

    </script>
<?php $__env->stopSection(); ?>

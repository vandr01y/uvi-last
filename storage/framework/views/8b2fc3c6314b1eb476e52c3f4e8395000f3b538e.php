<div class="tab-pane" id="form_17">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Artes y/o métodos de pesca autorizadas</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            <div class="box-body">

                <div class="form-group">
                    <label for="">Denominación del arte de pesca tipo red</label>
                    <?php echo e(Form::select('f17_denominacion_arte_pesca_tipo_red_id', $refs['denominacion_arte_pesca_tipo_redes'], ($vessel->exists ? $vessel->v_artesmetodospescaautorizados->denominacion_arte_pesca_tipo_red_id : null), ['class'=>'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>

                <div class="form-group <?php echo e($errors->has('f17_ojo_malla_pulgadas') ? 'has-error' : ''); ?>">
                    <label for="">Ojo de malla (pulgadas)</label>
                    <?php echo e(Form::text('f17_ojo_malla_pulgadas', ($vessel->exists ? $vessel->v_artesmetodospescaautorizados->ojo_malla_pulgadas : ''), ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('f17_ojo_malla_pulgadas')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('f17_ojo_malla_del_copo_pulgadas') ? 'has-error' : ''); ?>">
                    <label for="">Ojo de malla del copo (pulgadas)</label>
                    <?php echo e(Form::text('f17_ojo_malla_del_copo_pulgadas', ($vessel->exists ? $vessel->v_artesmetodospescaautorizados->ojo_malla_del_copo_pulgadas : ''), ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('f17_ojo_malla_del_copo_pulgadas')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('f17_longitud_red_brazas') ? 'has-error' : ''); ?>">
                    <label for="">Longitud de la red (Brazas)</label>
                    <?php echo e(Form::text('f17_longitud_red_brazas', ($vessel->exists ? $vessel->v_artesmetodospescaautorizados->longitud_red_brazas : ''), ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('f17_longitud_red_brazas')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('f17_longitud_relinga_superior_pies') ? 'has-error' : ''); ?>">
                    <label for="">Longitud de la relinga superior (Redes arrastre) en pies</label>
                    <?php echo e(Form::text('f17_longitud_relinga_superior_pies', ($vessel->exists ? $vessel->v_artesmetodospescaautorizados->longitud_relinga_superior_pies : ''), ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('f17_longitud_relinga_superior_pies')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('f17_ancho_red_brazas') ? 'has-error' : ''); ?>">
                    <label for="">Ancho de la red (Brazas)</label>
                    <?php echo e(Form::text('f17_ancho_red_brazas', ($vessel->exists ? $vessel->v_artesmetodospescaautorizados->ancho_red_brazas : ''), ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('f17_ancho_red_brazas')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('f17_cantidad_panos_piezas') ? 'has-error' : ''); ?>">
                    <label for="">Cantidad de paños o piezas</label>
                    <?php echo e(Form::text('f17_cantidad_panos_piezas', ($vessel->exists ? $vessel->v_artesmetodospescaautorizados->cantidad_panos_piezas : ''), ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('f17_cantidad_panos_piezas')); ?></span>
                </div>

                <div class="form-group">
                    <label for="">Material de la red</label>
                    <?php echo e(Form::select('f17_material_la_red_id', $refs['materiales_redes'], ($vessel->exists ? $vessel->v_artesmetodospescaautorizados->material_la_red_id : null), ['class'=>'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>

                <div class="form-group">
                    <label for="">Denominación del arte de pesca tipo Sedal ó de anzuelo</label>
                    <?php echo e(Form::select('f17_denominacion_arte_pesca_tipo_sedal_anzuelo_id', $refs['denominacion_arte_pesca_tipo_sedal_anzuelos'], ($vessel->exists ? $vessel->v_artesmetodospescaautorizados->denominacion_arte_pesca_tipo_sedal_anzuelo_id : null), ['class'=>'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>

                <div class="form-group">
                    <label for="">Tipo de anzuelo</label>
                    <?php echo e(Form::select('f17_tipo_anzuelo_id', $refs['tipos_anzuelos'], ($vessel->exists ? $vessel->v_artesmetodospescaautorizados->tipo_anzuelo_id : null), ['class'=>'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>

                <div class="form-group <?php echo e($errors->has('f17_tamanio_tipo_anzuelo') ? 'has-error' : ''); ?>">
                    <label for="">Tamaño y tipo de anzuelo</label>
                    <?php echo e(Form::text('f17_tamanio_tipo_anzuelo', ($vessel->exists ? $vessel->v_artesmetodospescaautorizados->tamanio_tipo_anzuelo : ''), ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('f17_tamanio_tipo_anzuelo')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('f17_cantidad_anzuelos') ? 'has-error' : ''); ?>">
                    <label for="">Cantidad de anzuelos</label>
                    <?php echo e(Form::text('f17_cantidad_anzuelos', ($vessel->exists ? $vessel->v_artesmetodospescaautorizados->cantidad_anzuelos : ''), ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('f17_cantidad_anzuelos')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('f17_longitud_linea_madre') ? 'has-error' : ''); ?>">
                    <label for="">Longitud de la línea madre</label>
                    <?php echo e(Form::text('f17_longitud_linea_madre', ($vessel->exists ? $vessel->v_artesmetodospescaautorizados->longitud_linea_madre : ''), ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('f17_longitud_linea_madre')); ?></span>
                </div>

                <div class="form-group">
                    <label for="">Material de la línea madre</label>
                    <?php echo e(Form::select('f17_material_linea_madre_id', $refs['materiales_linea_madre'], ($vessel->exists ? $vessel->v_artesmetodospescaautorizados->material_linea_madre_id : null), ['class'=>'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>

                <div class="form-group">
                    <label for="">Material de las bajantes</label>
                    <?php echo e(Form::select('f17_material_bajantes_id', $refs['materiales_bajantes'], ($vessel->exists ? $vessel->v_artesmetodospescaautorizados->material_bajantes_id : null), ['class'=>'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>

                <div class="form-group <?php echo e($errors->has('f17_cantidad_total_lineas') ? 'has-error' : ''); ?>">
                    <label for="">Cantidad total de líneas</label>
                    <?php echo e(Form::text('f17_cantidad_total_lineas', ($vessel->exists ? $vessel->v_artesmetodospescaautorizados->cantidad_total_lineas : ''), ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('f17_cantidad_total_lineas')); ?></span>
                </div>

                <div class="form-group">
                    <label for="">Denominación del arte de pesca tipo Trampas o nasas</label>
                    <?php echo e(Form::select('f17_denominacion_arte_pesca_tipo_trampas_nasas_id', $refs['denominacion_arte_pesca_tipo_trampas_nasas'], ($vessel->exists ? $vessel->v_artesmetodospescaautorizados->denominacion_arte_pesca_tipo_trampas_nasas_id : null), ['class'=>'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>

                <div class="form-group <?php echo e($errors->has('f17_cantidad_trampas_nasas') ? 'has-error' : ''); ?>">
                    <label for="">Cantidad de trampas o nasas</label>
                    <?php echo e(Form::text('f17_cantidad_trampas_nasas', ($vessel->exists ? $vessel->v_artesmetodospescaautorizados->cantidad_trampas_nasas : ''), ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('f17_cantidad_trampas_nasas')); ?></span>
                </div>

                <div class="form-group">
                    <label for="">Material principal de la trampa o nasa</label>
                    <?php echo e(Form::select('f17_material_principal_trampa_nasa_id', $refs['materiales_principales_trampas_nasas'], ($vessel->exists ? $vessel->v_artesmetodospescaautorizados->material_principal_trampa_nasa_id : null), ['class'=>'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>

                <div class="form-group">
                    <label for="">Tipo de artefactos de herir o aferrar</label>
                    <?php echo e(Form::select('f17_tipo_artefactos_herir_aferrar_id', $refs['tipos_artefactos_herir_aferrar'], ($vessel->exists ? $vessel->v_artesmetodospescaautorizados->tipo_artefactos_herir_aferrar_id : null), ['class'=>'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>

                <div class="form-group <?php echo e($errors->has('f17_cantidad_artefactos') ? 'has-error' : ''); ?>">
                    <label for="">Cantidad de artefactos</label>
                    <?php echo e(Form::text('f17_cantidad_artefactos', ($vessel->exists ? $vessel->v_artesmetodospescaautorizados->cantidad_artefactos : ''), ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('f17_cantidad_artefactos')); ?></span>
                </div>

                <div class="form-group">
                    <label for="">Material del artefacto</label>
                    <?php echo e(Form::select('f17_material_artefacto_id', $refs['materiales_artefactos'], ($vessel->exists ? $vessel->v_artesmetodospescaautorizados->material_artefacto_id : null), ['class'=>'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>

                <div class="form-group">
                    <label for="">Otros</label>
                    <?php echo e(Form::text('f17_otros', ($vessel->exists ? $vessel->v_artesmetodospescaautorizados->otros : ''), ['class'=>'form-control'])); ?>

                </div>

                <div class="form-group">
                    <label for="">Uso de Dispositivos Agregadores de Peces - FAD (Fish Aggregating Device)</label>

                    <div class="radio">
                      <label> <?php echo e(Form::radio('f17_fad', '1', (!$vessel->exists || ($vessel->exists && $vessel->v_artesmetodospescaautorizados->fad == 1)))); ?> Activo </label>
                    </div>

                    <div class="radio">
                      <label> <?php echo e(Form::radio('f17_fad', '0', ($vessel->exists && $vessel->v_artesmetodospescaautorizados->fad == 0))); ?> Inactivo </label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="">Tipo de FAD utilizados</label>
                    <?php echo e(Form::select('f17_tipo_fad_utilizados_id', $refs['tipos_fad'], ($vessel->exists ? $vessel->v_artesmetodospescaautorizados->tipo_fad_utilizados_id : null), ['class'=>'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>

                <div class="form-group <?php echo e($errors->has('f17_cantidad_fad_utilizados') ? 'has-error' : ''); ?>">
                    <label for="">Cantidad de FAD utilizados</label>
                    <?php echo e(Form::text('f17_cantidad_fad_utilizados', ($vessel->exists ? $vessel->v_artesmetodospescaautorizados->cantidad_fad_utilizados : ''), ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('f17_cantidad_fad_utilizados')); ?></span>
                </div>

                <div class="form-group">
                    <label for="">Componentes del FAD</label>
                    <?php echo e(Form::textarea('f17_componentes_fad', ($vessel->exists ? $vessel->v_artesmetodospescaautorizados->componentes_fad : ''), ['class'=>'form-control', 'rows' => '3'])); ?>

                </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <a href="#form_18" class="btn btn-success a-change-tab" data-toggle="tab">Siguiente <i class=""></i></a>
            </div>
        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

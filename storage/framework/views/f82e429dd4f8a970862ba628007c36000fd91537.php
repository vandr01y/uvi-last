<div class="tab-pane <?php echo e($active_tab == 7 ? 'active' : ''); ?>" id="form_7">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Equipos de navegación y comunicación</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            <?php echo e(Form::open(['route' => ['update_buque_artesanal_path', $buque->id], 'method' => 'PUT'])); ?>

            <?php echo e(csrf_field()); ?>

            <input type="hidden" name="form_tab" value="7">
            <div class="box-body">

                <div class="form-group">
                    <small class="text-muted"><span class="text-danger">*</span> Campos obligatorios</small>
                </div>

                <div class="form-group <?php echo e($errors->has('radio_comunicaciones') ? 'has-error' : ''); ?>">
                    <label for=""><small class="text-danger">*</small> Radio de comunicaciones</label>
                    <div class="radio">
                      <label> <?php echo e(Form::radio('radio_comunicaciones', '1', (is_null($buque->radio_comunicaciones) || $buque->radio_comunicaciones == 1))); ?> Sí </label>
                    </div>

                    <div class="radio">
                      <label> <?php echo e(Form::radio('radio_comunicaciones', '0', (is_numeric($buque->radio_comunicaciones) && $buque->radio_comunicaciones == 0))); ?> No </label>
                    </div>
                    <span class="help-block"><?php echo e($errors->first('radio_comunicaciones')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('marca_radio_comunicaciones') ? 'has-error' : ''); ?>">
                    <label for="">Marca radio de comunicaciones</label>
                    <?php echo e(Form::text('marca_radio_comunicaciones', $buque->marca_radio_comunicaciones, ['class' => 'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('marca_radio_comunicaciones')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('frecuencia_asignada') ? 'has-error' : ''); ?>">
                    <label for="">Especificar la frecuencia</label>
                    <?php echo e(Form::text('frecuencia_asignada', $buque->frecuencia_asignada, ['class' => 'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('frecuencia_asignada')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('brujula') ? 'has-error' : ''); ?>">
                    <label for="">Brújula</label>
                    <div class="radio">
                      <label> <?php echo e(Form::radio('brujula', '1', (is_null($buque->brujula) || $buque->brujula == 1))); ?> Sí </label>
                    </div>

                    <div class="radio">
                      <label> <?php echo e(Form::radio('brujula', '0', (is_numeric($buque->brujula) && $buque->brujula == 0))); ?> No </label>
                    </div>
                    <span class="help-block"><?php echo e($errors->first('brujula')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('marca_brujula') ? 'has-error' : ''); ?>">
                    <label for="">Marca de la brújjula</label>
                    <?php echo e(Form::text('marca_brujula', $buque->marca_brujula, ['class' => 'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('marca_brujula')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('gps') ? 'has-error' : ''); ?>">
                    <label for="">GPS</label>
                    <div class="radio">
                      <label> <?php echo e(Form::radio('gps', '1', (is_null($buque->gps) || $buque->gps == 1))); ?> Sí </label>
                    </div>

                    <div class="radio">
                      <label> <?php echo e(Form::radio('gps', '0', (is_numeric($buque->gps) && $buque->gps == 0))); ?> No </label>
                    </div>
                    <span class="help-block"><?php echo e($errors->first('gps')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('marca_gps') ? 'has-error' : ''); ?>">
                    <label for="">Marca del GPS</label>
                    <?php echo e(Form::text('marca_gps', $buque->marca_gps, ['class' => 'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('marca_gps')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('chalecos_salvavidas') ? 'has-error' : ''); ?>">
                    <label for=""><small class="text-danger">*</small> Chalecos salvavidas</label>
                    <div class="radio">
                      <label> <?php echo e(Form::radio('chalecos_salvavidas', '1', (is_null($buque->chalecos_salvavidas) || $buque->chalecos_salvavidas == 1))); ?> Sí </label>
                    </div>

                    <div class="radio">
                      <label> <?php echo e(Form::radio('chalecos_salvavidas', '0', (is_numeric($buque->chalecos_salvavidas) && $buque->chalecos_salvavidas == 0))); ?> No </label>
                    </div>
                    <span class="help-block"><?php echo e($errors->first('chalecos_salvavidas')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('numero_chalecos_salvavidas') ? 'has-error' : ''); ?>">
                    <label for="">Número de chalecos salvavidas</label>
                    <?php echo e(Form::text('numero_chalecos_salvavidas', $buque->numero_chalecos_salvavidas, ['class' => 'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('numero_chalecos_salvavidas')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('estado_condicion_chalecos_salvavidas_id') ? 'has-error' : ''); ?>">
                    <label for="">Estado de la condición de los chalecos salvavidas</label>
                    <?php echo e(Form::select('estado_condicion_chalecos_salvavidas_id', $refs['chalecos_salvavidas_estados'], $buque->estado_condicion_chalecos_salvavidas_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%'])); ?>

                    <span class="help-block"><?php echo e($errors->first('estado_condicion_chalecos_salvavidas_id')); ?></span>
                </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            <?php echo e(Form::close()); ?>

        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

    <?php $__env->startSection('page_header'); ?>
        <?php echo $__env->make('common.page_header', ['page_header' => 'Usuarios'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php $__env->stopSection(); ?>

    <?php $__env->startSection('content'); ?>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Listado de usuarios</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered table-hover" id="users-table">
                            <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Usuario</th>
                                    <th>E-mail</th>
                                    <th>Rol</th>
                                    <th>Status</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <tr>
                                        <td><?php echo e($user->id); ?></td>
                                        <td><?php echo e($user->name); ?></td>
                                        <td><?php echo e($user->email); ?></td>
                                        <td><?php echo e($user->rol->name); ?></td>
                                        <td><?php echo e($user->status == 1 ? 'Activo' : 'Inactivo'); ?></td>
                                        <td>
                                            <a href="<?php echo e(route('edit_user_path', ['user' => $user->id])); ?>" class="btn btn-xs btn-info">Editar</a>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                            </tbody>
                        </table>
                        <?php echo e($users->render()); ?>

                    </div>
                    <!-- /.box-body -->
                </div>
                  <!-- /.box -->
            </div>
        </div>
    <?php $__env->stopSection(); ?>

    <?php $__env->startSection('plugins_css'); ?>
        <?php echo e(Html::style('plugins/datatables/dataTables.bootstrap.css')); ?>

    <?php $__env->stopSection(); ?>
    <?php $__env->startSection('plugins_js'); ?>
        <?php echo e(Html::script('plugins/datatables/jquery.dataTables.min.js')); ?>

        <?php echo e(Html::script('plugins/datatables/dataTables.bootstrap.min.js')); ?>

        <script type="text/javascript">
            $(document).ready(inicio);

            function inicio()
            {
                var last_column = $("#users-table > thead > tr > th").length - 1;

                $("#users-table").DataTable({
                    paging: false,
                    lengthChange: false,
                    searching: true,
                    ordering: true,
                    info: true,
                    autoWidth: false,
                    //columnDefs: [], // Buscar por el atributo alt en las imagenes
                    aoColumnDefs: [{ "bSortable" : false, "aTargets" : [ last_column ] }],
                    language:{
                        emptyTable: "No hay información disponible",
                        info: "Mostrando _START_ a _END_ de _TOTAL_ registros",
                        infoEmpty: "No hay coincidencias",
                        infoFiltered: "(Filtrando de _MAX_ registros totales)",
                        lengthMenu: "Mostrar _MENU_ registros",
                        loadingRecords: "Cargando...",
                        processing: "Procesando...",
                        search: "Buscar:",
                        zeroRecords: "No se encontraron registros",
                        paginate: {
                            first: "Inicio",
                            last: "Final",
                            next: "Siguiente",
                            previous: "Anterior"
                        }
                    }
                });
            }
        </script>
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
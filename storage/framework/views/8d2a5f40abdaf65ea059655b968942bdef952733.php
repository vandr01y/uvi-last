<div class="tab-pane <?php echo e($active_tab == 1 ? 'active' : ''); ?>" id="form_1">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Identificación</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            <?php if($buque->exists): ?>
                <?php echo e(Form::open(['route' => ['update_buque_artesanal_path', $buque->id], 'method' => 'PUT'])); ?>

            <?php else: ?>
                <?php echo e(Form::open(['route' => 'store_buque_artesanal_path', 'method' => 'POST'])); ?>

            <?php endif; ?>
                <?php echo e(csrf_field()); ?>

            <input type="hidden" name="form_tab" value="1">
            <div class="box-body">
                <div class="form-group">
                    <small class="text-muted"><span class="text-danger">*</span> Campos obligatorios</small>
                </div>

                <div class="form-group <?php echo e($errors->has('nombre') ? 'has-error' : ''); ?>">
                    <label for=""><small class="text-danger">*</small> Nombre de la embarcación</label>
                    <?php echo e(Form::text('nombre', $buque->nombre, ['class' => 'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('nombre')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('matricula') ? 'has-error' : ''); ?>">
                    <label for="">Matrícula de la embarcación</label>
                    <?php echo e(Form::text('matricula', $buque->matricula, ['class' => 'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('matricula')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('numero_identificacion_embarcacion') ? 'has-error' : ''); ?>">
                    <label for="">Número de identificación de la embarcación</label>
                    <?php echo e(Form::text('numero_identificacion_embarcacion', $buque->numero_identificacion_embarcacion, ['class' => 'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('numero_identificacion_embarcacion')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('status') ? 'has-error' : ''); ?>">
                    <label for="">Status</label>
                    <div class="radio">
                      <label> <?php echo e(Form::radio('status', '1', (is_null($buque->status) || $buque->status == 1))); ?> Activo </label>
                    </div>

                    <div class="radio">
                      <label> <?php echo e(Form::radio('status', '0', (is_numeric($buque->status) && $buque->status == 0))); ?> Inactivo </label>
                    </div>
                    <span class="help-block <?php echo e($errors->first('status')); ?>"></span>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            <?php echo e(Form::close()); ?>

        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

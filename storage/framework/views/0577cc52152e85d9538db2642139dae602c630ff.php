<div class="tab-pane" id="form_16">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Características del permiso de pesca.</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            <div class="box-body">

                <div class="form-group">
                    <label for="">¿Tiene permiso de pesca vigente?</label>

                    <div class="radio">
                      <label> <?php echo e(Form::radio('f16_permiso_pesca_vigente', '1', (!$vessel->exists || ($vessel->exists && $vessel->permiso_pesca_vigente == 1)))); ?> Sí </label>
                    </div>

                    <div class="radio">
                      <label> <?php echo e(Form::radio('f16_permiso_pesca_vigente', '0', ($vessel->exists && $vessel->permiso_pesca_vigente == 0))); ?> No </label>
                    </div>
                </div>

                <div class="form-group <?php echo e($errors->has('f16_numero_permiso_resolucion') ? 'has-error' : ''); ?>">
                    <label for="">Número del permiso ó resolución mediante la cual se otorga el permiso de pesca (No. xxx del d/m/a).</label>
                    <?php echo e(Form::text('f16_numero_permiso_resolucion', ($vessel->exists ? $vessel->v_caracteristicaspermisopesca->numero_permiso_resolucion : ''), ['class' => 'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('f16_numero_permiso_resolucion')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('f16_numero_permiso_ultima_resolucion_vigente') ? 'has-error' : ''); ?>">
                    <label for="">Número del permiso ó última resolución vigente mediante la cual se otorga el permiso de pesca (No. xxx del d/m/a).</label>
                    <?php echo e(Form::text('f16_numero_permiso_ultima_resolucion_vigente', ($vessel->exists ? $vessel->v_caracteristicaspermisopesca->numero_permiso_ultima_resolucion_vigente : ''), ['class' => 'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('f16_numero_permiso_ultima_resolucion_vigente')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('f16_anios_vigencia_ultimo_permiso_pesca_comercial') ? 'has-error' : ''); ?>">
                    <label for="">Vigencia del último permiso de pesca comercial (años)</label>
                    <?php echo e(Form::text('f16_anios_vigencia_ultimo_permiso_pesca_comercial', ($vessel->exists ? $vessel->v_caracteristicaspermisopesca->anios_vigencia_ultimo_permiso_pesca_comercial : ''), ['class' => 'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('f16_anios_vigencia_ultimo_permiso_pesca_comercial')); ?></span>
                </div>

                <div class="form-group">
                    <label for="">Fecha de inicio del último permiso de pesca otorgado a la embarcación</label>

                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <?php echo e(Form::text('f16_fecha_inicio_ultimo_permiso_pesca', ($vessel->exists ? $vessel->v_caracteristicaspermisopesca->fecha_inicio_ultimo_permiso_pesca : ''), ['class' => 'form-control pull-right datepicker', 'readonly'=>'true'])); ?>

                    </div>
                </div>

                <div class="form-group">
                    <label for="">Fecha de terminación del último permiso de pesca otorgado a la embarcación</label>

                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <?php echo e(Form::text('f16_fecha_terminacion_ultimo_permiso_pesca', ($vessel->exists ? $vessel->v_caracteristicaspermisopesca->fecha_terminacion_ultimo_permiso_pesca : ''), ['class' => 'form-control pull-right datepicker', 'readonly'=>'true'])); ?>

                    </div>
                </div>

                <div class="form-group">
                    <label for="">Fecha de inicio la patente de pesca</label>

                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <?php echo e(Form::text('f16_fecha_inicio_patente_pesca', ($vessel->exists ? $vessel->v_caracteristicaspermisopesca->fecha_inicio_patente_pesca : ''), ['class' => 'form-control pull-right datepicker', 'readonly'=>'true'])); ?>

                    </div>
                </div>

                <div class="form-group">
                    <label for="">Fecha de terminación la patente de pesca</label>

                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <?php echo e(Form::text('f16_fecha_terminacion_patente_pesca', ($vessel->exists ? $vessel->v_caracteristicaspermisopesca->fecha_terminacion_patente_pesca : ''), ['class' => 'form-control pull-right datepicker', 'readonly'=>'true'])); ?>

                    </div>
                </div>

                <div class="form-group">
                    <label for="">Zona de pesca autorizada</label>
                    <?php echo e(Form::select('f16_zona_pesca_autorizada_id', $refs['zona_pesca_autorizadas'], ($vessel->exists ? $vessel->v_caracteristicaspermisopesca->zona_pesca_autorizada_id : null), ['class' => 'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>

                <div class="form-group">
                    <label for="">Puerto de desembarco autorizado en Colombia</label>
                    <?php echo e(Form::select('f16_colombia_puerto_desembarco_autorizado_id', $refs['colombia_puertos_desembarco_autorizados'], ($vessel->exists ? $vessel->v_caracteristicaspermisopesca->colombia_puerto_desembarco_autorizado_id : null), ['class' => 'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>

                <div class="form-group">
                    <label for="">Pesquería autorizada - Especies Objetivo</label>
                    <?php echo e(Form::select('f16_pesqueria_autorizada_id', $refs['pesqueria_autorizadas'], ($vessel->exists ? $vessel->v_caracteristicaspermisopesca->pesqueria_autorizada_id : null), ['class' => 'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <a href="#form_17" class="btn btn-success a-change-tab" data-toggle="tab">Siguiente <i class=""></i></a>
            </div>
        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

<div class="tab-pane <?php echo e($active_tab == 6 ? 'active' : ''); ?>" id="form_6">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Propietario y Tripulación</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            <?php echo e(Form::open(['route' => ['update_buque_artesanal_path', $buque->id], 'method' => 'PUT'])); ?>

            <?php echo e(csrf_field()); ?>

            <input type="hidden" name="form_tab" value="6">
            <div class="box-body">

                <div class="form-group">
                    <small class="text-muted"><span class="text-danger">*</span> Campos obligatorios</small>
                </div>

                <div class="form-group <?php echo e($errors->has('nombre_propietario') ? 'has-error' : ''); ?>">
                    <label for=""><small class="text-danger">*</small> Nombre del propietario</label>
                    <?php echo e(Form::text('nombre_propietario', $buque->nombre_propietario, ['class'=>'form-control'])); ?>

                    <span class="help-block"> <?php echo e($errors->first('nombre_propietario')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('numero_identificacion') ? 'has-error' : ''); ?>">
                    <label for=""><small class="text-danger">*</small> Número de identificación</label>
                    <?php echo e(Form::text('numero_identificacion', $buque->numero_identificacion, ['class'=>'form-control'])); ?>

                    <span class="help-block"> <?php echo e($errors->first('numero_identificacion')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('direccion_residencia') ? 'has-error' : ''); ?>">
                    <label for=""><small class="text-danger">*</small> Dirección de residencia</label>
                    <?php echo e(Form::text('direccion_residencia', $buque->direccion_residencia, ['class'=>'form-control'])); ?>

                    <span class="help-block"> <?php echo e($errors->first('direccion_residencia')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('ciudad_municipio') ? 'has-error' : ''); ?>">
                    <label for=""><small class="text-danger">*</small> Ciudad o municipio</label>
                    <?php echo e(Form::text('ciudad_municipio', $buque->ciudad_municipio, ['class'=>'form-control'])); ?>

                    <span class="help-block"> <?php echo e($errors->first('ciudad_municipio')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('telefono_fijo') ? 'has-error' : ''); ?>">
                    <label for="">Teléfono fijo</label>
                    <?php echo e(Form::text('telefono_fijo', $buque->telefono_fijo, ['class'=>'form-control'])); ?>

                    <span class="help-block"> <?php echo e($errors->first('telefono_fijo')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('telefono_celular') ? 'has-error' : ''); ?>">
                    <label for="">Teléfono de celular</label>
                    <?php echo e(Form::text('telefono_celular', $buque->telefono_celular, ['class'=>'form-control'])); ?>

                    <span class="help-block"> <?php echo e($errors->first('telefono_celular')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('carne_pesca') ? 'has-error' : ''); ?>">
                    <label for=""><small class="text-danger">*</small> Carné de pesca</label>
                    <div class="radio">
                      <label> <?php echo e(Form::radio('carne_pesca', '1', (is_null($buque->carne_pesca) || $buque->carne_pesca == 1))); ?> Sí </label>
                    </div>

                    <div class="radio">
                      <label> <?php echo e(Form::radio('carne_pesca', '0', (is_numeric($buque->carne_pesca) && $buque->carne_pesca == 0))); ?> No </label>
                    </div>
                    <span class="help-block"> <?php echo e($errors->first('carne_pesca')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('numero_carna_pesca') ? 'has-error' : ''); ?>">
                    <label for="">Número carné de pesca</label>
                    <?php echo e(Form::text('numero_carna_pesca', $buque->numero_carna_pesca, ['class'=>'form-control'])); ?>

                    <span class="help-block"> <?php echo e($errors->first('numero_carna_pesca')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('fecha_finalizacion_carne_pesca') ? 'has-error' : ''); ?>">
                    <label for="">Fecha de finalización carné de pesca propietario</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <?php echo e(Form::text('fecha_finalizacion_carne_pesca', $buque->fecha_finalizacion_carne_pesca, ['class' => 'form-control pull-right datepicker', 'readonly' => 'true'])); ?>

                    </div>
                    <span class="help-block"> <?php echo e($errors->first('fecha_finalizacion_carne_pesca')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('afiliacion') ? 'has-error' : ''); ?>">
                    <label for=""><small class="text-danger">*</small> Afiliación a organizaciones de pescadores</label>
                    <div class="radio">
                      <label> <?php echo e(Form::radio('afiliacion', '1', (is_null($buque->afiliacion) || $buque->afiliacion == 1))); ?> Sí </label>
                    </div>

                    <div class="radio">
                      <label> <?php echo e(Form::radio('afiliacion', '0', (is_numeric($buque->afiliacion) && $buque->afiliacion == 0))); ?> No </label>
                    </div>
                    <span class="help-block"> <?php echo e($errors->first('afiliacion')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('nombre_organizacion') ? 'has-error' : ''); ?>">
                    <label for="">Nombre de la organización</label>
                    <?php echo e(Form::text('nombre_organizacion', $buque->nombre_organizacion, ['class'=>'form-control'])); ?>

                    <span class="help-block"> <?php echo e($errors->first('nombre_organizacion')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('numero_tripulantes') ? 'has-error' : ''); ?>">
                    <label for=""><small class="text-danger">*</small> Número de tripulantes</label>
                    <?php echo e(Form::text('numero_tripulantes', $buque->numero_tripulantes, ['class'=>'form-control'])); ?>

                    <span class="help-block"> <?php echo e($errors->first('numero_tripulantes')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('nombre_tripulacion') ? 'has-error' : ''); ?>">
                    <label for="">Nombre de la tripulación</label>
                    <?php echo e(Form::textarea('nombre_tripulacion', $buque->nombre_tripulacion, ['class'=>'form-control', 'rows'=>3])); ?>

                    <span class="help-block"> <?php echo e($errors->first('nombre_tripulacion')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('numero_documentos_tripulacion') ? 'has-error' : ''); ?>">
                    <label for="">Número de documentos de identificación de la tripulación</label>
                    <?php echo e(Form::text('numero_documentos_tripulacion', $buque->numero_documentos_tripulacion, ['class'=>'form-control'])); ?>

                    <span class="help-block"> <?php echo e($errors->first('numero_documentos_tripulacion')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('direccion_residencia_tripulacion') ? 'has-error' : ''); ?>">
                    <label for="">Dirección de residencia de la tripulación</label>
                    <?php echo e(Form::textarea('direccion_residencia_tripulacion', $buque->direccion_residencia_tripulacion, ['class'=>'form-control', 'rows'=>3])); ?>

                    <span class="help-block"> <?php echo e($errors->first('direccion_residencia_tripulacion')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('telefono_contacto_tripulacion') ? 'has-error' : ''); ?>">
                    <label for="">Teléfono de contacto de la tripulación</label>
                    <?php echo e(Form::textarea('telefono_contacto_tripulacion', $buque->telefono_contacto_tripulacion, ['class'=>'form-control', 'rows'=>3])); ?>

                    <span class="help-block"> <?php echo e($errors->first('telefono_contacto_tripulacion')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('numero_carne_pesca_tripulacion') ? 'has-error' : ''); ?>">
                    <label for="">Número de carné de pesca de la tripulación</label>
                    <?php echo e(Form::textarea('numero_carne_pesca_tripulacion', $buque->numero_carne_pesca, ['class'=>'form-control', 'rows'=>3])); ?>

                    <span class="help-block"> <?php echo e($errors->first('numero_carne_pesca_tripulacion')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('fecha_finalizacion_carne_tripulacion') ? 'has-error' : ''); ?>">
                    <label for="">Fecha de finalización carné tripulación</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <?php echo e(Form::text('fecha_finalizacion_carne_tripulacion', $buque->fecha_finalizacion_carne_tripulacion, ['class' => 'form-control pull-right datepicker', 'readonly' => 'true'])); ?>

                    </div>
                    <span class="help-block"> <?php echo e($errors->first('fecha_finalizacion_carne_tripulacion')); ?></span>
                </div>


            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            <?php echo e(Form::close()); ?>

        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

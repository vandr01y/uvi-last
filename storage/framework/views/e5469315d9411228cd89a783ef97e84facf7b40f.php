<div class="tab-pane <?php echo e($active_tab == 8 ? 'active' : ''); ?>" id="form_8">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Artes y/o métodos de pesca autorizados</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            <?php echo e(Form::open(['route' => ['update_buque_artesanal_path', $buque->id], 'method' => 'PUT', 'files' => true])); ?>

            <?php echo e(csrf_field()); ?>

            <input type="hidden" name="form_tab" value="8">
            <div class="box-body">

                <div class="form-group">
                    <small class="text-muted"><span class="text-danger">*</span> Campos obligatorios</small>
                </div>

                <div class="form-group <?php echo e($errors->has('tipo_red_id') ? 'has-error' : ''); ?>">
                    <label for="">Denominación del arte de pesca tipo red</label>
                    <?php echo e(Form::select('tipo_red_id', $refs['arte_pesca_tipo_red'], $buque->tipo_red_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%'])); ?>

                    <span class="help-block"><?php echo e($errors->first('tipo_red_id')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('ojo_malla') ? 'has-error' : ''); ?>">
                    <label for="">Ojo de malla</label>
                    <?php echo e(Form::text('ojo_malla', $buque->ojo_malla, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('ojo_malla')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('ojo_malla_copo') ? 'has-error' : ''); ?>">
                    <label for="">Ojo de malla del copo</label>
                    <?php echo e(Form::text('ojo_malla_copo', $buque->ojo_malla_copo, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('ojo_malla_copo')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('longitud_red') ? 'has-error' : ''); ?>">
                    <label for="">Longitud de la red</label>
                    <?php echo e(Form::text('longitud_red', $buque->longitud_red, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('longitud_red')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('ancho_red') ? 'has-error' : ''); ?>">
                    <label for="">Ancho de la red</label>
                    <?php echo e(Form::text('ancho_red', $buque->ancho_red, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('ancho_red')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('cantidad_panos_piezas') ? 'has-error' : ''); ?>">
                    <label for="">Cantidad de paños o piezas</label>
                    <?php echo e(Form::text('cantidad_panos_piezas', $buque->cantidad_panos_piezas, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('cantidad_panos_piezas')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('material_red_id') ? 'has-error' : ''); ?>">
                    <label for="">Material de la red</label>
                    <?php echo e(Form::select('material_red_id', $refs['materiales_redes'], $buque->material_red_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%'])); ?>

                    <span class="help-block"><?php echo e($errors->first('material_red_id')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('zonas_pesca_frecuentadas') ? 'has-error' : ''); ?>">
                    <label for="">Zonas de pesca frecuentadas</label>
                    <?php echo e(Form::text('zonas_pesca_frecuentadas', $buque->zonas_pesca_frecuentadas, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('zonas_pesca_frecuentadas')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('tipos_lineas_mano_id') ? 'has-error' : ''); ?>">
                    <label for="">Tipos de líneas de mano y afines</label>
                    <?php echo e(Form::select('tipos_lineas_mano_id', $refs['lineas_mano'], $buque->tipos_lineas_mano_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%'])); ?>

                    <span class="help-block"><?php echo e($errors->first('tipos_lineas_mano_id')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('trampas_id') ? 'has-error' : ''); ?>">
                    <label for="">Trampas</label>
                    <?php echo e(Form::select('trampas_id', $refs['trampas_nasas'], $buque->trampas_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%'])); ?>

                    <span class="help-block"><?php echo e($errors->first('trampas_id')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('cantidad_trampas') ? 'has-error' : ''); ?>">
                    <label for="">Cantidad de trampas</label>
                    <?php echo e(Form::text('cantidad_trampas', $buque->cantidad_trampas, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('cantidad_trampas')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('material_principal_trampas_id') ? 'has-error' : ''); ?>">
                    <label for="">Material principal de la trampa</label>
                    <?php echo e(Form::select('material_principal_trampas_id', $refs['materiales_trampas_nasas'], $buque->material_principal_trampas_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%'])); ?>

                    <span class="help-block"><?php echo e($errors->first('material_principal_trampas_id')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('zonas_pesca_trampas') ? 'has-error' : ''); ?>">
                    <label for="">Zonas de pesca frecuentadas con trampas</label>
                    <?php echo e(Form::text('zonas_pesca_trampas', $buque->zonas_pesca_trampas, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('zonas_pesca_trampas')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('tipos_artefactos_herir_aferrar_id') ? 'has-error' : ''); ?>">
                    <label for="">Tipo de artefactos de herir o aferrar</label>
                    <?php echo e(Form::select('tipos_artefactos_herir_aferrar_id', $refs['tipos_artefactos'], $buque->tipos_artefactos_herir_aferrar_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%'])); ?>

                    <span class="help-block"><?php echo e($errors->first('tipos_artefactos_herir_aferrar_id')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('cantidad_artefactos') ? 'has-error' : ''); ?>">
                    <label for="">Cantidad de artefactos</label>
                    <?php echo e(Form::text('cantidad_artefactos', $buque->cantidad_artefactos, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('cantidad_artefactos')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('material_artefacto_id') ? 'has-error' : ''); ?>">
                    <label for="">Material del artefacto</label>
                    <?php echo e(Form::select('material_artefacto_id', $refs['materiales_artefactos'], $buque->material_artefacto_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%'])); ?>

                    <span class="help-block"><?php echo e($errors->first('material_artefacto_id')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('zonas_pesca_artefactos') ? 'has-error' : ''); ?>">
                    <label for="">Zonas de pesca frecuentadas con artefactos de herir o aferrar</label>
                    <?php echo e(Form::text('zonas_pesca_artefactos', $buque->zonas_pesca_artefactos, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('zonas_pesca_artefactos')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('otros') ? 'has-error' : ''); ?>">
                    <label for="">Otros</label>
                    <div class="radio">
                      <label> <?php echo e(Form::radio('otros', '1', (is_null($buque->otros) || $buque->otros == 1))); ?> Sí </label>
                    </div>

                    <div class="radio">
                      <label> <?php echo e(Form::radio('otros', '0', (is_numeric($buque->otros) && $buque->otros == 0))); ?> No </label>
                    </div>
                    <span class="help-block"><?php echo e($errors->first('otros')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('artes_metodos_poco_convencionales') ? 'has-error' : ''); ?>">
                    <label for="">Artes y/o métodos de pesca poco convencionales</label>
                    <?php echo e(Form::text('artes_metodos_poco_convencionales', $buque->artes_metodos_poco_convencionales, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('artes_metodos_poco_convencionales')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('cantidad_artes_metodos_poco_convencionales') ? 'has-error' : ''); ?>">
                    <label for="">Cantidad de artes de pesca poco convencionales</label>
                    <?php echo e(Form::text('cantidad_artes_metodos_poco_convencionales', $buque->cantidad_artes_metodos_poco_convencionales, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('cantidad_artes_metodos_poco_convencionales')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('uso_fad') ? 'has-error' : ''); ?>">
                    <label for="">Uso de Dispositivos Agregadores de Peces FAD</label>
                    <div class="radio">
                      <label> <?php echo e(Form::radio('uso_fad', '1', (is_null($buque->uso_fad) || $buque->uso_fad == 1))); ?> Sí </label>
                    </div>

                    <div class="radio">
                      <label> <?php echo e(Form::radio('uso_fad', '0', (is_numeric($buque->uso_fad) && $buque->uso_fad == 0))); ?> No </label>
                    </div>
                    <span class="help-block"><?php echo e($errors->first('uso_fad')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('tipos_fad_id') ? 'has-error' : ''); ?>">
                    <label for="">Tipo de FAD utilizados</label>
                    <?php echo e(Form::select('tipos_fad_id', $refs['tipos_fad'], $buque->tipos_fad_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%'])); ?>

                    <span class="help-block"><?php echo e($errors->first('tipos_fad_id')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('cantidad_fad') ? 'has-error' : ''); ?>">
                    <label for="">Cantidad de FAD utilizados</label>
                    <?php echo e(Form::text('cantidad_fad', $buque->cantidad_fad, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('cantidad_fad')); ?></span>
                </div>

                <div class="form-group <?php echo e(count($errors->get('photos_f8.*')) > 0 ? 'has-error' : ''); ?>">
                    <label for="">Fotografías</label>
                    <?php echo e(Form::file('photos_f8[]', ['id' => 'buque-artesanal-artes-files', 'class' => 'form-control', 'multiple' => 'true'])); ?>

                    <p class="help-block text-muted">Formatos admitidos: PNG, JPG, JPEG. Tamaño Máximo: 2MB</p>
                    <span class="help-block text-danger" id="buque-artesanal-artes-files-help-block"></span>
                    <span class="help-block">
                        <?php $__currentLoopData = $errors->get('photos_f8.*'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $messages): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                            <?php $__currentLoopData = $messages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $message): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <?php echo e($message); ?>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                    </span>
                    <?php if($buque->exists && count($buque->ba_a_pictures) > 0): ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Imágenes actuales: </label>

                                    <div class="row">
                                    <?php $__currentLoopData = $buque->ba_a_pictures; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $picture): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                        <div class="col-md-3 text-center buque-artesanal-arte-img-container" data-id='<?php echo e($picture->id); ?>'>
                                            <?php echo e(Html::image($constants['asset_artes_photos_path'] . $picture->file, 'Imagen de Arte de Pesca', ['class' => 'img-responsive img-rounded'])); ?>

                                            <span class="btn btn-xs btn-danger delete-buques-artesanales-img" data-id='<?php echo e($picture->id); ?>' data-form="8"><i class="fa fa-trash"></i></span>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success" id="submit-f8">Guardar</button><br>
              <small class="text-muted"><span id="f8-total-files">0</span> Archivo(s) Seleccionado(s)</small>
            </div>
            <?php echo e(Form::close()); ?>

        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

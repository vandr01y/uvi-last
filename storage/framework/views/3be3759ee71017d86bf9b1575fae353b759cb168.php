<div class="tab-pane <?php echo e($active_tab == 3 ? 'active' : ''); ?>" id="form_3">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Registro</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            <?php echo e(Form::open(['route' => ['update_vessel_path', $vessel->id], 'method' => 'PUT'])); ?>

            <?php echo e(csrf_field()); ?>

            <input type="hidden" name="form_tab" value="3">
            <div class="box-body">
                <div class="form-group">
                    <small class="text-muted"><span class="text-danger">*</span> Campos obligatorios</small>
                </div>

                <div class="form-group <?php echo e($errors->has('estado_actual_pabellon') ? 'has-error' : ''); ?>">
                    <label for=""><span class="text-danger">*</span> Estado actual del pabellón</label>
                    <?php echo e(Form::select('estado_actual_pabellon', $refs['countries'], $vessel->estado_actual_pabellon, ['class' => 'form-control select2', 'style' => 'width: 100%' ])); ?>

                    <span class="help-block"><?php echo e($errors->first('estado_actual_pabellon')); ?></span>
                </div>

                <div class="form-group">
                    <label for="">Estado actual del pabellón - Fecha de Registro</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <?php echo e(Form::text('estado_actual_pabellon_fecha_registro', $vessel->estado_actual_pabellon_fecha_registro, ['class' => 'form-control pull-right datepicker', 'readonly' => 'true'])); ?>

                    </div>
                </div>

                <div class="form-group <?php echo e($errors->has('numero_registro_nacional') ? 'has-error' : ''); ?>">
                    <label for="">Número de registro nacional</label>
                    <?php echo e(Form::text('numero_registro_nacional', $vessel->numero_registro_nacional, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('numero_registro_nacional')); ?> </span>
                </div>

                <div class="form-group <?php echo e($errors->has('nombre_barco') ? 'has-error' : ''); ?>">
                    <label for=""><span class="text-danger">*</span> Nombre del barco</label>
                    <?php echo e(Form::text('nombre_barco', $vessel->nombre_barco, ['class' => 'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('nombre_barco')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('nombre_barco_ingles') ? 'has-error' : ''); ?>">
                    <label for="">Nombre del barco en ingles</label>
                    <?php echo e(Form::text('nombre_barco_ingles', $vessel->nombre_barco_ingles, ['class' => 'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('nombre_barco_ingles')); ?> </span>
                </div>

                <div class="form-group">
                    <label for="">Puerto de registro</label>
                    <?php echo e(Form::select('puerto_registro', $refs['registration_ports'], $vessel->puerto_registro, ['class' => 'form-control select2', 'style' => 'width: 100%' ])); ?>

                </div>

                <div class="form-group">
                    <label for="">Tipo de barco</label>
                    <?php echo e(Form::select('tipo_barco', $refs['vessel_types'], $vessel->tipo_barco, ['class' => 'form-control select2', 'style' => 'width: 100%' ])); ?>

                </div>

                <div class="form-group">
                    <label for="">Funcionamiento</label>
                    <div class="radio">
                      <label> <?php echo e(Form::radio('funcionamiento', '1', (is_null($vessel->funcionamiento) || $vessel->funcionamiento == 1))); ?> Sí </label>
                    </div>

                    <div class="radio">
                      <label> <?php echo e(Form::radio('funcionamiento', '0', (is_numeric($vessel->funcionamiento) && $vessel->funcionamiento == 0))); ?> No </label>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            <?php echo e(Form::close()); ?>

        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

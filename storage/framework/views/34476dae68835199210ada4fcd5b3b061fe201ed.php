    <?php $__env->startSection('page_header'); ?>
        <?php echo $__env->make('common.page_header', ['page_header' => 'Actualizar Buque'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php $__env->stopSection(); ?>

    <?php $__env->startSection('content'); ?>
        <?php echo $__env->make('common.validation_errors', ['general' => true], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->make('buques_artesanales.forms.CreateUpdateForm', ['buque' => $buque, 'refs' => $refs], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
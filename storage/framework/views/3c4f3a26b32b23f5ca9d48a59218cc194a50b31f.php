<?php 
    $request_tab = app('request')->input('tab');
    $active_tab =  is_null(old('form_tab', null)) ? (!is_null($request_tab) ? $request_tab : 1) : old('form_tab');
 ?>
<div class="row">
    <div class="col-md-3">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs" style="padding-left: 0; padding-right:0" id="tabs-left">
                <li class="<?php echo e($active_tab == 1 ? 'active' : ''); ?> col-xs-12"><a href="#form_1" id="tab-identificacion" data-toggle="tab">1. Identificación</a></li>
                <?php if($vessel->exists): ?>
                    <li class="<?php echo e($active_tab == 2 ? 'active' : ''); ?> col-xs-12"><a href="#form_2" id="tab-regional-id" data-toggle="tab">2. Identificación Regional</a></li>
                    <li class="<?php echo e($active_tab == 3 ? 'active' : ''); ?> col-xs-12"><a href="#form_3" id="tab-registry" data-toggle="tab">3. Registro</a></li>
                    <li class="<?php echo e($active_tab == 4 ? 'active' : ''); ?> col-xs-12"><a href="#form_4" id="tab-dimensions" data-toggle="tab">4. Dimensiones</a></li>
                    <li class="<?php echo e($active_tab == 5 ? 'active' : ''); ?> col-xs-12"><a href="#form_5" id="tab-building" data-toggle="tab">5. Construcción</a></li>
                    <li class="<?php echo e($active_tab == 6 ? 'active' : ''); ?> col-xs-12"><a href="#form_6" id="tab-ownership" data-toggle="tab">6. Propiedad</a></li>
                    <li class="<?php echo e($active_tab == 7 ? 'active' : ''); ?> col-xs-12"><a href="#form_7" id="tab-flag" data-toggle="tab">7. Permiso de Pesca</a></li>
                    <li class="<?php echo e($active_tab == 8 ? 'active' : ''); ?> col-xs-12"><a href="#form_8" id="tab-flag" data-toggle="tab">8. Artes y/o métodos autorizados</a></li>
                    <li class="<?php echo e($active_tab == 9 ? 'active' : ''); ?> col-xs-12"><a href="#form_9" id="tab-flag" data-toggle="tab">9. Dispositivo Excluidor de tortugas - DET</a></li>
                    <li class="<?php echo e($active_tab == 10 ? 'active' : ''); ?> col-xs-12"><a href="#form_10" id="tab-photos" data-toggle="tab">10. Fotografias de la embarcación</a></li>
                <?php endif; ?>
            </ul>
        </div>
    </div>

    <div class="col-md-9">

            <div class="tab-content">
                <?php echo $__env->make('vessels.forms.partial_forms.form_1', ['vessel' => $vessel, 'refs' =>$refs], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php echo $__env->make('vessels.forms.partial_forms.form_2', ['vessel' => $vessel, 'refs' => $refs], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php echo $__env->make('vessels.forms.partial_forms.form_3', ['vessel' => $vessel, 'refs' => $refs], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php echo $__env->make('vessels.forms.partial_forms.form_4', ['vessel' => $vessel, 'refs' => $refs], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php echo $__env->make('vessels.forms.partial_forms.form_5', ['vessel' => $vessel, 'refs' => $refs], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php echo $__env->make('vessels.forms.partial_forms.form_6', ['vessel' => $vessel, 'refs' => $refs], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php echo $__env->make('vessels.forms.partial_forms.form_7', ['vessel'=> $vessel, 'refs' => $refs], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php echo $__env->make('vessels.forms.partial_forms.form_8', ['vessel'=> $vessel, 'refs' => $refs], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php echo $__env->make('vessels.forms.partial_forms.form_9', ['vessel'=> $vessel, 'refs' => $refs], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php echo $__env->make('vessels.forms.partial_forms.form_10', ['vessel'=> $vessel, 'refs' => $refs, 'constants' => $constants], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
            <!-- .tab-content -->
    </div>
    <!-- .div-col-md-9-->
</div>

<?php $__env->startSection('plugins_css'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('plugins/datepicker/datepicker3.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/select2/select2.min.css')); ?>">
    <style media="screen">
        .nav-tabs-custom>.nav-tabs>li.active>a, .nav-tabs-custom>.nav-tabs>li.active:hover>a
        {
            font-weight: bold;
            color: #3c8dbc !important;
            border: none !important;
        }
        #help-block-error-files
        {
            color: #a94442 !important;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('plugins_js'); ?>
    <script type="text/javascript" src="<?php echo e(asset('plugins/select2/select2.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('plugins/datepicker/bootstrap-datepicker.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('plugins/datepicker/locales/bootstrap-datepicker.es.js')); ?>"></script>
    <script type="text/javascript">
        $(document).ready(inicio);

        $('.datepicker').datepicker({
            language: 'es',
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $('.datepicker-years').datepicker( {
            language: 'es',
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years",
            autoclose: true
        });

        $("select").select2();

        function inicio()
        {
            $("span.delete-vessel-img").click(deleteImg);

            if(window.FileReader)
            {
                $("#vessel-files").change(verifyFiles);
            }
        }

        function verifyFiles(e)
        {
            var available_tipes = ['jpg', 'jpeg', 'png'];
            var max_size = 2097152;

            var files = e.originalEvent.target.files;
            var submit_disabled = false;
            var message = '';

            if(files.length > 0)
            {
                $("#submit-form-10").prop('disabled', true);

                var regex_extension = /(?:\.([^.]+))?$/;

                for(var i=0, len=files.length; i<len; i++)
                {
                    var n = files[i].name, s = files[i].size, t = files[i].type;

                    var ext = regex_extension.exec(n)[1];

                    if(typeof ext === 'undefined' || available_tipes.indexOf(ext.toLowerCase()) === -1 || s > max_size)
                    {
                        submit_disabled = true;
                        message = 'El archivo: "' + n + '" no cumple con los requisitos, intente quitarlo o cambiarlo';
                        break;
                    }
                }
            }

            $("#submit-form-10").prop('disabled', submit_disabled);
            $("#help-block-error-files").text(message);
        }

        function deleteImg()
        {
            var id = $(this).attr("data-id");
            console.log(id);
            var r = confirm("¿Eliminar la imagen?");

            if(r)
            {
                var datos = {id : id};

                $.ajax({
                   url: '/vessels/delete_image/' + id,
                   type: 'get',
                   dataType: 'json',
                   data: datos,
                   beforeSend: function(){},
                   success: function(response){

                       if(response.success)
                       {
                           $("div.vessel-img-container[data-id='"+id+"']").remove();
                           alert("Imagen eliminada correctamente");
                       }
                       else
                       {
                           alert("No se puede eliminar la imagen por el momento");
                       }
                   },
                   error: function(request, error, status){
                       console.log("AJAX Error: ");
                       console.log($(request.responseText).text());
                    }
                });
            }
        }

    </script>
<?php $__env->stopSection(); ?>

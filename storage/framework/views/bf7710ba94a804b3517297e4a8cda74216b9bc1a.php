    <?php $__env->startSection('page_header'); ?>
        <?php echo $__env->make('common.page_header', ['page_header' => 'Sanciones - Buques Artesanales'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php $__env->stopSection(); ?>

    <?php $__env->startSection('content'); ?>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Listado de sanciones de buques artesanales</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                            <table class="table table-bordered table-hover dt-responsive" id="sanciones-table">
                                <thead>
                                    <tr>
                                        <th>NUR</th>
                                        <th class="text-center">Buque</th>
                                        <th class="text-center">Infracción en proceso de investigación</th>
                                        <th class="text-center">Fecha de sanción</th>
                                        <th class="text-center">Valor Multa</th>
                                        <th class="text-center">Estado de la sanción</th>
                                        <th class="text-center">Archivos</th>
                                        <th class="text-center">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $__currentLoopData = $sanciones; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sancion): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                        <tr>
                                            <td class="text-center"><?php echo e($sancion->nur); ?></td>
                                            <td class="text-center"><?php echo e($sancion->rel_buque_artesanal->nombre); ?></td>
                                            <td class="text-center"><b><?php echo e($sancion->ref_infraccion_proceso->name); ?></b></td>
                                            <td class="text-center"><?php echo e($sancion->fecha_sancion); ?></td>
                                            <td class="text-center">$ <?php echo e(number_format($sancion->valor_multa, 2, '.',' ')); ?></td>
                                            <td class="text-center"><?php echo e($sancion->ref_sanciones_estados->name); ?></td>
                                            <td class="text-center">
                                                <?php if(count($sancion->rel_files) > 0): ?>
                                                    <a href="<?php echo e(route('buque_artesanal_sancion_download_attachments_zip', ['sancion' => $sancion->id])); ?>" target="_blank"><i class="fa fa-file-archive-o fa-lg"></i></a>
                                                <?php else: ?>
                                                    <span class="text-muted">Sin Archivos</span>
                                                <?php endif; ?>
                                            </td>
                                            <td class="text-center">
                                                <a href="<?php echo e(route('edit_buque_artesanal_sancion_path', ['sancion' => $sancion->id])); ?>" class="btn btn-xs btn-info">Editar</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                </tbody>
                            </table>

                        <?php echo e($sanciones->render()); ?>

                    </div>
                    <!-- /.box-body -->
                </div>
                  <!-- /.box -->
            </div>
        </div>
    <?php $__env->stopSection(); ?>
    <?php $__env->startSection('plugins_css'); ?>
        <link rel="stylesheet" href="<?php echo e(asset('plugins/datatables/dataTables.bootstrap.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('plugins/datatables/responsive.bootstrap.min.css')); ?>">
        <style media="screen">
        </style>
    <?php $__env->stopSection(); ?>
    <?php $__env->startSection('plugins_js'); ?>
        <script src="<?php echo e(asset('plugins/datatables/jquery.dataTables.min.js')); ?>"></script>
        <script src="<?php echo e(asset('plugins/datatables/dataTables.bootstrap.min.js')); ?>"></script>
        <script type="text/javascript">
            $(document).ready(inicio);

            function inicio()
            {
                var table = $("#sanciones-table");
                var last_column = $(table).find("thead > tr > th").length - 1;

                $(table).DataTable({
                    paging: false,
                    lengthChange: false,
                    searching: true,
                    ordering: true,
                    info: true,
                    autoWidth: false,
                    //columnDefs: [], // Buscar por el atributo alt en las imagenes
                    aoColumnDefs: [{ "bSortable" : false, "aTargets" : [ last_column ] }],
                    language: DATATABLES_LAN
                });
            }
        </script>
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
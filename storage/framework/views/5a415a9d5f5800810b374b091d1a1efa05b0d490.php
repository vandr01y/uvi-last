<div class="tab-pane <?php echo e($active_tab == 4 ? 'active' : ''); ?>" id="form_4">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Dimensiones</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            <?php echo e(Form::open(['route' => ['update_vessel_path', $vessel->id], 'method' => 'PUT'])); ?>

            <?php echo e(csrf_field()); ?>

            <input type="hidden" name="form_tab" value="4">
            <div class="box-body">
                <div class="form-group">
                    <small class="text-muted"><span class="text-danger">*</span> Campos obligatorios</small>
                </div>

                <div class="form-group <?php echo e($errors->has('longitud_total') ? 'has-error' : ''); ?>">
                    <label for=""><span class="text-danger">*</span> Longitud total (LOA)(m)</label>
                    <?php echo e(Form::text('longitud_total', $vessel->longitud_total, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('longitud_total')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('eslora_perpendiculares') ? 'has-error' : ''); ?>">
                    <label for="">Eslora entre perpendiculares (LBP) (m)</label>
                    <?php echo e(Form::text('eslora_perpendiculares', $vessel->eslora_perpendiculares, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('eslora_perpendiculares')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('longitud_registrada') ? 'has-error' : ''); ?>">
                    <label for="">Longitud registrada (m)</label>
                    <?php echo e(Form::text('longitud_registrada', $vessel->longitud_registrada, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('longitud_registrada')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('manga') ? 'has-error' : ''); ?>">
                    <label for="">Manga / extrema amplitud (m)</label>
                    <?php echo e(Form::text('manga', $vessel->manga, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('manga')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('puntal') ? 'has-error' : ''); ?>">
                    <label for="">Puntal (m)</label>
                    <?php echo e(Form::text('puntal', $vessel->puntal, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('puntal')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('calado') ? 'has-error' : ''); ?>">
                    <label for="">Calado (m)</label>
                    <?php echo e(Form::text('calado', $vessel->calado, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('calado')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('peso_muerto') ? 'has-error' : ''); ?>">
                    <label for="">Peso muerto (Sólo los buques de suministro y transporte refrigerado)</label>
                    <?php echo e(Form::text('peso_muerto', $vessel->peso_muerto, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('peso_muerto')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('tonelaje_neto') ? 'has-error' : ''); ?>">
                    <label for="">Tonelaje neto (NT) (Sólo los buques de suministro y transporte refrigerado)</label>
                    <?php echo e(Form::text('tonelaje_neto', $vessel->tonelaje_neto, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('tonelaje_neto')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('tonelaje_registro_neto') ? 'has-error' : ''); ?>">
                    <label for="">Tonelaje de registro neto (TRN) (Sólo los buques de suministro y transporte refrigerado)</label>
                    <?php echo e(Form::text('tonelaje_registro_neto', $vessel->tonelaje_registro_neto, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('tonelaje_registro_neto')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('arqueo_bruto') ? 'has-error' : ''); ?>">
                    <label for=""><span class="text-danger">*</span> Arqueo bruto (GT)</label>
                    <?php echo e(Form::text('arqueo_bruto', $vessel->arqueo_bruto, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('arqueo_bruto')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('tonelaje_registro_bruto') ? 'has-error' : ''); ?>">
                    <label for=""><span class="text-danger">*</span> Tonelaje de registro bruto (TRB)</label>
                    <?php echo e(Form::text('tonelaje_registro_bruto', $vessel->tonelaje_registro_bruto, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('tonelaje_registro_bruto')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('potencia_motor_principal') ? 'has-error' : ''); ?>">
                    <label for="">Potencia del motor principal/s </label>
                    <?php echo e(Form::text('potencia_motor_principal', $vessel->potencia_motor_principal, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('potencia_motor_principal')); ?></span>
                </div>

                <div class="form-group">
                    <label for="">Unidad de Potencia</label>
                    <?php echo e(Form::select('unidad_potencia', $refs['power_units'], $vessel->unidad_potencia, ['class' => 'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>

                <div class="form-group">
                    <label for="">Material casco</label>
                    <?php echo e(Form::select('material_casco', $refs['hull_materials'], $vessel->material_casco, ['class' => 'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>

                <div class="form-group <?php echo e($errors->has('capacidad_bodega_pescado') ? 'has-error' : ''); ?>">
                    <label for="">Capacidad de la bodega de pescado</label>
                    <?php echo e(Form::text('capacidad_bodega_pescado', $vessel->capacidad_bodega_pescado, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('capacidad_bodega_pescado')); ?></span>
                </div>

                <div class="form-group">
                    <label for="">Tipo de la bodega de pescado</label>
                    <?php echo e(Form::select('tipo_bodega_pescado', $refs['fish_hold_types'], $vessel->tipo_bodega_pescado, ['class' => 'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            <?php echo e(Form::close()); ?>

        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

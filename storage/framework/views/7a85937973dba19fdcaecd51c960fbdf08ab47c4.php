<div class="tab-pane <?php echo e($active_tab == 10 ? 'active' : ''); ?>" id="form_10">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Fotografias de la embarcación</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            <?php echo e(Form::open(['route' => ['update_vessel_path', $vessel->id], 'method' => 'PUT', 'files' => true])); ?>

            <?php echo e(csrf_field()); ?>

            <input type="hidden" name="form_tab" value="10">
            <div class="box-body">
                <div class="form-group <?php echo e(count($errors->get('photos.*')) > 0 ? 'has-error' : ''); ?>">
                    <label for="">Fotografías</label>
                    <?php echo e(Form::file('photos[]', ['id' => 'vessel-files', 'class' => 'form-control', 'multiple' => 'true'])); ?>

                    <p class="help-block text-muted">Formatos admitidos: PNG, JPG, JPEG. Tamaño Máximo: 2MB</p>
                    <span class="help-block text-danger" id="help-block-error-files"></span>
                    <span class="help-block">
                        <?php $__currentLoopData = $errors->get('photos.*'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $messages): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                            <?php $__currentLoopData = $messages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $message): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <?php echo e($message); ?>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                    </span>
                    <?php if($vessel->exists && count($vessel->vr_pictures) > 0): ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Imágenes actuales: </label>

                                    <div class="row">
                                    <?php $__currentLoopData = $vessel->vr_pictures; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $picture): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                        <div class="col-md-3 text-center vessel-img-container" data-id='<?php echo e($picture->id); ?>'>
                                            <img src='<?php echo e(asset($constants['asset_vessels_images_path'] . $picture->picture)); ?>' class="img-responsive img-rounded"/>
                                            <span class="btn btn-xs btn-danger delete-vessel-img" data-id='<?php echo e($picture->id); ?>'><i class="fa fa-trash"></i></span>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" id="submit-form-10" class="btn btn-success">Guardar</button>
            </div>
            <?php echo e(Form::close()); ?>

        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

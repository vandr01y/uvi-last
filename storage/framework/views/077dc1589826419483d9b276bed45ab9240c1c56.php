<div class="tab-pane <?php echo e($active_tab == 8 ? 'active' : ''); ?>" id="form_8">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Artes y/o métodos de pesca autorizadas</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            <?php echo e(Form::open(['route' => ['update_vessel_path', $vessel->id], 'method' => 'PUT'])); ?>

            <?php echo e(csrf_field()); ?>

            <input type="hidden" name="form_tab" value="8">
            <div class="box-body">
                <div class="form-group">
                    <label for="">Denominación del arte de pesca tipo red</label>
                    <?php echo e(Form::select('arte_pesca_red', $refs['denominacion_arte_pesca_tipo_redes'], $vessel->arte_pesca_red, ['class'=>'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>

                <div class="form-group <?php echo e($errors->has('ojo_malla') ? 'has-error' : ''); ?>">
                    <label for="">Ojo de malla (pulgadas)</label>
                    <?php echo e(Form::text('ojo_malla', $vessel->ojo_malla, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('ojo_malla')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('ojo_malla_copo') ? 'has-error' : ''); ?>">
                    <label for="">Ojo de malla del copo (pulgadas)</label>
                    <?php echo e(Form::text('ojo_malla_copo', $vessel->ojo_malla_copo, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('ojo_malla_copo')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('longitud_red') ? 'has-error' : ''); ?>">
                    <label for="">Longitud de la red (Brazas)</label>
                    <?php echo e(Form::text('longitud_red', $vessel->longitud_red, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('longitud_red')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('longitud_relinga_superior') ? 'has-error' : ''); ?>">
                    <label for="">Longitud de la relinga superior (Redes arrastre) en pies</label>
                    <?php echo e(Form::text('longitud_relinga_superior', $vessel->longitud_relinga_superior, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('longitud_relinga_superior')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('ancho_red') ? 'has-error' : ''); ?>">
                    <label for="">Ancho de la red (Brazas)</label>
                    <?php echo e(Form::text('ancho_red', $vessel->ancho_red, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('ancho_red')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('cantidad_piezas') ? 'has-error' : ''); ?>">
                    <label for="">Cantidad de paños o piezas</label>
                    <?php echo e(Form::text('cantidad_piezas', $vessel->cantidad_piezas, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('cantidad_piezas')); ?></span>
                </div>

                <div class="form-group">
                    <label for="">Material de la red</label>
                    <?php echo e(Form::select('material_red', $refs['materiales_redes'], $vessel->material_red, ['class'=>'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>

                <div class="form-group">
                    <label for="">Denominación del arte de pesca tipo Sedal ó de anzuelo</label>
                    <?php echo e(Form::select('arte_pesca_anzuelo', $refs['denominacion_arte_pesca_tipo_sedal_anzuelos'], $vessel->arte_pesca_anzuelo, ['class'=>'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>

                <div class="form-group">
                    <label for="">Tipo de anzuelo</label>
                    <?php echo e(Form::select('tipo_anzuelo', $refs['tipos_anzuelos'], $vessel->tipo_anzuelo, ['class'=>'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>

                <div class="form-group <?php echo e($errors->has('tamanio_anzuelo') ? 'has-error' : ''); ?>">
                    <label for="">Tamaño y tipo de anzuelo</label>
                    <?php echo e(Form::text('tamanio_anzuelo', $vessel->tamanio_anzuelo, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('tamanio_anzuelo')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('cantidad_anzuelos') ? 'has-error' : ''); ?>">
                    <label for="">Cantidad de anzuelos</label>
                    <?php echo e(Form::text('cantidad_anzuelos', $vessel->cantidad_anzuelos, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('cantidad_anzuelos')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('longitud_linea_madre') ? 'has-error' : ''); ?>">
                    <label for="">Longitud de la línea madre</label>
                    <?php echo e(Form::text('longitud_linea_madre', $vessel->longitud_linea_madre, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('longitud_linea_madre')); ?></span>
                </div>

                <div class="form-group">
                    <label for="">Material de la línea madre</label>
                    <?php echo e(Form::select('material_linea_madre', $refs['materiales_linea_madre'], $vessel->material_linea_madre, ['class'=>'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>

                <div class="form-group">
                    <label for="">Material de las bajantes</label>
                    <?php echo e(Form::select('material_bajantes', $refs['materiales_bajantes'], $vessel->material_bajantes, ['class'=>'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>

                <div class="form-group <?php echo e($errors->has('cantidad_total_lineas') ? 'has-error' : ''); ?>">
                    <label for="">Cantidad total de líneas</label>
                    <?php echo e(Form::text('cantidad_total_lineas', $vessel->cantidad_total_lineas, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('cantidad_total_lineas')); ?></span>
                </div>

                <div class="form-group">
                    <label for="">Denominación del arte de pesca tipo Trampas o nasas</label>
                    <?php echo e(Form::select('arte_pesca_trampa', $refs['denominacion_arte_pesca_tipo_trampas_nasas'], $vessel->arte_pesca_trampa, ['class'=>'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>

                <div class="form-group <?php echo e($errors->has('cantidad_trampas') ? 'has-error' : ''); ?>">
                    <label for="">Cantidad de trampas o nasas</label>
                    <?php echo e(Form::text('cantidad_trampas', $vessel->cantidad_trampas, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('cantidad_trampas')); ?></span>
                </div>

                <div class="form-group">
                    <label for="">Material principal de la trampa o nasa</label>
                    <?php echo e(Form::select('materiales_trampas', $refs['materiales_principales_trampas_nasas'], $vessel->materiales_trampas, ['class'=>'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>

                <div class="form-group">
                    <label for="">Tipo de artefactos de herir o aferrar</label>
                    <?php echo e(Form::select('tipo_artefactos_herir', $refs['tipos_artefactos_herir_aferrar'], $vessel->tipo_artefactos_herir, ['class'=>'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>

                <div class="form-group <?php echo e($errors->has('cantidad_artefactos') ? 'has-error' : ''); ?>">
                    <label for="">Cantidad de artefactos</label>
                    <?php echo e(Form::text('cantidad_artefactos', $vessel->cantidad_artefactos, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('cantidad_artefactos')); ?></span>
                </div>

                <div class="form-group">
                    <label for="">Material del artefacto</label>
                    <?php echo e(Form::select('material_artefactos', $refs['materiales_artefactos'], $vessel->material_artefactos, ['class'=>'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>

                <div class="form-group <?php echo e($errors->has('arte_pesca_otros') ? 'has-error' : ''); ?>">
                    <label for="">Otros</label>
                    <?php echo e(Form::text('arte_pesca_otros', $vessel->arte_pesca_otros, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('arte_pesca_otros')); ?></span>
                </div>

                <div class="form-group">
                    <label for="">Uso de Dispositivos Agregadores de Peces - FAD (Fish Aggregating Device)</label>

                    <div class="radio">
                      <label> <?php echo e(Form::radio('fad', '1', (is_null($vessel->fad) && $vessel->fad == 1))); ?> Activo </label>
                    </div>

                    <div class="radio">
                      <label> <?php echo e(Form::radio('fad', '0', (is_numeric($vessel->fad) && $vessel->fad == 0))); ?> Inactivo </label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="">Tipo de FAD utilizados</label>
                    <?php echo e(Form::select('tipos_fad', $refs['tipos_fad'], $vessel->tipos_fad, ['class'=>'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>

                <div class="form-group <?php echo e($errors->has('cantidad_fad') ? 'has-error' : ''); ?>">
                    <label for="">Cantidad de FAD utilizados</label>
                    <?php echo e(Form::text('cantidad_fad', $vessel->cantidad_fad, ['class'=>'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('cantidad_fad')); ?></span>
                </div>

                <div class="form-group">
                    <label for="">Componentes del FAD</label>
                    <?php echo e(Form::textarea('componentes_fad', $vessel->componentes_fad, ['class'=>'form-control', 'rows' => '3'])); ?>

                </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            <?php echo e(Form::close()); ?>

        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

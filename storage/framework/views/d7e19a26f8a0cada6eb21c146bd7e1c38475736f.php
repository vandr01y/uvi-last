<?php if(isset($page_header)): ?>
    <section class="content-header">
      <h1>
        <?php echo e($page_header); ?>

        <!--<small>Optional description</small>-->
      </h1>
      <!--<ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
    </ol>-->
    </section>
<?php endif; ?>

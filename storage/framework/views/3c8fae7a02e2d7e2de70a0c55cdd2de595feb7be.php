<div class="tab-pane <?php echo e($active_tab == 5 ? 'active' : ''); ?>" id="form_5">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Construcción</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            <?php echo e(Form::open(['route' => ['update_vessel_path', $vessel->id], 'method' => 'PUT'])); ?>

            <?php echo e(csrf_field()); ?>

            <input type="hidden" name="form_tab" value="5">
            <div class="box-body">
                <div class="form-group">
                    <label for="">Año de construcción</label>

                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <?php echo e(Form::text('anio_construccion', $vessel->anio_construccion, ['class' => 'form-control pull-right datepicker-years', 'readonly' => 'true'])); ?>

                    </div>
                </div>

                <div class="form-group">
                    <label for="">País de construcción</label>
                    <?php echo e(Form::select('pais_construccion', $refs['countries'], $vessel->pais_construccion, ['class' => 'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            <?php echo e(Form::close()); ?>

        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

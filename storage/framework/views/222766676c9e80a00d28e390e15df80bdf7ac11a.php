    <?php $__env->startSection('page_header'); ?>
        <?php echo $__env->make('common.page_header', ['page_header' => 'Buques'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php $__env->stopSection(); ?>

    <?php $__env->startSection('content'); ?>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Listado de buques</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered table-hover" id="vessels-table">
                            <thead>
                                <tr>
                                    <th style="width: 50px;">IMO Number</th>
                                    <th class="text-center">Nombre</th>
                                    <th class="text-center">País</th>
                                    <th class="text-center">Tipo</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Fotos</th>
                                    <th class="text-center">Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = $vessels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vessel): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <tr>
                                        <td class="text-center"><?php echo e($vessel->numero_omi); ?></td>

                                        <td class="text-center">
                                            <?php if(trim($vessel->nombre_barco) != ''): ?>
                                                <?php echo e($vessel->nombre_barco); ?>

                                            <?php else: ?>
                                                <span class="text-muted">Sin nombre</span>
                                            <?php endif; ?>
                                        </td>

                                        <td class="text-center">
                                            <?php if(isset($vessel->ref_pais_pabellon->name) && !empty($vessel->ref_pais_pabellon->alpha_2) && File::exists(public_path('img/flags/') . strtolower($vessel->ref_pais_pabellon->alpha_2) . ".svg")): ?>
                                                <img src="<?php echo e(asset('img/flags/' . strtolower($vessel->ref_pais_pabellon->alpha_2) . ".svg")); ?>" alt="<?php echo e($vessel->ref_pais_pabellon->name); ?>" title="<?php echo e($vessel->ref_pais_pabellon->name); ?>" class="img-flag">
                                            <?php else: ?>
                                                <img src="<?php echo e(asset('img/flags/default.svg')); ?>" alt="<?php echo e(isset($vessel->ref_pais_pabellon->name) ? $vessel->ref_pais_pabellon->name : 'País no asignado'); ?>" title="<?php echo e(isset($vessel->ref_pais_pabellon->name) ? $vessel->ref_pais_pabellon->name : 'País no asignado'); ?>" class="img-flag">
                                            <?php endif; ?>
                                        </td>

                                        <td class="text-center">
                                            <?php if(isset($vessel->ref_tipo_barco->name)): ?>
                                                <?php echo e($vessel->ref_tipo_barco->name); ?>

                                            <?php else: ?>
                                                <span class="text-muted">Tipo no asignado</span>
                                            <?php endif; ?>
                                        </td>

                                        <td class="text-center">
                                            <b><?php echo e($vessel->status == 1 ? 'Activo' : 'Inactivo'); ?></b>
                                        </td>

                                        <td class="text-center">
                                            <?php if(count($vessel->vr_pictures) > 0): ?>
                                                <!--<img class='img-rounded vessel-photo' src="<?php echo e(asset($constants['asset_vessels_images_path'] . $vessel->vr_pictures->first()->picture)); ?>" data-id="<?php echo e($vessel->id); ?>">-->
                                                <i class="icon ion-images ion-15x vessel-photo" data-id="<?php echo e($vessel->id); ?>"></i>
                                            <?php else: ?>
                                                <span class="text-muted">Sin Fotos</span>
                                            <?php endif; ?>
                                        </td>
                                        <td class="text-center">
                                            <a href="<?php echo e(route('edit_vessel_path', ['vessel' => $vessel->id])); ?>" class="btn btn-xs btn-info">Editar</a>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                            </tbody>
                        </table>
                        <?php echo e($vessels->render()); ?>

                    </div>
                    <!-- /.box-body -->
                </div>
                  <!-- /.box -->
            </div>
        </div>

        <div class="modal fade" id="modal-img" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Imagenes del buque: </h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <?php $__currentLoopData = $vessels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vessel): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <?php if(isset($vessel->vr_pictures) && count($vessel->vr_pictures) > 0): ?>

                                        <div id="carousel-<?php echo e($vessel->id); ?>" class="carousel slide" data-ride="carousel" data-interval="false" style="display: none;">

                                            <ol class="carousel-indicators">
                                                <?php $__currentLoopData = $vessel->vr_pictures; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $picture): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                    <li data-target="#carousel-<?php echo e($vessel->id); ?>" data-slide-to="<?php echo e($loop->index); ?>" class="<?php echo e($loop->first ? 'active' : ''); ?>"></li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                            </ol>

                                            <div class="carousel-inner">
                                                <?php $__currentLoopData = $vessel->vr_pictures; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $picture): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                    <div class="item <?php echo e($loop->first ? 'active' : ''); ?>">
                                                        <img src="<?php echo e(asset($constants['asset_vessels_images_path'] . $picture->picture)); ?>" title="Foto <?php echo e($loop->iteration); ?>">

                                                        <div class="carousel-caption">
                                                            Foto <?php echo e($loop->iteration); ?>

                                                        </div>
                                                    </div>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                            </div>

                                            <a class="left carousel-control" href="#carousel-<?php echo e($vessel->id); ?>" data-slide="prev">
                                              <span class="fa fa-angle-left"></span>
                                            </a>
                                            <a class="right carousel-control" href="#carousel-<?php echo e($vessel->id); ?>" data-slide="next">
                                              <span class="fa fa-angle-right"></span>
                                            </a>
                                        </div>

                                    <?php endif; ?>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    <?php $__env->stopSection(); ?>
    <?php $__env->startSection('plugins_css'); ?>
        <link rel="stylesheet" href="<?php echo e(asset('plugins/datatables/dataTables.bootstrap.css')); ?>">
        <style media="screen">
            img.img-flag
            {
                width: 30px;
            }
            i.vessel-photo
            {
                cursor: pointer;
            }
        </style>
    <?php $__env->stopSection(); ?>
    <?php $__env->startSection('plugins_js'); ?>
        <script src="<?php echo e(asset('plugins/datatables/jquery.dataTables.min.js')); ?>"></script>
        <script src="<?php echo e(asset('plugins/datatables/dataTables.bootstrap.min.js')); ?>"></script>
        <script src="<?php echo e(asset('plugins/datatables/plugins/alt-string.js')); ?>"></script>
        <script type="text/javascript">
            $(document).ready(inicio);

            function inicio()
            {
                var last_column = $("#users-table > thead > tr > th").length - 1;

                $("#vessels-table").DataTable({
                    paging: false,
                    lengthChange: false,
                    searching: true,
                    ordering: true,
                    info: true,
                    autoWidth: false,
                    //columnDefs: [], // Buscar por el atributo alt en las imagenes
                    aoColumnDefs: [{ "bSortable" : false, "aTargets" : [ last_column ] }, { type: 'alt-string', targets: 2 }],
                    language: DATATABLES_LAN
                });

                $("i.vessel-photo").click(function(){

                    var id = $(this).attr('data-id');
                    $("#carousel-" + id).show();
                    $("#modal-img").modal('show');

                    $("#carousel-"+id+" > ol.carousel-indicators > li, #carousel-"+id+" > div.carousel-inner > div.item").removeClass('active');
                    $("#carousel-"+id+" > ol.carousel-indicators > li:first-child, #carousel-"+id+" > div.carousel-inner > div.item:first-child").addClass('active');
                });

                $("#modal-img").on('hidden.bs.modal', function () {
                    $("div[id^='carousel-']").hide();
                });
            }
        </script>
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
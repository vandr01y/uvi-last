    <?php $__env->startSection('page_header'); ?>
        <?php echo $__env->make('common.page_header', ['page_header' => 'Mi Perfil'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php $__env->stopSection(); ?>

    <?php $__env->startSection('content'); ?>
        <?php echo $__env->make('common.validation_errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="row">
            <div class="col-md-3">
              <!-- Profile Image -->
              <div class="box box-primary">
                <div class="box-body box-profile">
                  <?php echo e(Html::image("img/avatars/" . $user->profile_picture, 'Foto de perfil', ['class' => 'profile-user-img img-responsive img-circle'])); ?>

                  <h3 class="profile-username text-center"><?php echo e($user->name); ?></h3>
                  <p class="text-muted text-center"><?php echo e($user->rol->name); ?></p>

                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>

            <div class="col-md-9">
                <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs">
                    <li class="active"><a href="#datos" data-toggle="tab" aria-expanded="true">Datos</a></li>
                    <li><a href="#change_password" data-toggle="tab">Cambiar Contraseña</a></li>
                  </ul>

                  <div class="tab-content">

                    <div class="tab-pane active" id="datos">
                        <form class="form-horizontal" action="<?php echo e(route('update_user_profile_path', ['user' => $user->id])); ?>" role="form" method="POST" enctype="multipart/form-data">
                            <?php echo e(method_field('PUT')); ?>

                            <?php echo e(csrf_field()); ?>


                            <div class="form-group">
                              <label for="name" class="col-sm-2 control-label">Nombre</label>

                              <div class="col-sm-10">
                                <input type="text" name="name" class="form-control" id="name" placeholder="Nombre" value="<?php echo e(old('name') ? old('name') : $user->name); ?>">
                              </div>
                            </div>

                            <div class="form-group">
                              <label for="image" class="col-sm-2 control-label">Imagen de Perfil</label>

                              <div class="col-sm-10">
                                <input type="file" name="image" id="user-photo">
                                <p class="help-block">Formatos admitidos: <b>PNG, JPG, JPEG</b>. Tamaño Máximo <b>2MB</b></p>
                                <span id="help-block-error-files" class="help-block"></span>
                              </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <div class="checkbox">
                                      <label>
                                        <input type="checkbox" name="remove_img" id="remove-img" value="1"> Sin Imagen
                                      </label>
                                    </div>
                                  </div>
                            </div>


                            <div class="form-group">
                              <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-success" id="guardar">Guardar</button>
                              </div>
                            </div>
                      </form>
                    </div>
                    <!-- /.tab-pane -->

                    <div class="tab-pane" id="change_password">
                        <form class="form-horizontal" action="<?php echo e(route('update_user_password_path', ['user' => $user->id])); ?>" role="form" method="POST">
                            <?php echo e(method_field('PUT')); ?>

                            <?php echo e(csrf_field()); ?>


                        <div class="form-group">
                          <label for="password_current" class="col-sm-2 control-label">Contraseña Actual</label>

                          <div class="col-sm-10">
                            <input type="password" name="current_password" class="form-control" id="password_current" placeholder="Contraseña Actual">
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="password" class="col-sm-2 control-label">Nueva Contraseña</label>

                          <div class="col-sm-10">
                            <input type="password" name="password" class="form-control" id="password" placeholder="Nueva Contraseña">
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="password_confirmation" class="col-sm-2 control-label">Confirme nueva Contraseña</label>

                          <div class="col-sm-10">
                            <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Confirme Nueva Contraseña">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success">Guardar</button>
                            <p class="help-block">En caso de éxito, se cerrará la sesión</p>
                          </div>
                        </div>

                      </form>
                    </div>
                    <!-- /.tab-pane -->
                  </div>
                  <!-- /.tab-content -->
                </div>
                <!-- /.nav-tabs-custom -->
              </div>
        </div>
    <?php $__env->stopSection(); ?>
    <?php $__env->startSection('plugins_css'); ?>
        <style media="screen">
            #help-block-error-files
            {
                color: #a94442 !important;
            }
        </style>
    <?php $__env->stopSection(); ?>
    <?php $__env->startSection('plugins_js'); ?>
    <script type="text/javascript">
        $(document).ready(inicio);

        function inicio()
        {
            if(window.FileReader)
            {
                $("#user-photo").change(verifyFiles);
            }
        }

        function verifyFiles(e)
        {
            var message = '';
            var submit_disabled = false;

            var available_tipes = ['jpg', 'jpeg', 'png'];
            var max_size = 2097152;

            var files = e.originalEvent.target.files;

            if(files.length > 0)
            {
                $("#guardar").prop('disabled', true);

                var regex_extension = /(?:\.([^.]+))?$/;

                for(var i=0, len=files.length; i<len; i++)
                {
                    var n = files[i].name, s = files[i].size, t = files[i].type;

                    var ext = regex_extension.exec(n)[1];

                    if(typeof ext === 'undefined' || available_tipes.indexOf(ext.toLowerCase()) === -1 || s > max_size)
                    {
                        submit_disabled = true;
                        message = 'El archivo: "' + n + '" no cumple con los requisitos, intente quitarlo o cambiarlo';
                        break;
                    }
                }
            }

            $("#guardar").prop('disabled', submit_disabled);
            $("#help-block-error-files").text(message);
        }
    </script>
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
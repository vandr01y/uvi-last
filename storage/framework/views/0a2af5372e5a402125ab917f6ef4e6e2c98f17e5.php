<div class="tab-pane <?php echo e($active_tab == 9 ? 'active' : ''); ?>" id="form_9">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Fotografias de la embarcación</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            <?php echo e(Form::open(['route' => ['update_buque_artesanal_path', $buque->id], 'method' => 'PUT', 'files' => true])); ?>

            <?php echo e(csrf_field()); ?>

            <input type="hidden" name="form_tab" value="9">
            <div class="box-body">
                <div class="form-group <?php echo e(count($errors->get('photos_f9.*')) > 0 ? 'has-error' : ''); ?>">
                    <label for="">Fotografías</label>
                    <?php echo e(Form::file('photos_f9[]', ['id' => 'buque-artesanal-files', 'class' => 'form-control', 'multiple' => 'true'])); ?>

                    <p class="help-block text-muted">Formatos admitidos: PNG, JPG, JPEG. Tamaño Máximo: 2MB</p>
                    <span class="help-block text-danger" id="buque-artesanal-files-help-block"></span>
                    <span class="help-block">
                        <?php $__currentLoopData = $errors->get('photos_f9.*'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $messages): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                            <?php $__currentLoopData = $messages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $message): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <?php echo e($message); ?>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                    </span>
                    <?php if($buque->exists && count($buque->ba_e_pictures) > 0): ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Imágenes actuales: </label>

                                    <div class="row">
                                    <?php $__currentLoopData = $buque->ba_e_pictures; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $picture): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                        <div class="col-md-3 text-center buque-artesanal-img-container" data-id='<?php echo e($picture->id); ?>'>
                                            <img src='<?php echo e(asset($constants['asset_embarcaciones_photos_path'] . $picture->file)); ?>' class="img-responsive img-rounded"/>
                                            <span class="btn btn-xs btn-danger delete-buques-artesanales-img" data-id='<?php echo e($picture->id); ?>'><i class="fa fa-trash"></i></span>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success" id="submit-f9">Guardar</button><br>
              <small class="text-muted"><span id="f9-total-files">0</span> Archivos Seleccionados</small>
            </div>
            <?php echo e(Form::close()); ?>

        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

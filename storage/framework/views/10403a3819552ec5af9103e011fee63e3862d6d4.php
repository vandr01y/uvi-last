<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="box box-primary">
            <?php if($sancion->exists): ?>
                <?php echo e(Form::open(['route' => ['update_buque_artesanal_sancion_path', $sancion->id], 'method' => 'PUT', 'files' => true])); ?>

            <?php else: ?>
                <?php echo e(Form::open(['route' => 'store_buque_artesanal_sancion_path', 'method' => 'POST', 'files' => true])); ?>

            <?php endif; ?>
                <?php echo e(csrf_field()); ?>



            <div class="box-header with-border">
            </div>

            <div class="box-body">
                <div role="form">
                    <div class="form-group <?php echo e($errors->has('buque_artesanal_id') ? 'has-error' : ''); ?>">
                        <label for=""><span class="text-danger">*</span> Buque</label>
                        <?php echo e(Form::select('buque_artesanal_id', $refs['buques_artesanales'], $sancion->buque_artesanal_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2'])); ?>

                        <span class="help-block"><?php echo e($errors->first('buque_artesanal_id')); ?></span>
                    </div>

                    <div class="form-group <?php echo e($errors->has('presunta_violacion') ? 'has-error' : ''); ?>">
                        <label for=""><span class="text-danger">*</span> ¿La motonave ha tenido presunta violación al estatuto pesquero?</label>
                        <div class="radio">
                          <label> <?php echo e(Form::radio('presunta_violacion', '1', (!$sancion->exists || ($sancion->exists && $sancion->presunta_violacion == 1)))); ?> Sí </label>
                        </div>

                        <div class="radio">
                          <label> <?php echo e(Form::radio('presunta_violacion', '0', ($sancion->exists && $sancion->presunta_violacion == 0))); ?> No </label>
                        </div>
                        <span class="help-block"><?php echo e($errors->first('presunta_violacion')); ?></span>
                    </div>

                    <div class="form-group <?php echo e($errors->has('nur') ? 'has-error' : ''); ?>">
                        <label for=""><span class="text-danger">*</span> Número único de registro - NUR</label>
                        <?php echo e(Form::text('nur', $sancion->nur, ['class' => 'form-control'])); ?>

                        <span class="help-block"><?php echo e($errors->first('nur')); ?></span>
                    </div>

                    <div class="form-group <?php echo e($errors->has('infraccion_proceso_id') ? 'has-error' : ''); ?>">
                        <label for=""><span class="text-danger">*</span> Infracción en proceso de investigación</label>
                        <?php echo e(Form::select('infraccion_proceso_id', $refs['sanciones_tipos_infracciones'], $sancion->infraccion_proceso_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2'])); ?>

                        <span class="help-block"><?php echo e($errors->first('infraccion_proceso_id')); ?></span>
                    </div>

                    <div class="form-group <?php echo e($errors->has('sanciones_estatuto_pesquero') ? 'has-error' : ''); ?>">
                        <label for=""><span class="text-danger">*</span> Sanciones al estatuto pesquero</label>
                        <div class="radio">
                          <label> <?php echo e(Form::radio('sanciones_estatuto_pesquero', '1', (!$sancion->exists || ($sancion->exists && $sancion->sanciones_estatuto_pesquero == 1)))); ?> Sí </label>
                        </div>

                        <div class="radio">
                          <label> <?php echo e(Form::radio('sanciones_estatuto_pesquero', '0', ($sancion->exists && $sancion->sanciones_estatuto_pesquero == 0))); ?> No </label>
                        </div>
                        <span class="help-block"><?php echo e($errors->first('sanciones_estatuto_pesqu')); ?></span>
                    </div>

                    <div class="form-group <?php echo e($errors->has('numero_acto_administrativo') ? 'has-error' : ''); ?>">
                        <label for=""><span class="text-danger">*</span> Número acto adminsitrativo mediante el cual se sanciono violacion al estatuto pesquero (No. xxx).</label>
                        <?php echo e(Form::text('numero_acto_administrativo', $sancion->numero_acto_administrativo, ['class' => 'form-control'])); ?>

                        <span class="help-block"><?php echo e($errors->first('numero_acto_administrativo')); ?></span>
                    </div>

                    <div class="form-group <?php echo e($errors->has('fecha_sancion') ? 'has-error' : ''); ?>">
                        <label for=""><span class="text-danger">*</span> Fecha de la sanción</label>

                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <?php echo e(Form::text('fecha_sancion', $sancion->fecha_sancion, ['class' => 'form-control pull-right datepicker', 'readonly' => 'true'])); ?>

                        </div>
                        <span class="help-block"><?php echo e($errors->first('fecha_sancion')); ?></span>
                    </div>

                    <div class="form-group <?php echo e($errors->has('infraccion_sancionada_id') ? 'has-error' : ''); ?>">
                        <label for="">Infracción sancionada</label>
                        <?php echo e(Form::select('infraccion_sancionada_id', $refs['sanciones_tipos_infracciones'], $sancion->infraccion_sancionada_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2'])); ?>

                        <span class="help-block"><?php echo e($errors->first('infraccion_sancionada_id')); ?></span>
                    </div>

                    <div class="form-group <?php echo e($errors->has('tipo_sancion_id') ? 'has-error' : ''); ?>">
                        <label for="">Tipo Sanción</label>
                        <?php echo e(Form::select('tipo_sancion_id', $refs['sanciones_tipos'], $sancion->tipo_sancion_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2'])); ?>

                        <span class="help-block"><?php echo e($errors->first('tipo_sancion_id')); ?></span>
                    </div>

                    <div class="form-group <?php echo e($errors->has('fallo_ejecutoriado_id') ? 'has-error' : ''); ?>">
                        <label for="">Fallo ejecutoriado o con recurso</label>
                        <?php echo e(Form::select('fallo_ejecutoriado_id', $refs['sanciones_fallos_ejecutoriados'], $sancion->fallo_ejecutoriado_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2'])); ?>

                        <span class="help-block"><?php echo e($errors->first('fallo_ejecutoriado_id')); ?></span>
                    </div>

                    <div class="form-group <?php echo e($errors->has('valor_multa') ? 'has-error' : ''); ?>">
                        <label for=""><span class="text-danger">*</span> Valor de la multa</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-dollar"></i>
                            </div>
                            <?php echo e(Form::text('valor_multa', $sancion->valor_multa, ['class' => 'form-control pull-right'])); ?>

                        </div>
                        <span class="help-block"><?php echo e($errors->first('valor_multa')); ?></span>
                    </div>

                    <div class="form-group <?php echo e($errors->has('estado_sancion_id') ? 'has-error' : ''); ?>">
                        <label for=""><span class="text-danger">*</span> Estado de la sanción</label>
                        <?php echo e(Form::select('estado_sancion_id', $refs['sanciones_estados'], $sancion->estado_sancion_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2'])); ?>

                        <span class="help-block"><?php echo e($errors->first('estado_sancion_id')); ?></span>
                    </div>

                    <div class="form-group <?php echo e($errors->has('detalles') ? 'has-error' : ''); ?>">
                        <label for="">Detalles u observación</label>
                        <?php echo e(Form::textarea('detalles', $sancion->detalles, ['class' => 'form-control', 'rows' => 3])); ?>

                        <span class="help-block"><?php echo e($errors->first('detalles')); ?></span>
                    </div>

                    <div class="form-group <?php echo e(count($errors->get('attachments.*')) > 0 ? 'has-error' : ''); ?>">
                        <label for="">Archivos Adjuntos</label>
                        <?php echo e(Form::file('attachments[]', ['id' => 'attachments', 'class' => 'form-control', 'multiple' => 'true'])); ?>

                        <p class="help-block text-muted">Formatos admitidos: PDF. Tamaño Máximo: 5MB</p>
                        <span class="help-block text-danger" id="help-block-error-files"></span>
                        <span class="help-block">
                            <?php $__currentLoopData = $errors->get('attachments.*'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $messages): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <?php $__currentLoopData = $messages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $message): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <?php echo e($message); ?>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        </span>

                        <?php if(count($sancion->rel_files) > 0): ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Archivos actuales: </label>

                                        <div class="row">
                                        <?php $__currentLoopData = $sancion->rel_files; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                            <div class="col-md-3 text-center buque-artesanal-sancion-attachment-container" data-id='<?php echo e($file->id); ?>'>
                                                    <div class="form-group">
                                                        <a href="<?php echo e(route('buque_artesanal_sancion_download_attachment', ['id' => $file->id])); ?>" target="_blank" title="<?php echo e($file->name); ?>">
                                                            <i class="fa fa-file-pdf-o fa-2x text-danger"></i>
                                                            <br>
                                                            <span class="text-muted">
                                                                <?php echo e(strlen($file->name) > 15 ? ( substr($file->name, 0, 14) . "...") : $file->name); ?>

                                                            </span>
                                                        </a>
                                                        <br>
                                                        <span class="btn btn-xs btn-danger delete-buque-artesanal-sancion-attachment" data-id='<?php echo e($file->id); ?>'><i class="fa fa-trash"></i></span>
                                                    </div>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>

                </div>

                <div class="form-group <?php echo e($errors->has('status') ? 'has-error' : ''); ?>">
                    <label for="">Status</label>
                    <div class="radio">
                      <label> <?php echo e(Form::radio('status', '1', (!$sancion->exists || ($sancion->exists && $sancion->status == 1)))); ?> Activa </label>
                    </div>

                    <div class="radio">
                      <label> <?php echo e(Form::radio('status', '0', ($sancion->exists && $sancion->status == 0))); ?> Inactiva </label>
                    </div>
                    <span class="help-block"><?php echo e($errors->first('status')); ?></span>
                </div>

            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-md btn-success" id="guardar">Guardar</button>
            </div>

            <?php echo e(Form::close()); ?>

        </div>
    </div>
</div>

<?php $__env->startSection('plugins_css'); ?>
    <?php echo e(Html::style('plugins/datepicker/datepicker3.css')); ?>

    <?php echo e(Html::style('plugins/select2/select2.min.css')); ?>

    <style media="screen">
        #help-block-error-files
        {
            color: #a94442 !important;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('plugins_js'); ?>
    <?php echo e(Html::script('plugins/select2/select2.min.js')); ?>

    <?php echo e(Html::script('plugins/datepicker/bootstrap-datepicker.js')); ?>

    <?php echo e(Html::script('plugins/datepicker/locales/bootstrap-datepicker.es.js')); ?>


    <script type="text/javascript">
        $(document).ready(inicio);

        function inicio()
        {
            $('.datepicker').datepicker({
                language: 'es',
                format: 'yyyy-mm-dd',
                autoclose: true
            });

            $("select").select2();

            if(window.FileReader)
            {
                $("#attachments").change(verifyFiles);
            }

            $("span.delete-buque-artesanal-sancion-attachment").click(deleteAttachment);
        }

        function verifyFiles(e)
        {
            var files = e.originalEvent.target.files;
            var submit_disabled = false;
            var message = '';

            var b_submit = $("#guardar");

            if(files.length > 0)
            {
                var available_tipes = ['pdf'];
                var max_size = 5 * 1048576;

                $(b_submit).prop('disabled', true);

                var regex_extension = /(?:\.([^.]+))?$/;

                for(var i=0, len=files.length; i<len; i++)
                {
                    var n = files[i].name, s = files[i].size, t = files[i].type;

                    var ext = regex_extension.exec(n)[1];

                    if(typeof ext === 'undefined' || available_tipes.indexOf(ext.toLowerCase()) === -1 || s > max_size)
                    {
                        submit_disabled = true;
                        message = 'El archivo: "' + n + '" no cumple con los requisitos, intente quitarlo o cambiarlo';
                        break;
                    }
                }
            }

            $(b_submit).prop('disabled', submit_disabled);
            $("#help-block-error-files").text(message);
        }

        function deleteAttachment()
        {
            var id = $(this).attr("data-id");
            var r = confirm("¿Eliminar el archivo?");

            if(r)
            {
                var datos = {id : id};
                var route = "<?php echo e(url('/sanciones_buques_artesanales/delete_attachment/')); ?>" + "/";
                $.ajax({
                   url: route+id,
                   type: 'get',
                   dataType: 'json',
                   data: datos,
                   beforeSend: function(){},
                   success: function(response){

                       if(response.success)
                       {
                           $("div.buque-artesanal-sancion-attachment-container[data-id='"+id+"']").remove();
                           alert("Archivo eliminado correctamente");
                       }
                       else
                       {
                           alert("No se puede eliminar el archivo por el momento");
                       }
                   },
                   error: function(request, error, status){
                       console.log("AJAX Error: ");
                       console.log($(request.responseText).text());
                    }
                });
            }
        }
    </script>
<?php $__env->stopSection(); ?>

<div class="tab-pane" id="form_18">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">DET</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            <div class="box-body">

                <div class="form-group">
                    <label for="">Tipo de marco - material (tubo ó barra plana)</label>
                    <?php echo e(Form::select('f18_tipo_marco_material_id', $refs['materiales_tipo_marco'], ($vessel->exists ? $vessel->v_dets->vessel_id : null), ['class' => 'form-control select2', 'style' => 'width: 100%'])); ?>

                </div>

                <div class="form-group">
                    <label for="">Distancia entre las barras (hasta 4 pulgadas)</label>
                    <?php echo e(Form::text('f18_distancia_entre_barras', ($vessel->exists ? $vessel->v_dets->distancia_entre_barras : ''), ['class' => 'form-control'])); ?>

                </div>

                <div class="form-group">
                    <label for="">Altura y ancho de la parrilla (mínimo 32*32 pulgadas)</label>
                    <?php echo e(Form::text('f18_altura_ancho_parrilla', ($vessel->exists ? $vessel->v_dets->altura_ancho_parrilla : ''), ['class' => 'form-control'])); ?>

                </div>

                <div class="form-group">
                    <label for="">Angulo de Inclinación Derecho (30 – 55 grados, ideal 45 grados)</label>
                    <?php echo e(Form::text('f18_angulo_inclinacion_derecho', ($vessel->exists ? $vessel->v_dets->angulo_inclinacion_derecho : ''), ['class' => 'form-control'])); ?>

                </div>

                <div class="form-group">
                    <label for="">Angulo de Inclinación Izquierdo (30 – 55 grados, 45 grados)</label>
                    <?php echo e(Form::text('f18_angulo_inclinacion_izquierdo', ($vessel->exists ? $vessel->v_dets->angulo_inclinacion_izquierdo : ''), ['class' => 'form-control'])); ?>

                </div>

                <div class="form-group">
                    <label for="">Amplitud mínima de escape de la solapa (71 pulgadas)</label>
                    <?php echo e(Form::text('f18_amplitud_minima_escape_solapa', ($vessel->exists ? $vessel->v_dets->amplitud_minima_escape_solapa : ''), ['class' => 'form-control'])); ?>

                </div>

                <div class="form-group">
                    <label for="">Amplitud mínima de escape de la solapa DET cobertura doble  (mínimo 56 pulgadas)</label>
                    <?php echo e(Form::text('f18_amplitud_minima_escape_solapa_det_cobertura_doble', ($vessel->exists ? $vessel->v_dets->amplitud_minima_escape_solapa_det_cobertura_doble : ''), ['class' => 'form-control'])); ?>

                </div>

                <div class="form-group">
                    <label for=""> Longitud de la relinga inferior (15 pulgadas)</label>
                    <?php echo e(Form::text('f18_longitud_relinga_inferior', ($vessel->exists ? $vessel->v_dets->longitud_relinga_inferior : ''), ['class' => 'form-control'])); ?>

                </div>

                <div class="form-group">
                    <label for="">Traslape (máximo 15 pulgadas)</label>
                    <?php echo e(Form::text('f18_traslape', ($vessel->exists ? $vessel->v_dets->traslape : ''), ['class' => 'form-control'])); ?>

                </div>

                <div class="form-group <?php echo e($errors->has('f18_redes_repuesto') ? 'has-error' : ''); ?>">
                    <label for="">Redes de repuesto</label>
                    <?php echo e(Form::text('f18_redes_repuesto', ($vessel->exists ? $vessel->v_dets->redes_repuesto : ''), ['class' => 'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('f18_redes_repuesto')); ?></span>
                </div>

                <div class="form-group <?php echo e($errors->has('f18_parrillas_repuesto') ? 'has-error' : ''); ?>">
                    <label for="">Parrillas de repuesto</label>
                    <?php echo e(Form::text('f18_parrillas_repuesto', ($vessel->exists ? $vessel->v_dets->parrillas_repuesto : ''), ['class' => 'form-control'])); ?>

                    <span class="help-block"><?php echo e($errors->first('f18_parrillas_repuesto')); ?></span>
                </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <a href="#form_7" class="btn btn-success a-change-tab" data-toggle="tab">Siguiente <i class=""></i></a>
            </div>
        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

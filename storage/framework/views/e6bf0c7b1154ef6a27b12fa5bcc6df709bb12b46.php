<div class="tab-pane" id="form_12">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Permiso de Pesca</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            <div class="box-body">

                <div class="form-group">
                    <label for="">Número OMI</label>
                    <input type="text" class="form-control" name="f12_imo_number">
                </div>

                <div class="form-group">
                    <label for="">Tipo de autorización</label>
                    <input type="text" class="form-control" name="f12_authorisation_type">
                </div>

                <div class="form-group">
                    <label for="">Número de autorización</label>
                    <input type="text" class="form-control" name="f12_authorisation_number">
                </div>

                <div class="form-group">
                    <label for="">Titular de la autorización de comercialización</label>
                    <select class="form-control" name="f12_authorisation_holder_id">
                        <option value="">1</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="">Publicado por</label>
                    <input type="text" class="form-control" name="f12_issued_by">
                </div>

                <div class="form-group">
                    <label for="">Fecha de emisión</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="f12_issued_date" class="form-control pull-right datepicker" value="" READONLY>
                    </div>
                </div>

                <div class="form-group">
                    <label for="">Fecha de inicio del período de autorización</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="f12_authorisation_period_start_date" class="form-control pull-right datepicker" value="" READONLY>
                    </div>
                </div>

                <div class="form-group">
                    <label for="">Fecha de finalización del período de autorización</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="f12_authorisation_period_end_date" class="form-control pull-right datepicker" value="" READONLY>
                    </div>
                </div>

                <div class="form-group">
                    <label for="">Zona autorizada</label>
                    <select class="form-control" name="f12_authorised_area7_id">
                        <option value="">1</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="">Especies autorizadas</label>
                    <select class="form-control" name="f12_authorised_species7_id">
                        <option value="">1</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="">Artes autorizados</label>
                    <select class="form-control" name="f12_authorised_gear7_id">
                        <option value="">1</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="">Fecha de revocación</label>

                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="f12_date_of_revocation" class="form-control pull-right datepicker" value="" READONLY>
                    </div>
                </div>

                <div class="form-group">
                    <label for="">Motivo para la revocación</label>
                    <select class="form-control" name="f12_reason_for_revocation_id">
                        <option value="">1</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="">Detalles de la revocación</label>
                    <input type="text" class="form-control" name="f12_reason_for_revocation_details">
                </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button class="btn btn-success">Siguiente <i class=""></i></button>
            </div>
        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

@if(isset($errors) && count($errors) > 0)
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="alert alert-danger text-center">
                @if(isset($general) && $general === true)
                    <span>Revise los errores</span>
                @else
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>
    </div>
@endif

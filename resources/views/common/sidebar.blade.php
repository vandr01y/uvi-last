<ul class="sidebar-menu">
    <li class="header">MENU</li>
    <!-- Optionally, you can add icons to the links -->
    <li><a href="{{ route('home_path') }}"><i class="fa fa-home"></i> <span>Inicio</span></a></li>
    <!--<li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>-->
    <li class="treeview">
        <a href="#"><i class="fa fa-user"></i> <span>Usuarios</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="{{ route('users_list') }}">Lista</a></li>
            <li><a href="{{ route('create_user_path') }}">Registrar</a></li>
        </ul>
    </li>
    <!-- BUQUES -->
    <li class="treeview">
        <a href="#"><i class="fa fa-desktop"></i> <span>UVI</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li>
                <a href="#">
                    <i class="fa fa-ship"></i>Buques Industriales
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('vessels_list') }}"><i class="fa fa-list-alt"></i>Lista</a></li>
                    <li><a href="{{ route('create_vessel_path') }}"><i class="fa fa-pencil"></i>Registrar</a></li>
                </ul>
            </li>

            <li>
                <a href="#">
                    <i class="fa fa-ship"></i>Buques Artesanales
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('buques_artesanales_list') }}"><i class="fa fa-list-alt"></i>Lista</a></li>
                    <li><a href="{{ route('create_buque_artesanal_path') }}"><i class="fa fa-pencil"></i>Registrar</a></li>
                </ul>
            </li>
            <!-- SANCIONES -->
            <li>
                <a href="#">
                    <i class="fa fa-gavel"></i>Sanciones - Industriales
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ route('vessel_sanciones_list') }}"><i class="fa fa-list-alt"></i>Lista</a>
                    </li>

                    <li>
                        <a href="{{ route('create_vessel_sancion_path') }}"><i class="fa fa-pencil"></i>Registrar</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-gavel"></i>Sanciones - Artesanales
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ route('buques_artesanales_sanciones_list') }}"><i class="fa fa-list-alt"></i>Lista</a>
                    </li>

                    <li>
                        <a href="{{ route('create_buque_artesanal_sancion_path') }}"><i class="fa fa-pencil"></i>Registrar</a>
                    </li>
                </ul>
            </li>
        </ul>
    </li>
</ul>

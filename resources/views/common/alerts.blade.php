@if(session()->has('alert_success') && session()->has('alert_message'))

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="alert alert-{{session()->get('alert_success') ? 'success' : 'danger'}} alert-dismissible text-center">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{session()->get('alert_message')}}
            </div>
        </div>
    </div>

@endif

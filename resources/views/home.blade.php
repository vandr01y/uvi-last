@extends('layouts.app')
@section('content')

    <div class="row">
        <div class="col-md-12">
            <h2>Usuarios</h2>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-user"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Usuarios Registrados</span>
              <span class="info-box-number">{{ count($users) }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
    </div>

<div class="row">
    <div class="col-md-12">
        <h2>Buques</h2>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-ship"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Buques Registrados</span>
          <span class="info-box-number">{{ count($vessels) }}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
</div>
@endsection

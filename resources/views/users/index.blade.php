@extends('layouts.app')

    @section('page_header')
        @include('common.page_header', ['page_header' => 'Usuarios'])
    @endsection

    @section('content')
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Listado de usuarios</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered table-hover" id="users-table">
                            <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Usuario</th>
                                    <th>E-mail</th>
                                    <th>Rol</th>
                                    <th>Status</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <td>{{ $user->id }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->rol->name }}</td>
                                        <td>{{ $user->status == 1 ? 'Activo' : 'Inactivo' }}</td>
                                        <td>
                                            <a href="{{route('edit_user_path', ['user' => $user->id])}}" class="btn btn-xs btn-info">Editar</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $users->render() }}
                    </div>
                    <!-- /.box-body -->
                </div>
                  <!-- /.box -->
            </div>
        </div>
    @endsection

    @section('plugins_css')
        {{ Html::style('plugins/datatables/dataTables.bootstrap.css') }}
    @endsection
    @section('plugins_js')
        {{ Html::script('plugins/datatables/jquery.dataTables.min.js') }}
        {{ Html::script('plugins/datatables/dataTables.bootstrap.min.js') }}
        <script type="text/javascript">
            $(document).ready(inicio);

            function inicio()
            {
                var last_column = $("#users-table > thead > tr > th").length - 1;

                $("#users-table").DataTable({
                    paging: false,
                    lengthChange: false,
                    searching: true,
                    ordering: true,
                    info: true,
                    autoWidth: false,
                    //columnDefs: [], // Buscar por el atributo alt en las imagenes
                    aoColumnDefs: [{ "bSortable" : false, "aTargets" : [ last_column ] }],
                    language:{
                        emptyTable: "No hay información disponible",
                        info: "Mostrando _START_ a _END_ de _TOTAL_ registros",
                        infoEmpty: "No hay coincidencias",
                        infoFiltered: "(Filtrando de _MAX_ registros totales)",
                        lengthMenu: "Mostrar _MENU_ registros",
                        loadingRecords: "Cargando...",
                        processing: "Procesando...",
                        search: "Buscar:",
                        zeroRecords: "No se encontraron registros",
                        paginate: {
                            first: "Inicio",
                            last: "Final",
                            next: "Siguiente",
                            previous: "Anterior"
                        }
                    }
                });
            }
        </script>
    @endsection

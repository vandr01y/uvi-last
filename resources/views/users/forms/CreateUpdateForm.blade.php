<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                 Ingrese la información
              </h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            @if($user->exists)
                <form class="" action="{{route('update_user_path', ['user' => $user->id])}}" role="form" method="POST" enctype="multipart/form-data">
                    {{ method_field('PUT') }}
            @else
                <form class="" action="{{route('store_user_path')}}" role="form" method="POST" enctype="multipart/form-data">
            @endif
                {{ csrf_field() }}

              <div class="box-body">
                  <input type="hidden" name="" value="{{$user->exists}}">

                  <div class="form-group">
                      <label for="">Nombre:</label>
                      <input type="text" name="name" class="form-control" value="{{$user->exists ? $user->name : old('name')}}">
                  </div>

                  @if(!$user->exists)

                      <div class="form-group">
                          <label for="">Correo:</label>
                          <input type="text" name="email" class="form-control" value="{{$user->exists ? $user->email : old('email')}}">
                      </div>

                      <div class="form-group">
                          <label for="">Contraseña:</label>
                          <input type="password" name="password" class="form-control" value="">
                      </div>

                      <div class="form-group">
                          <label for="">Confirmar Contraseña:</label>
                          <input type="password" name="password_confirmation" class="form-control" value="">
                      </div>

                  @endif

                  <div class="form-group">
                      <label>Rol</label>
                      <select name="rol_id" class="form-control">
                          @foreach ($roles AS $rol)
                                <option value="{{ $rol->id }}" {{ (($user->exists && $user->rol_id == $rol->id) || old('rol_id') == $rol->id) ? "SELECTED" : '' }}>{{ $rol->name }}</option>
                          @endforeach
                      </select>
                  </div>

                  <div class="form-group">
                      <label for="">Imagen</label>
                      <input type="file" name="image" id="user-photo">
                      <p class="help-block">Formatos admitidos: <b>PNG, JPG, JPEG</b>. Tamaño Máximo <b>2MB</b></p>
                      <span id="help-block-error-files" class="help-block"></span>
                  </div>

                  @if ($user->exists && $user->profile_picture != 'default.png')
                      <div class="form-group">
                          <label>Imagen Actual</label><br>
                          {{ Html::image("img/avatars/" . $user->profile_picture, 'Foto de perfil', ['class'=>'img-responsive img-thumbnail'])}}
                      </div>

                      <div class="form-group">
                          <div class="checkbox">
                          <label>
                            <input type="checkbox" name="remove_img" value="1"> Eliminar Imagen
                          </label>
                        </div>
                      </div>
                  @endif

                  <div class="form-group">
                      <label for="">Estado:</label>

                      <div class="radio">
                        <label>
                          <input type="radio" name="status" value="1" {{ ((old('status', null) !== null && old('status') == 1) || (!$user->exists && old('status', null) === null) || ($user->exists && $user->status == 1)) ? "CHECKED" : "" }}>
                          Activo
                        </label>
                      </div>

                      <div class="radio">
                        <label>
                          <input type="radio" name="status" value="0" {{ ((old('status', null) !== null && old('status') == 0) || (old('status', null) === null && $user->exists && $user->status == 0)) ? "CHECKED" : "" }} >
                          Inactivo
                        </label>
                      </div>
                  </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-xs btn-success" id="guardar">Guardar</button>
              </div>
            </form>
        </div>
        <!-- /.box -->
    </div>
</div>
@section('plugins_css')
    <style media="screen">
        #help-block-error-files
        {
            color: #a94442 !important;
        }
    </style>
@endsection
@section('plugins_js')
<script type="text/javascript">
    $(document).ready(inicio);

    function inicio()
    {
        if(window.FileReader)
        {
            $("#user-photo").change(verifyFiles);
        }
    }

    function verifyFiles(e)
    {
        var available_tipes = ['jpg', 'jpeg', 'png'];
        var max_size = 2097152;

        var files = e.originalEvent.target.files;
        var submit_disabled = false;
        var message = '';

        if(files.length > 0)
        {
            $("#guardar").prop('disabled', true);

            var regex_extension = /(?:\.([^.]+))?$/;

            for(var i=0, len=files.length; i<len; i++)
            {
                var n = files[i].name, s = files[i].size, t = files[i].type;

                var ext = regex_extension.exec(n)[1];

                if(typeof ext === 'undefined' || available_tipes.indexOf(ext.toLowerCase()) === -1 || s > max_size)
                {
                    submit_disabled = true;
                    message = 'El archivo: "' + n + '" no cumple con los requisitos, intente quitarlo o cambiarlo';
                    break;
                }
            }
        }

        $("#guardar").prop('disabled', submit_disabled);
        $("#help-block-error-files").text(message);
    }
</script>
@endsection

@extends('layouts.app')
    @section('page_header')
        @include('common.page_header', ['page_header' => 'Registrar Usuario'])
    @endsection

    @section('content')
        @include('common.validation_errors')
        @include('users.forms.CreateUpdateForm', ['user' => $user, 'roles' => $roles])
    @endsection

@extends('layouts.app')

    @section('page_header')
        @include('common.page_header', ['page_header' => 'Buques Artesanales'])
    @endsection

    @section('content')
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Listado de buques artesanales</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered table-hover" id="buques-artesanales-table">
                            <thead>
                                <tr>
                                    <th class="text-center">Nombre</th>
                                    <th class="text-center">País</th>
                                    <th class="text-center">Tipo</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Fotos</th>
                                    <th class="text-center">Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($buques as $buque)
                                    <tr>
                                        <td class="text-center">{{ $buque->nombre }}</td>

                                        <td class="text-center">
                                            @if (isset($buque->ref_pais->name) && !empty($buque->ref_pais->alpha_2) && File::exists('img/flags/' . strtolower($buque->ref_pais->alpha_2) . ".svg"))
                                                {{ Html::image('img/flags/' . strtolower($buque->ref_pais->alpha_2) . '.svg', $buque->ref_pais->name, ['class' => 'img-flag', 'title' => $buque->ref_pais->name]) }}
                                            @else
                                                {{ Html::image('img/flags/default.svg', (isset($buque->ref_pais->name) ? $buque->ref_pais->name : 'País no asignado'), ['class'=>'img-flag', 'title' => isset($buque->ref_pais->name) ? $buque->ref_pais->name : 'País no asignado'])}}
                                            @endif
                                        </td>

                                        <td class="text-center">
                                            @if(isset($buque->ref_tipo_embarcacion->name))
                                                {{ $buque->ref_tipo_embarcacion->name }}
                                            @else
                                                <span class="text-muted">Tipo no asignado</span>
                                            @endif
                                        </td>

                                        <td class="text-center">
                                            <b>{{ $buque->status == 1 ? 'Activo' : 'Inactivo'}}</b>
                                        </td>

                                        <td class="text-center">
                                            @if (count($buque->ba_e_pictures) > 0)
                                                <i class="icon ion-images ion-15x buque-artesanal-photo" data-id="{{ $buque->id }}"></i>
                                            @else
                                                <span class="text-muted">Sin Fotos</span>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            <a href="{{route('edit_buque_artesanal_path', ['buque' => $buque->id])}}" class="btn btn-xs btn-info">Editar</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                  <!-- /.box -->
            </div>
        </div>

        <div class="modal fade" id="modal-img" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Imagenes del buque artesanal: </h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                @foreach ($buques as $buque)
                                    @if (isset($buque->ba_e_pictures) && count($buque->ba_e_pictures) > 0)

                                        <div id="carousel-{{$buque->id}}" class="carousel slide" data-ride="carousel" data-interval="false" style="display: none;">

                                            <ol class="carousel-indicators">
                                                @foreach ($buque->ba_e_pictures as $picture)
                                                    <li data-target="#carousel-{{$buque->id}}" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
                                                @endforeach
                                            </ol>

                                            <div class="carousel-inner">
                                                @foreach ($buque->ba_e_pictures as $picture)
                                                    <div class="item {{ $loop->first ? 'active' : '' }}">
                                                        {{ Html::image($constants['asset_embarcaciones_photos_path'] . $picture->file, 'Foto ' . $loop->iteration) }}
                                                        <div class="carousel-caption">
                                                            Foto {{ $loop->iteration }}
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>

                                            <a class="left carousel-control" href="#carousel-{{$buque->id}}" data-slide="prev">
                                              <span class="fa fa-angle-left"></span>
                                            </a>
                                            <a class="right carousel-control" href="#carousel-{{$buque->id}}" data-slide="next">
                                              <span class="fa fa-angle-right"></span>
                                            </a>
                                        </div>

                                    @endif
                                  @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    @endsection
    @section('plugins_css')
        {{ Html::style('plugins/datatables/dataTables.bootstrap.css') }}
        <style media="screen">
            img.img-flag
            {
                width: 30px;
            }
            i.buque-artesanal-photo
            {
                cursor: pointer;
            }
        </style>
    @endsection
    @section('plugins_js')
        {{ Html::script('plugins/datatables/jquery.dataTables.min.js') }}
        {{ Html::script('plugins/datatables/dataTables.bootstrap.min.js') }}
        {{ Html::script('plugins/datatables/plugins/alt-string.js') }}
        <script type="text/javascript">
            $(document).ready(inicio);

            function inicio()
            {
                var last_column = $("#users-table > thead > tr > th").length - 1;

                $("#buques-artesanales-table").DataTable({
                    lengthChange: false,
                    searching: true,
                    ordering: true,
                    info: true,
                    autoWidth: false,
                    //columnDefs: [], // Buscar por el atributo alt en las imagenes
                    aoColumnDefs: [{ "bSortable" : false, "aTargets" : [ last_column ] }, { type: 'alt-string', targets: 1 }],
                    language: DATATABLES_LAN
                });

                $("#modal-img").on('hidden.bs.modal', function () {
                    $("div[id^='carousel-']").hide();
                });
            }

            $("i.buque-artesanal-photo").click(function(){

                var id = $(this).attr('data-id');
                $("#carousel-" + id).show();
                $("#modal-img").modal('show');

                $("#carousel-"+id+" > ol.carousel-indicators > li, #carousel-"+id+" > div.carousel-inner > div.item").removeClass('active');
                $("#carousel-"+id+" > ol.carousel-indicators > li:first-child, #carousel-"+id+" > div.carousel-inner > div.item:first-child").addClass('active');
            });
        </script>
    @endsection

@extends('layouts.app')
    @section('page_header')
        @include('common.page_header', ['page_header' => 'Registrar Buque Artesanal'])
    @endsection

    @section('content')
        @include('common.validation_errors', ['general' => true])
        @include('buques_artesanales.forms.CreateUpdateForm', ['buque' => $buque])
    @endsection

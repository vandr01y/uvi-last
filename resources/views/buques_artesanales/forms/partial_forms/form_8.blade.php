<div class="tab-pane {{ $active_tab == 8 ? 'active' : '' }}" id="form_8">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Artes y/o métodos de pesca autorizados</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            {{ Form::open(['route' => ['update_buque_artesanal_path', $buque->id], 'method' => 'PUT', 'files' => true]) }}
            {{ csrf_field() }}
            <input type="hidden" name="form_tab" value="8">
            <div class="box-body">

                <div class="form-group">
                    <small class="text-muted"><span class="text-danger">*</span> Campos obligatorios</small>
                </div>

                <div class="form-group {{ $errors->has('tipo_red_id') ? 'has-error' : '' }}">
                    <label for="">Denominación del arte de pesca tipo red</label>
                    {{ Form::select('tipo_red_id', $refs['arte_pesca_tipo_red'], $buque->tipo_red_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%']) }}
                    <span class="help-block">{{ $errors->first('tipo_red_id') }}</span>
                </div>

                <div class="form-group {{ $errors->has('ojo_malla') ? 'has-error' : '' }}">
                    <label for="">Ojo de malla</label>
                    {{ Form::text('ojo_malla', $buque->ojo_malla, ['class'=>'form-control']) }}
                    <span class="help-block">{{ $errors->first('ojo_malla') }}</span>
                </div>

                <div class="form-group {{ $errors->has('ojo_malla_copo') ? 'has-error' : '' }}">
                    <label for="">Ojo de malla del copo</label>
                    {{ Form::text('ojo_malla_copo', $buque->ojo_malla_copo, ['class'=>'form-control']) }}
                    <span class="help-block">{{ $errors->first('ojo_malla_copo') }}</span>
                </div>

                <div class="form-group {{ $errors->has('longitud_red') ? 'has-error' : '' }}">
                    <label for="">Longitud de la red</label>
                    {{ Form::text('longitud_red', $buque->longitud_red, ['class'=>'form-control']) }}
                    <span class="help-block">{{ $errors->first('longitud_red') }}</span>
                </div>

                <div class="form-group {{ $errors->has('ancho_red') ? 'has-error' : '' }}">
                    <label for="">Ancho de la red</label>
                    {{ Form::text('ancho_red', $buque->ancho_red, ['class'=>'form-control']) }}
                    <span class="help-block">{{ $errors->first('ancho_red') }}</span>
                </div>

                <div class="form-group {{ $errors->has('cantidad_panos_piezas') ? 'has-error' : '' }}">
                    <label for="">Cantidad de paños o piezas</label>
                    {{ Form::text('cantidad_panos_piezas', $buque->cantidad_panos_piezas, ['class'=>'form-control']) }}
                    <span class="help-block">{{ $errors->first('cantidad_panos_piezas') }}</span>
                </div>

                <div class="form-group {{ $errors->has('material_red_id') ? 'has-error' : '' }}">
                    <label for="">Material de la red</label>
                    {{ Form::select('material_red_id', $refs['materiales_redes'], $buque->material_red_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%']) }}
                    <span class="help-block">{{ $errors->first('material_red_id') }}</span>
                </div>

                <div class="form-group {{ $errors->has('zonas_pesca_frecuentadas') ? 'has-error' : '' }}">
                    <label for="">Zonas de pesca frecuentadas</label>
                    {{ Form::text('zonas_pesca_frecuentadas', $buque->zonas_pesca_frecuentadas, ['class'=>'form-control']) }}
                    <span class="help-block">{{ $errors->first('zonas_pesca_frecuentadas') }}</span>
                </div>

                <div class="form-group {{ $errors->has('tipos_lineas_mano_id') ? 'has-error' : '' }}">
                    <label for="">Tipos de líneas de mano y afines</label>
                    {{ Form::select('tipos_lineas_mano_id', $refs['lineas_mano'], $buque->tipos_lineas_mano_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%']) }}
                    <span class="help-block">{{ $errors->first('tipos_lineas_mano_id') }}</span>
                </div>

                <div class="form-group {{ $errors->has('trampas_id') ? 'has-error' : '' }}">
                    <label for="">Trampas</label>
                    {{ Form::select('trampas_id', $refs['trampas_nasas'], $buque->trampas_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%']) }}
                    <span class="help-block">{{ $errors->first('trampas_id') }}</span>
                </div>

                <div class="form-group {{ $errors->has('cantidad_trampas') ? 'has-error' : '' }}">
                    <label for="">Cantidad de trampas</label>
                    {{ Form::text('cantidad_trampas', $buque->cantidad_trampas, ['class'=>'form-control']) }}
                    <span class="help-block">{{ $errors->first('cantidad_trampas') }}</span>
                </div>

                <div class="form-group {{ $errors->has('material_principal_trampas_id') ? 'has-error' : '' }}">
                    <label for="">Material principal de la trampa</label>
                    {{ Form::select('material_principal_trampas_id', $refs['materiales_trampas_nasas'], $buque->material_principal_trampas_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%']) }}
                    <span class="help-block">{{ $errors->first('material_principal_trampas_id') }}</span>
                </div>

                <div class="form-group {{ $errors->has('zonas_pesca_trampas') ? 'has-error' : '' }}">
                    <label for="">Zonas de pesca frecuentadas con trampas</label>
                    {{ Form::text('zonas_pesca_trampas', $buque->zonas_pesca_trampas, ['class'=>'form-control']) }}
                    <span class="help-block">{{ $errors->first('zonas_pesca_trampas') }}</span>
                </div>

                <div class="form-group {{ $errors->has('tipos_artefactos_herir_aferrar_id') ? 'has-error' : '' }}">
                    <label for="">Tipo de artefactos de herir o aferrar</label>
                    {{ Form::select('tipos_artefactos_herir_aferrar_id', $refs['tipos_artefactos'], $buque->tipos_artefactos_herir_aferrar_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%']) }}
                    <span class="help-block">{{ $errors->first('tipos_artefactos_herir_aferrar_id') }}</span>
                </div>

                <div class="form-group {{ $errors->has('cantidad_artefactos') ? 'has-error' : '' }}">
                    <label for="">Cantidad de artefactos</label>
                    {{ Form::text('cantidad_artefactos', $buque->cantidad_artefactos, ['class'=>'form-control']) }}
                    <span class="help-block">{{ $errors->first('cantidad_artefactos') }}</span>
                </div>

                <div class="form-group {{ $errors->has('material_artefacto_id') ? 'has-error' : '' }}">
                    <label for="">Material del artefacto</label>
                    {{ Form::select('material_artefacto_id', $refs['materiales_artefactos'], $buque->material_artefacto_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%']) }}
                    <span class="help-block">{{ $errors->first('material_artefacto_id') }}</span>
                </div>

                <div class="form-group {{ $errors->has('zonas_pesca_artefactos') ? 'has-error' : '' }}">
                    <label for="">Zonas de pesca frecuentadas con artefactos de herir o aferrar</label>
                    {{ Form::text('zonas_pesca_artefactos', $buque->zonas_pesca_artefactos, ['class'=>'form-control']) }}
                    <span class="help-block">{{ $errors->first('zonas_pesca_artefactos') }}</span>
                </div>

                <div class="form-group {{ $errors->has('otros') ? 'has-error' : '' }}">
                    <label for="">Otros</label>
                    <div class="radio">
                      <label> {{ Form::radio('otros', '1', (is_null($buque->otros) || $buque->otros == 1)) }} Sí </label>
                    </div>

                    <div class="radio">
                      <label> {{ Form::radio('otros', '0', (is_numeric($buque->otros) && $buque->otros == 0)) }} No </label>
                    </div>
                    <span class="help-block">{{ $errors->first('otros') }}</span>
                </div>

                <div class="form-group {{ $errors->has('artes_metodos_poco_convencionales') ? 'has-error' : '' }}">
                    <label for="">Artes y/o métodos de pesca poco convencionales</label>
                    {{ Form::text('artes_metodos_poco_convencionales', $buque->artes_metodos_poco_convencionales, ['class'=>'form-control']) }}
                    <span class="help-block">{{ $errors->first('artes_metodos_poco_convencionales') }}</span>
                </div>

                <div class="form-group {{ $errors->has('cantidad_artes_metodos_poco_convencionales') ? 'has-error' : '' }}">
                    <label for="">Cantidad de artes de pesca poco convencionales</label>
                    {{ Form::text('cantidad_artes_metodos_poco_convencionales', $buque->cantidad_artes_metodos_poco_convencionales, ['class'=>'form-control']) }}
                    <span class="help-block">{{ $errors->first('cantidad_artes_metodos_poco_convencionales') }}</span>
                </div>

                <div class="form-group {{ $errors->has('uso_fad') ? 'has-error' : '' }}">
                    <label for="">Uso de Dispositivos Agregadores de Peces FAD</label>
                    <div class="radio">
                      <label> {{ Form::radio('uso_fad', '1', (is_null($buque->uso_fad) || $buque->uso_fad == 1)) }} Sí </label>
                    </div>

                    <div class="radio">
                      <label> {{ Form::radio('uso_fad', '0', (is_numeric($buque->uso_fad) && $buque->uso_fad == 0)) }} No </label>
                    </div>
                    <span class="help-block">{{ $errors->first('uso_fad') }}</span>
                </div>

                <div class="form-group {{ $errors->has('tipos_fad_id') ? 'has-error' : '' }}">
                    <label for="">Tipo de FAD utilizados</label>
                    {{ Form::select('tipos_fad_id', $refs['tipos_fad'], $buque->tipos_fad_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%']) }}
                    <span class="help-block">{{ $errors->first('tipos_fad_id') }}</span>
                </div>

                <div class="form-group {{ $errors->has('cantidad_fad') ? 'has-error' : '' }}">
                    <label for="">Cantidad de FAD utilizados</label>
                    {{ Form::text('cantidad_fad', $buque->cantidad_fad, ['class'=>'form-control']) }}
                    <span class="help-block">{{ $errors->first('cantidad_fad') }}</span>
                </div>

                <div class="form-group {{ count($errors->get('photos_f8.*')) > 0 ? 'has-error' : '' }}">
                    <label for="">Fotografías</label>
                    {{ Form::file('photos_f8[]', ['id' => 'buque-artesanal-artes-files', 'class' => 'form-control', 'multiple' => 'true']) }}
                    <p class="help-block text-muted">Formatos admitidos: PNG, JPG, JPEG. Tamaño Máximo: 2MB</p>
                    <span class="help-block text-danger" id="buque-artesanal-artes-files-help-block"></span>
                    <span class="help-block">
                        @foreach($errors->get('photos_f8.*') as $messages)
                            @foreach($messages as $message)
                                {{ $message }}
                            @endforeach
                        @endforeach
                    </span>
                    @if($buque->exists && count($buque->ba_a_pictures) > 0)
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Imágenes actuales: </label>

                                    <div class="row">
                                    @foreach ($buque->ba_a_pictures AS $picture)
                                        <div class="col-md-3 text-center buque-artesanal-arte-img-container" data-id='{{ $picture->id }}'>
                                            {{ Html::image($constants['asset_artes_photos_path'] . $picture->file, 'Imagen de Arte de Pesca', ['class' => 'img-responsive img-rounded'])}}
                                            <span class="btn btn-xs btn-danger delete-buques-artesanales-img" data-id='{{ $picture->id }}' data-form="8"><i class="fa fa-trash"></i></span>
                                        </div>
                                    @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success" id="submit-f8">Guardar</button><br>
              <small class="text-muted"><span id="f8-total-files">0</span> Archivo(s) Seleccionado(s)</small>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

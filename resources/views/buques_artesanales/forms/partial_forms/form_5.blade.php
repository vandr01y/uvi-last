<div class="tab-pane {{ $active_tab == 5 ? 'active' : '' }}" id="form_5">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Propulsión de la embarcación</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            {{ Form::open(['route' => ['update_buque_artesanal_path', $buque->id], 'method' => 'PUT']) }}
            {{ csrf_field() }}
            <input type="hidden" name="form_tab" value="5">
            <div class="box-body">

                <div class="form-group">
                    <small class="text-muted"><span class="text-danger">*</span> Campos obligatorios</small>
                </div>

                <div class="form-group {{ $errors->has('tipo_propulsion_id') ? 'has-error' : '' }}">
                    <label for=""><small class="text-danger">*</small> Tipo de propulsión</label>
                    {{ Form::select('tipo_propulsion_id', $refs['tipos_propulsion'], $buque->tipo_propulsion_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%']) }}
                    <span class="help-block">{{ $errors->first('tipo_propulsion_id') }}</span>
                </div>

                <div class="form-group {{ $errors->has('tipo_motor_id') ? 'has-error' : '' }}">
                    <label for="">Tipo del motor</label>
                    {{ Form::select('tipo_motor_id', $refs['tipos_motor'], $buque->tipo_motor_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%']) }}
                    <span class="help-block">{{ $errors->first('tipo_motor_id') }}</span>
                </div>

                <div class="form-group {{ $errors->has('marca_motor') ? 'has-error' : '' }}">
                    <label for="">Marca del motor</label>
                    {{ Form::text('marca_motor', $buque->marca_motor, ['class'=>'form-control']) }}
                    <span class="help-block">{{ $errors->first('marca_motor') }}</span>
                </div>

                <div class="form-group {{ $errors->has('potencia_motor') ? 'has-error' : '' }}">
                    <label for="">Potencia del motor</label>
                    {{ Form::text('potencia_motor', $buque->potencia_motor, ['class'=>'form-control']) }}
                    <span class="help-block">{{ $errors->first('potencia_motor') }}</span>
                </div>

                <div class="form-group {{ $errors->has('numero_motor') ? 'has-error' : '' }}">
                    <label for="">Número del motor</label>
                    {{ Form::text('numero_motor', $buque->numero_motor, ['class'=>'form-control']) }}
                    <span class="help-block">{{ $errors->first('numero_motor') }}</span>
                </div>

                <div class="form-group {{ $errors->has('tipo_combustible') ? 'has-error' : '' }}">
                    <label for="">Tipo de combustible</label>
                    {{ Form::text('tipo_combustible', $buque->tipo_combustible, ['class'=>'form-control']) }}
                    <span class="help-block">{{ $errors->first('tipo_combustible') }}</span>
                </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

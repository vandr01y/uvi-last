<div class="tab-pane {{ $active_tab == 3 ? 'active' : '' }}" id="form_3">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Identificación Regional</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            {{ Form::open(['route' => ['update_buque_artesanal_path', $buque->id], 'method' => 'PUT']) }}
            {{ csrf_field() }}
            <input type="hidden" name="form_tab" value="3">
            <div class="box-body">

                <div class="form-group">
                    <small class="text-muted"><span class="text-danger">*</span> Campos obligatorios</small>
                </div>

                <div class="form-group {{ $errors->has('eslora') ? 'has-error' : '' }}">
                    <label for=""><small class="text-danger">*</small> Eslora</label>
                    {{ Form::text('eslora', $buque->eslora, ['class'=>'form-control']) }}
                    <span class="help-block">{{ $errors->first('eslora') }}</span>
                </div>

                <div class="form-group {{ $errors->has('manga') ? 'has-error' : '' }}">
                    <label for=""><small class="text-danger">*</small> Manga</label>
                    {{ Form::text('manga', $buque->manga, ['class'=>'form-control']) }}
                    <span class="help-block">{{ $errors->first('manga') }}</span>
                </div>

                <div class="form-group {{ $errors->has('puntal') ? 'has-error' : '' }}">
                    <label for=""><small class="text-danger">*</small> Puntal</label>
                    {{ Form::text('puntal', $buque->puntal, ['class'=>'form-control']) }}
                    <span class="help-block">{{ $errors->first('puntal') }}</span>
                </div>

                <div class="form-group {{ $errors->has('calado') ? 'has-error' : '' }}">
                    <label for="">Calado</label>
                    {{ Form::text('calado', $buque->calado, ['class'=>'form-control']) }}
                    <span class="help-block">{{ $errors->first('calado') }}</span>
                </div>

                <div class="form-group {{ $errors->has('trn') ? 'has-error' : '' }}">
                    <label for="">Tonelaje de Registro Neto (TRN)</label>
                    {{ Form::text('trn', $buque->trn, ['class'=>'form-control']) }}
                    <span class="help-block">{{ $errors->first('trn') }}</span>
                </div>

                <div class="form-group {{ $errors->has('trb') ? 'has-error' : '' }}">
                    <label for="">Tonelaje de Registro Bruto (TRB)</label>
                    {{ Form::text('trb', $buque->trb, ['class'=>'form-control']) }}
                    <span class="help-block">{{ $errors->first('trb') }}</span>
                </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

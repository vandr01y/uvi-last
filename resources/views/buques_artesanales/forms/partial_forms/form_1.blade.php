<div class="tab-pane {{ $active_tab == 1 ? 'active' : '' }}" id="form_1">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Identificación</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            @if($buque->exists)
                {{ Form::open(['route' => ['update_buque_artesanal_path', $buque->id], 'method' => 'PUT']) }}
            @else
                {{ Form::open(['route' => 'store_buque_artesanal_path', 'method' => 'POST']) }}
            @endif
                {{ csrf_field() }}
            <input type="hidden" name="form_tab" value="1">
            <div class="box-body">
                <div class="form-group">
                    <small class="text-muted"><span class="text-danger">*</span> Campos obligatorios</small>
                </div>

                <div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
                    <label for=""><small class="text-danger">*</small> Nombre de la embarcación</label>
                    {{ Form::text('nombre', $buque->nombre, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('nombre') }}</span>
                </div>

                <div class="form-group {{ $errors->has('matricula') ? 'has-error' : '' }}">
                    <label for="">Matrícula de la embarcación</label>
                    {{ Form::text('matricula', $buque->matricula, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('matricula') }}</span>
                </div>

                <div class="form-group {{ $errors->has('numero_identificacion_embarcacion') ? 'has-error' : '' }}">
                    <label for="">Número de identificación de la embarcación</label>
                    {{ Form::text('numero_identificacion_embarcacion', $buque->numero_identificacion_embarcacion, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('numero_identificacion_embarcacion') }}</span>
                </div>

                <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                    <label for="">Status</label>
                    <div class="radio">
                      <label> {{ Form::radio('status', '1', (is_null($buque->status) || $buque->status == 1)) }} Activo </label>
                    </div>

                    <div class="radio">
                      <label> {{ Form::radio('status', '0', (is_numeric($buque->status) && $buque->status == 0)) }} Inactivo </label>
                    </div>
                    <span class="help-block {{ $errors->first('status') }}"></span>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

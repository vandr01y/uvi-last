<div class="tab-pane {{ $active_tab == 7 ? 'active' : '' }}" id="form_7">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Equipos de navegación y comunicación</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            {{ Form::open(['route' => ['update_buque_artesanal_path', $buque->id], 'method' => 'PUT']) }}
            {{ csrf_field() }}
            <input type="hidden" name="form_tab" value="7">
            <div class="box-body">

                <div class="form-group">
                    <small class="text-muted"><span class="text-danger">*</span> Campos obligatorios</small>
                </div>

                <div class="form-group {{ $errors->has('radio_comunicaciones') ? 'has-error' : '' }}">
                    <label for=""><small class="text-danger">*</small> Radio de comunicaciones</label>
                    <div class="radio">
                      <label> {{ Form::radio('radio_comunicaciones', '1', (is_null($buque->radio_comunicaciones) || $buque->radio_comunicaciones == 1)) }} Sí </label>
                    </div>

                    <div class="radio">
                      <label> {{ Form::radio('radio_comunicaciones', '0', (is_numeric($buque->radio_comunicaciones) && $buque->radio_comunicaciones == 0)) }} No </label>
                    </div>
                    <span class="help-block">{{ $errors->first('radio_comunicaciones') }}</span>
                </div>

                <div class="form-group {{ $errors->has('marca_radio_comunicaciones') ? 'has-error' : '' }}">
                    <label for="">Marca radio de comunicaciones</label>
                    {{ Form::text('marca_radio_comunicaciones', $buque->marca_radio_comunicaciones, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('marca_radio_comunicaciones') }}</span>
                </div>

                <div class="form-group {{ $errors->has('frecuencia_asignada') ? 'has-error' : '' }}">
                    <label for="">Especificar la frecuencia</label>
                    {{ Form::text('frecuencia_asignada', $buque->frecuencia_asignada, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('frecuencia_asignada') }}</span>
                </div>

                <div class="form-group {{ $errors->has('brujula') ? 'has-error' : '' }}">
                    <label for="">Brújula</label>
                    <div class="radio">
                      <label> {{ Form::radio('brujula', '1', (is_null($buque->brujula) || $buque->brujula == 1)) }} Sí </label>
                    </div>

                    <div class="radio">
                      <label> {{ Form::radio('brujula', '0', (is_numeric($buque->brujula) && $buque->brujula == 0)) }} No </label>
                    </div>
                    <span class="help-block">{{ $errors->first('brujula') }}</span>
                </div>

                <div class="form-group {{ $errors->has('marca_brujula') ? 'has-error' : '' }}">
                    <label for="">Marca de la brújjula</label>
                    {{ Form::text('marca_brujula', $buque->marca_brujula, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('marca_brujula') }}</span>
                </div>

                <div class="form-group {{ $errors->has('gps') ? 'has-error' : '' }}">
                    <label for="">GPS</label>
                    <div class="radio">
                      <label> {{ Form::radio('gps', '1', (is_null($buque->gps) || $buque->gps == 1)) }} Sí </label>
                    </div>

                    <div class="radio">
                      <label> {{ Form::radio('gps', '0', (is_numeric($buque->gps) && $buque->gps == 0)) }} No </label>
                    </div>
                    <span class="help-block">{{ $errors->first('gps') }}</span>
                </div>

                <div class="form-group {{ $errors->has('marca_gps') ? 'has-error' : '' }}">
                    <label for="">Marca del GPS</label>
                    {{ Form::text('marca_gps', $buque->marca_gps, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('marca_gps') }}</span>
                </div>

                <div class="form-group {{ $errors->has('chalecos_salvavidas') ? 'has-error' : '' }}">
                    <label for=""><small class="text-danger">*</small> Chalecos salvavidas</label>
                    <div class="radio">
                      <label> {{ Form::radio('chalecos_salvavidas', '1', (is_null($buque->chalecos_salvavidas) || $buque->chalecos_salvavidas == 1)) }} Sí </label>
                    </div>

                    <div class="radio">
                      <label> {{ Form::radio('chalecos_salvavidas', '0', (is_numeric($buque->chalecos_salvavidas) && $buque->chalecos_salvavidas == 0)) }} No </label>
                    </div>
                    <span class="help-block">{{ $errors->first('chalecos_salvavidas') }}</span>
                </div>

                <div class="form-group {{ $errors->has('numero_chalecos_salvavidas') ? 'has-error' : '' }}">
                    <label for="">Número de chalecos salvavidas</label>
                    {{ Form::text('numero_chalecos_salvavidas', $buque->numero_chalecos_salvavidas, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('numero_chalecos_salvavidas') }}</span>
                </div>

                <div class="form-group {{ $errors->has('estado_condicion_chalecos_salvavidas_id') ? 'has-error' : '' }}">
                    <label for="">Estado de la condición de los chalecos salvavidas</label>
                    {{ Form::select('estado_condicion_chalecos_salvavidas_id', $refs['chalecos_salvavidas_estados'], $buque->estado_condicion_chalecos_salvavidas_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%']) }}
                    <span class="help-block">{{ $errors->first('estado_condicion_chalecos_salvavidas_id') }}</span>
                </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

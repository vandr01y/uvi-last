<div class="tab-pane {{ $active_tab == 4 ? 'active' : '' }}" id="form_4">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Identificación Regional</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            {{ Form::open(['route' => ['update_buque_artesanal_path', $buque->id], 'method' => 'PUT']) }}
            {{ csrf_field() }}
            <input type="hidden" name="form_tab" value="4">
            <div class="box-body">

                <div class="form-group">
                    <small class="text-muted"><span class="text-danger">*</span> Campos obligatorios</small>
                </div>

                <div class="form-group {{ $errors->has('anio_construccion') ? 'has-error' : '' }}">
                    <label for="">Año de construcción</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        {{ Form::text('anio_construccion', $buque->anio_construccion, ['class' => 'form-control pull-right datepicker', 'readonly' => 'true'])}}
                    </div>

                    <span class="help-block">{{ $errors->first('anio_construccion') }}</span>
                </div>

                <div class="form-group {{ $errors->has('lugar_construccion') ? 'has-error' : '' }}">
                    <label for="">Lugar de construcción</label>
                    {{ Form::text('lugar_construccion', $buque->lugar_construccion, ['class'=>'form-control']) }}
                    <span class="help-block">{{ $errors->first('lugar_construccion') }}</span>
                </div>

                <div class="form-group {{ $errors->has('astillero_construccion') ? 'has-error' : '' }}">
                    <label for="">Astillero donde se construyó</label>
                    {{ Form::text('astillero_construccion', $buque->astillero_construccion, ['class'=>'form-control']) }}
                    <span class="help-block">{{ $errors->first('astillero_construccion') }}</span>
                </div>

                <div class="form-group {{ $errors->has('carpintero_construccion') ? 'has-error' : '' }}">
                    <label for="">Carpintero que la construyó</label>
                    {{ Form::text('carpintero_construccion', $buque->carpintero_construccion, ['class'=>'form-control']) }}
                    <span class="help-block">{{ $errors->first('carpintero_construccion') }}</span>
                </div>

                <div class="form-group {{ $errors->has('material_embarcacion_id') ? 'has-error' : '' }}">
                    <label for=""><small class="text-danger">*</small> Material de la embarcación</label>
                    {{ Form::select('material_embarcacion_id', $refs['hull_materials'], $buque->material_embarcacion_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%']) }}
                    <span class="help-block">{{ $errors->first('material_embarcacion_id') }}</span>
                </div>


            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

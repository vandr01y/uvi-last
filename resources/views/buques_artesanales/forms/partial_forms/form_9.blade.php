<div class="tab-pane {{ $active_tab == 9 ? 'active' : '' }}" id="form_9">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Fotografias de la embarcación</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            {{ Form::open(['route' => ['update_buque_artesanal_path', $buque->id], 'method' => 'PUT', 'files' => true]) }}
            {{ csrf_field() }}
            <input type="hidden" name="form_tab" value="9">
            <div class="box-body">
                <div class="form-group {{ count($errors->get('photos_f9.*')) > 0 ? 'has-error' : '' }}">
                    <label for="">Fotografías</label>
                    {{ Form::file('photos_f9[]', ['id' => 'buque-artesanal-files', 'class' => 'form-control', 'multiple' => 'true']) }}
                    <p class="help-block text-muted">Formatos admitidos: PNG, JPG, JPEG. Tamaño Máximo: 2MB</p>
                    <span class="help-block text-danger" id="buque-artesanal-files-help-block"></span>
                    <span class="help-block">
                        @foreach($errors->get('photos_f9.*') as $messages)
                            @foreach($messages as $message)
                                {{ $message }}
                            @endforeach
                        @endforeach
                    </span>
                    @if($buque->exists && count($buque->ba_e_pictures) > 0)
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Imágenes actuales: </label>

                                    <div class="row">
                                    @foreach ($buque->ba_e_pictures AS $picture)
                                        <div class="col-md-3 text-center buque-artesanal-img-container" data-id='{{ $picture->id }}'>
                                            {{ Html::image($constants['asset_embarcaciones_photos_path'] . $picture->file, 'Imagen de Arte de Pesca', ['class' => 'img-responsive img-rounded'])}}
                                            <span class="btn btn-xs btn-danger delete-buques-artesanales-img" data-id='{{ $picture->id }}'><i class="fa fa-trash"></i></span>
                                        </div>
                                    @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success" id="submit-f9">Guardar</button><br>
              <small class="text-muted"><span id="f9-total-files">0</span> Archivos Seleccionados</small>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

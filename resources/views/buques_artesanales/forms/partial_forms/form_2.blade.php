<div class="tab-pane {{ $active_tab == 2 ? 'active' : '' }}" id="form_2">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Identificación Regional</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            {{ Form::open(['route' => ['update_buque_artesanal_path', $buque->id], 'method' => 'PUT']) }}
            {{ csrf_field() }}
            <input type="hidden" name="form_tab" value="2">
            <div class="box-body">
                <div class="form-group">
                    <small class="text-muted"><span class="text-danger">*</span> Campos obligatorios</small>
                </div>

                <div class="form-group {{ $errors->has('bandera_id') ? 'has-error' : '' }}">
                    <label for="">Bandera</label>
                    {{ Form::select('bandera_id', $refs['countries'], $buque->bandera_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%']) }}
                    <span class="help-block">{{ $errors->first('bandera_id') }}</span>
                </div>

                <div class="form-group {{ $errors->has('puerto_id') ? 'has-error' : '' }}">
                    <label for="">Puerto de Registro</label>
                    {{ Form::select('puerto_id', $refs['registration_ports'], $buque->puerto_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%']) }}
                    <span class="help-block">{{ $errors->first('puerto_id') }}</span>
                </div>

                <div class="form-group {{ $errors->has('tipo_id') ? 'has-error' : '' }}">
                    <label for="">Tipo de embarcación</label>
                    {{ Form::select('tipo_id', $refs['tipo_embarcacion'], $buque->tipo_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%']) }}
                    <span class="help-block">{{ $errors->first('tipo_id') }}</span>
                </div>

                <div class="form-group {{ $errors->has('estado_operacional') ? 'has-error' : '' }}">
                    <label for=""><small class="text-danger">*</small> Estado operacional</label>

                    <div class="radio">
                      <label> {{ Form::radio('estado_operacional', '1', (is_null($buque->estado_operacional) || $buque->estado_operacional == 1)) }} Sí </label>
                    </div>

                    <div class="radio">
                      <label> {{ Form::radio('estado_operacional', '0', (is_numeric($buque->estado_operacional) && $buque->estado_operacional == 0)) }} No </label>
                    </div>

                    <span class="help-block">{{ $errors->first('estado_operacional') }}</span>
                </div>

                <div class="form-group {{ $errors->has('cuenca_id') ? 'has-error' : '' }}">
                    <label for=""><small class="text-danger">*</small> Cuenca donde opera</label>
                    {{ Form::select('cuenca_id', $refs['cuencas'], $buque->cuenca_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%']) }}
                    <span class="help-block">{{ $errors->first('cuenca_id') }}</span>
                </div>

                <div class="form-group {{ $errors->has('departamento_id') ? 'has-error' : '' }}">
                    <label for=""><small class="text-danger">*</small> Departamento donde opera</label>
                    {{ Form::select('departamento_id', $refs['colombia_departamentos'], $buque->departamento_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%']) }}
                    <span class="help-block">{{ $errors->first('departamento_id') }}</span>
                </div>

                <div class="form-group {{ $errors->has('municipio_id') ? 'has-error' : '' }}">
                    <label for=""><small class="text-danger">*</small> Municipio donde opera</label>
                    {{ Form::select('municipio_id', $refs['colombia_municipios'], $buque->municipio_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%']) }}
                    <span class="help-block">{{ $errors->first('municipio_id') }}</span>
                </div>

                <div class="form-group {{ $errors->has('localidad') ? 'has-error' : '' }}">
                    <label for=""><small class="text-danger">*</small> Localidad donde opera</label>
                    {{ Form::text('localidad', $buque->localidad, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('localidad') }}</span>
                </div>

                <div class="form-group {{ $errors->has('lugar_desembarco') ? 'has-error' : '' }}">
                    <label for=""><small class="text-danger">*</small> Lugar de desembarco</label>
                    {{ Form::text('lugar_desembarco', $buque->lugar_desembarco, ['class' => 'form-control']) }}
                    <!--{{ Form::select('lugar_desembarco_id', $refs['X'], $buque->lugar_desembarco_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2', 'style' => 'width: 100%']) }}-->
                    <span class="help-block">{{ $errors->first('lugar_desembarco') }}</span>
                </div>

                <div class="form-group {{ $errors->has('permiso_pesca') ? 'has-error' : '' }}">
                    <label for=""><small class="text-danger">*</small> Permiso de pesca</label>

                    <div class="radio">
                      <label> {{ Form::radio('permiso_pesca', '1', (is_null($buque->permiso_pesca) || $buque->permiso_pesca == 1)) }} Sí </label>
                    </div>

                    <div class="radio">
                      <label> {{ Form::radio('permiso_pesca', '0', (is_numeric($buque->permiso_pesca) && $buque->permiso_pesca == 0)) }} No </label>
                    </div>

                    <span class="help-block">{{ $errors->first('permiso_pesca') }}</span>
                </div>

                <div class="form-group {{ $errors->has('numero_resolucion') ? 'has-error' : '' }}">
                    <label for="">Número de Resolución</label>
                    {{ Form::text('numero_resolucion', $buque->numero_resolucion, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('numero_resolucion') }}</span>
                </div>

                <div class="form-group {{ $errors->has('vigencia_permiso') ? 'has-error' : '' }}">
                    <label for="">Vigencia del permiso (años)</label>
                    {{ Form::text('vigencia_permiso', $buque->vigencia_permiso, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('vigencia_permiso') }}</span>
                </div>

                <div class="form-group {{ $errors->has('fecha_inicio') ? 'has-error' : '' }}">
                    <label for="">Fecha de Inicio</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        {{ Form::text('fecha_inicio', $buque->fecha_inicio, ['class' => 'form-control pull-right datepicker', 'readonly' => 'true'])}}
                    </div>
                    <span class="help-block">{{ $errors->first('fecha_inicio') }}</span>
                </div>

                <div class="form-group {{ $errors->has('fecha_finalizacion') ? 'has-error' : '' }}">
                    <label for="">Fecha de finalización</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        {{ Form::text('fecha_finalizacion', $buque->fecha_finalizacion, ['class' => 'form-control pull-right datepicker', 'readonly' => 'true'])}}
                    </div>
                    <span class="help-block">{{ $errors->first('fecha_finalizacion') }}</span>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

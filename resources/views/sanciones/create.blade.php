@extends('layouts.app')
    @section('page_header')
        @include('common.page_header', ['page_header' => 'Registrar Sanción - Buque Industrisal'])
    @endsection

    @section('content')
        @include('common.validation_errors', ['general' => true])
        @include('sanciones.forms.CreateUpdateForm', ['sancion' => $sancion, 'refs' => $refs])
    @endsection

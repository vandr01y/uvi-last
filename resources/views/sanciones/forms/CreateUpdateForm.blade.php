<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="box box-primary">
            @if($sancion->exists)
                {{ Form::open(['route' => ['update_vessel_sancion_path', $sancion->id], 'method' => 'PUT', 'files' => true]) }}
            @else
                {{ Form::open(['route' => 'store_vessel_sancion_path', 'method' => 'POST', 'files' => true]) }}
            @endif
                {{ csrf_field() }}


            <div class="box-header with-border">
            </div>

            <div class="box-body">
                <div role="form">
                    <div class="form-group {{ $errors->has('vessel_id') ? 'has-error' : '' }}">
                        <label for=""><span class="text-danger">*</span> Buque</label>
                        {{ Form::select('vessel_id', $refs['vessels'], $sancion->vessel_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2']) }}
                        <span class="help-block">{{ $errors->first('vessel_id') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('presunta_violacion') ? 'has-error' : '' }}">
                        <label for=""><span class="text-danger">*</span> ¿La motonave ha tenido presunta violación al estatuto pesquero?</label>
                        <div class="radio">
                          <label> {{ Form::radio('presunta_violacion', '1', (!$sancion->exists || ($sancion->exists && $sancion->presunta_violacion == 1))) }} Sí </label>
                        </div>

                        <div class="radio">
                          <label> {{ Form::radio('presunta_violacion', '0', ($sancion->exists && $sancion->presunta_violacion == 0)) }} No </label>
                        </div>
                        <span class="help-block">{{ $errors->first('presunta_violacion') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('nur') ? 'has-error' : '' }}">
                        <label for=""><span class="text-danger">*</span> Número único de registro - NUR</label>
                        {{ Form::text('nur', $sancion->nur, ['class' => 'form-control']) }}
                        <span class="help-block">{{ $errors->first('nur') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('infraccion_proceso_id') ? 'has-error' : '' }}">
                        <label for=""><span class="text-danger">*</span> Infracción en proceso de investigación</label>
                        {{ Form::select('infraccion_proceso_id', $refs['sanciones_tipos_infracciones'], $sancion->infraccion_proceso_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2']) }}
                        <span class="help-block">{{ $errors->first('infraccion_proceso_id') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('sanciones_estatuto_pesquero') ? 'has-error' : '' }}">
                        <label for=""><span class="text-danger">*</span> Sanciones al estatuto pesquero</label>
                        <div class="radio">
                          <label> {{ Form::radio('sanciones_estatuto_pesquero', '1', (!$sancion->exists || ($sancion->exists && $sancion->sanciones_estatuto_pesquero == 1))) }} Sí </label>
                        </div>

                        <div class="radio">
                          <label> {{ Form::radio('sanciones_estatuto_pesquero', '0', ($sancion->exists && $sancion->sanciones_estatuto_pesquero == 0)) }} No </label>
                        </div>
                        <span class="help-block">{{ $errors->first('sanciones_estatuto_pesqu') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('numero_acto_administrativo') ? 'has-error' : '' }}">
                        <label for=""><span class="text-danger">*</span> Número acto adminsitrativo mediante el cual se sanciono violacion al estatuto pesquero (No. xxx).</label>
                        {{ Form::text('numero_acto_administrativo', $sancion->numero_acto_administrativo, ['class' => 'form-control']) }}
                        <span class="help-block">{{ $errors->first('numero_acto_administrativo') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('fecha_sancion') ? 'has-error' : '' }}">
                        <label for=""><span class="text-danger">*</span> Fecha de la sanción</label>

                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            {{ Form::text('fecha_sancion', $sancion->fecha_sancion, ['class' => 'form-control pull-right datepicker', 'readonly' => 'true'])}}
                        </div>
                        <span class="help-block">{{ $errors->first('fecha_sancion') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('infraccion_sancionada_id') ? 'has-error' : '' }}">
                        <label for="">Infracción sancionada</label>
                        {{ Form::select('infraccion_sancionada_id', $refs['sanciones_tipos_infracciones'], $sancion->infraccion_sancionada_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2']) }}
                        <span class="help-block">{{ $errors->first('infraccion_sancionada_id') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('tipo_sancion_id') ? 'has-error' : '' }}">
                        <label for="">Tipo Sanción</label>
                        {{ Form::select('tipo_sancion_id', $refs['sanciones_tipos'], $sancion->tipo_sancion_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2']) }}
                        <span class="help-block">{{ $errors->first('tipo_sancion_id') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('fallo_ejecutoriado_id') ? 'has-error' : '' }}">
                        <label for="">Fallo ejecutoriado o con recurso</label>
                        {{ Form::select('fallo_ejecutoriado_id', $refs['sanciones_fallos_ejecutoriados'], $sancion->fallo_ejecutoriado_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2']) }}
                        <span class="help-block">{{ $errors->first('fallo_ejecutoriado_id') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('valor_multa') ? 'has-error' : '' }}">
                        <label for=""><span class="text-danger">*</span> Valor de la multa</label>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-dollar"></i>
                            </div>
                            {{ Form::text('valor_multa', $sancion->valor_multa, ['class' => 'form-control pull-right'])}}
                        </div>
                        <span class="help-block">{{ $errors->first('valor_multa') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('estado_sancion_id') ? 'has-error' : '' }}">
                        <label for=""><span class="text-danger">*</span> Estado de la sanción</label>
                        {{ Form::select('estado_sancion_id', $refs['sanciones_estados'], $sancion->estado_sancion_id, ['placeholder' => 'Seleccione', 'class' => 'form-control select2']) }}
                        <span class="help-block">{{ $errors->first('estado_sancion_id') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('detalles') ? 'has-error' : '' }}">
                        <label for="">Detalles u observación</label>
                        {{ Form::textarea('detalles', $sancion->detalles, ['class' => 'form-control', 'rows' => 3]) }}
                        <span class="help-block">{{ $errors->first('detalles') }}</span>
                    </div>

                    <div class="form-group {{ count($errors->get('attachments.*')) > 0 ? 'has-error' : '' }}">
                        <label for="">Archivos Adjuntos</label>
                        {{ Form::file('attachments[]', ['id' => 'attachments', 'class' => 'form-control', 'multiple' => 'true']) }}
                        <p class="help-block text-muted">Formatos admitidos: PDF. Tamaño Máximo: 5MB</p>
                        <span class="help-block text-danger" id="help-block-error-files"></span>
                        <span class="help-block">
                            @foreach($errors->get('attachments.*') as $messages)
                                @foreach($messages as $message)
                                    {{ $message }}
                                @endforeach
                            @endforeach
                        </span>

                        @if (count($sancion->rel_files) > 0)
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Archivos actuales: </label>

                                        <div class="row">
                                        @foreach ($sancion->rel_files AS $file)
                                            <div class="col-md-3 text-center vessel-sancion-attachment-container" data-id='{{ $file->id }}'>
                                                    <div class="form-group">
                                                        <a href="{{ route('vessel_sancion_download_attachment', ['id' => $file->id]) }}" target="_blank" title="{{ $file->name }}">
                                                            <i class="fa fa-file-pdf-o fa-2x text-danger"></i>
                                                            <br>
                                                            <span class="text-muted">
                                                                {{ strlen($file->name) > 15 ? ( substr($file->name, 0, 14) . "...") : $file->name }}
                                                            </span>
                                                        </a>
                                                        <br>
                                                        <span class="btn btn-xs btn-danger delete-vessel-sancion-attachment" data-id='{{ $file->id }}'><i class="fa fa-trash"></i></span>
                                                    </div>
                                            </div>
                                        @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>

                </div>

                <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                    <label for="">Status</label>
                    <div class="radio">
                      <label> {{ Form::radio('status', '1', (!$sancion->exists || ($sancion->exists && $sancion->status == 1))) }} Activa </label>
                    </div>

                    <div class="radio">
                      <label> {{ Form::radio('status', '0', ($sancion->exists && $sancion->status == 0)) }} Inactiva </label>
                    </div>
                    <span class="help-block">{{ $errors->first('status') }}</span>
                </div>

            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-md btn-success" id="guardar">Guardar</button>
            </div>

            {{ Form::close() }}
        </div>
    </div>
</div>

@section('plugins_css')
    {{ Html::style('plugins/datepicker/datepicker3.css') }}
    {{ Html::style('plugins/select2/select2.min.css') }}
    <style media="screen">
        #help-block-error-files
        {
            color: #a94442 !important;
        }
    </style>
@endsection

@section('plugins_js')
    {{ Html::script('plugins/select2/select2.min.js') }}
    {{ Html::script('plugins/datepicker/bootstrap-datepicker.js') }}
    {{ Html::script('plugins/datepicker/locales/bootstrap-datepicker.es.js') }}

    <script type="text/javascript">
        $(document).ready(inicio);

        function inicio()
        {
            $('.datepicker').datepicker({
                language: 'es',
                format: 'yyyy-mm-dd',
                autoclose: true
            });

            $("select").select2();

            if(window.FileReader)
            {
                $("#attachments").change(verifyFiles);
            }

            $("span.delete-vessel-sancion-attachment").click(deleteAttachment);
        }

        function verifyFiles(e)
        {
            var files = e.originalEvent.target.files;
            var submit_disabled = false;
            var message = '';

            var b_submit = $("#guardar");

            if(files.length > 0)
            {
                var available_tipes = ['pdf'];
                var max_size = 5 * 1048576;

                $(b_submit).prop('disabled', true);

                var regex_extension = /(?:\.([^.]+))?$/;

                for(var i=0, len=files.length; i<len; i++)
                {
                    var n = files[i].name, s = files[i].size, t = files[i].type;

                    var ext = regex_extension.exec(n)[1];

                    console.log(n,s,t, ext);

                    if(typeof ext === 'undefined' || available_tipes.indexOf(ext.toLowerCase()) === -1 || s > max_size)
                    {
                        submit_disabled = true;
                        message = 'El archivo: "' + n + '" no cumple con los requisitos, intente quitarlo o cambiarlo';
                        break;
                    }
                }
            }

            $(b_submit).prop('disabled', submit_disabled);
            $("#help-block-error-files").text(message);
        }

        function deleteAttachment()
        {
            var id = $(this).attr("data-id");
            var r = confirm("¿Eliminar el archivo?");

            if(r)
            {
                var datos = {id : id};
                var route = "{{url('/sanciones/delete_attachment/')}}" + "/";
                $.ajax({
                   url: route+id,
                   type: 'get',
                   dataType: 'json',
                   data: datos,
                   beforeSend: function(){},
                   success: function(response){

                       if(response.success)
                       {
                           $("div.vessel-sancion-attachment-container[data-id='"+id+"']").remove();
                           alert("Archivo eliminado correctamente");
                       }
                       else
                       {
                           alert("No se puede eliminar el archivo por el momento");
                       }
                   },
                   error: function(request, error, status){
                       console.log("AJAX Error: ");
                       console.log($(request.responseText).text());
                    }
                });
            }
        }
    </script>
@endsection

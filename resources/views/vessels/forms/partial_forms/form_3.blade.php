<div class="tab-pane {{ $active_tab == 3 ? 'active' : '' }}" id="form_3">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Registro</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            {{ Form::open(['route' => ['update_vessel_path', $vessel->id], 'method' => 'PUT']) }}
            {{ csrf_field() }}
            <input type="hidden" name="form_tab" value="3">
            <div class="box-body">
                <div class="form-group">
                    <small class="text-muted"><span class="text-danger">*</span> Campos obligatorios</small>
                </div>

                <div class="form-group {{ $errors->has('estado_actual_pabellon') ? 'has-error' : '' }}">
                    <label for=""><span class="text-danger">*</span> Estado actual del pabellón</label>
                    {{ Form::select('estado_actual_pabellon', $refs['countries'], $vessel->estado_actual_pabellon, ['class' => 'form-control select2', 'style' => 'width: 100%' ])}}
                    <span class="help-block">{{ $errors->first('estado_actual_pabellon') }}</span>
                </div>

                <div class="form-group">
                    <label for="">Estado actual del pabellón - Fecha de Registro</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        {{ Form::text('estado_actual_pabellon_fecha_registro', $vessel->estado_actual_pabellon_fecha_registro, ['class' => 'form-control pull-right datepicker', 'readonly' => 'true']) }}
                    </div>
                </div>

                <div class="form-group {{ $errors->has('numero_registro_nacional') ? 'has-error' : '' }}">
                    <label for="">Número de registro nacional</label>
                    {{ Form::text('numero_registro_nacional', $vessel->numero_registro_nacional, ['class'=>'form-control']) }}
                    <span class="help-block">{{ $errors->first('numero_registro_nacional') }} </span>
                </div>

                <div class="form-group {{ $errors->has('nombre_barco') ? 'has-error' : '' }}">
                    <label for=""><span class="text-danger">*</span> Nombre del barco</label>
                    {{ Form::text('nombre_barco', $vessel->nombre_barco, ['class' => 'form-control'])}}
                    <span class="help-block">{{ $errors->first('nombre_barco') }}</span>
                </div>

                <div class="form-group {{ $errors->has('nombre_barco_ingles') ? 'has-error' : '' }}">
                    <label for="">Nombre del barco en ingles</label>
                    {{ Form::text('nombre_barco_ingles', $vessel->nombre_barco_ingles, ['class' => 'form-control'])}}
                    <span class="help-block">{{ $errors->first('nombre_barco_ingles') }} </span>
                </div>

                <div class="form-group">
                    <label for="">Puerto de registro</label>
                    {{ Form::select('puerto_registro', $refs['registration_ports'], $vessel->puerto_registro, ['class' => 'form-control select2', 'style' => 'width: 100%' ])}}
                </div>

                <div class="form-group">
                    <label for="">Tipo de barco</label>
                    {{ Form::select('tipo_barco', $refs['vessel_types'], $vessel->tipo_barco, ['class' => 'form-control select2', 'style' => 'width: 100%' ])}}
                </div>

                <div class="form-group">
                    <label for="">Funcionamiento</label>
                    <div class="radio">
                      <label> {{ Form::radio('funcionamiento', '1', (is_null($vessel->funcionamiento) || $vessel->funcionamiento == 1)) }} Sí </label>
                    </div>

                    <div class="radio">
                      <label> {{ Form::radio('funcionamiento', '0', (is_numeric($vessel->funcionamiento) && $vessel->funcionamiento == 0)) }} No </label>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

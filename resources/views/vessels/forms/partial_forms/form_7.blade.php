<div class="tab-pane {{ $active_tab == 7 ? 'active' : '' }}" id="form_7">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Características del permiso de pesca.</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            {{ Form::open(['route' => ['update_vessel_path', $vessel->id], 'method' => 'PUT']) }}
            {{ csrf_field() }}
            <input type="hidden" name="form_tab" value="7">
            <div class="box-body">
                <div class="form-group">
                    <label for="">¿Tiene permiso de pesca vigente?</label>

                    <div class="radio">
                      <label> {{ Form::radio('permiso_vigente', '1', (is_null($vessel->permiso_vigente || $vessel->permiso_vigente == 1))) }} Sí </label>
                    </div>

                    <div class="radio">
                      <label> {{ Form::radio('permiso_vigente', '0', (is_numeric($vessel->permiso_vigente) && $vessel->permiso_vigente == 0)) }} No </label>
                    </div>
                </div>

                <div class="form-group {{ $errors->has('numero_permiso') ? 'has-error' : '' }}">
                    <label for="">Número del permiso ó resolución mediante la cual se otorga el permiso de pesca (No. xxx del d/m/a).</label>
                    {{ Form::text('numero_permiso', $vessel->numero_permiso, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('numero_permiso') }}</span>
                </div>

                <div class="form-group {{ $errors->has('ultima_resolucion') ? 'has-error' : '' }}">
                    <label for="">Número del permiso ó última resolución vigente mediante la cual se otorga el permiso de pesca (No. xxx del d/m/a).</label>
                    {{ Form::text('ultima_resolucion', $vessel->ultima_resolucion, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('ultima_resolucion') }}</span>
                </div>

                <div class="form-group {{ $errors->has('vigencia_permiso') ? 'has-error' : '' }}">
                    <label for="">Vigencia del último permiso de pesca comercial (años)</label>
                    {{ Form::text('vigencia_permiso', $vessel->vigencia_permiso, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('vigencia_permiso') }}</span>
                </div>

                <div class="form-group">
                    <label for="">Fecha de inicio del último permiso de pesca otorgado a la embarcación</label>

                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        {{ Form::text('fecha_inicio_permiso', $vessel->fecha_inicio_permiso, ['class' => 'form-control pull-right datepicker', 'readonly'=>'true']) }}
                    </div>
                </div>

                <div class="form-group">
                    <label for="">Fecha de terminación del último permiso de pesca otorgado a la embarcación</label>

                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        {{ Form::text('fecha_final_permiso', $vessel->fecha_final_permiso, ['class' => 'form-control pull-right datepicker', 'readonly'=>'true']) }}
                    </div>
                </div>

                <div class="form-group">
                    <label for="">Fecha de inicio la patente de pesca</label>

                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        {{ Form::text('fecha_inicio_patente', $vessel->fecha_inicio_patente, ['class' => 'form-control pull-right datepicker', 'readonly'=>'true']) }}
                    </div>
                </div>

                <div class="form-group">
                    <label for="">Fecha de terminación la patente de pesca</label>

                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        {{ Form::text('fecha_final_patente', $vessel->fecha_final_patente, ['class' => 'form-control pull-right datepicker', 'readonly'=>'true']) }}
                    </div>
                </div>

                <div class="form-group">
                    <label for="">Zona de pesca autorizada</label>
                    {{ Form::select('zona_pesca_autorizada', $refs['zona_pesca_autorizadas'], $vessel->zona_pesca_autorizada, ['class' => 'form-control select2', 'style' => 'width: 100%']) }}
                </div>

                <div class="form-group">
                    <label for="">Puerto de desembarco autorizado en Colombia</label>
                    {{ Form::select('puerto_desembarco', $refs['colombia_puertos_desembarco_autorizados'], $vessel->puerto_desembarco, ['class' => 'form-control select2', 'style' => 'width: 100%']) }}
                </div>

                <div class="form-group">
                    <label for="">Pesquería autorizada - Especies Objetivo</label>
                    {{ Form::select('pesqueria_autorizada', $refs['pesqueria_autorizadas'], $vessel->pesqueria_autorizada, ['class' => 'form-control select2', 'style' => 'width: 100%']) }}
                </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

<div class="tab-pane {{ $active_tab == 9 ? 'active' : '' }}" id="form_9">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">DET</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            {{ Form::open(['route' => ['update_vessel_path', $vessel->id], 'method' => 'PUT']) }}
            {{ csrf_field() }}
            <input type="hidden" name="form_tab" value="9">
            <div class="box-body">
                <div class="form-group">
                    <label for="">Tipo de marco - material (tubo ó barra plana)</label>
                    {{ Form::select('tipo_material_det', $refs['materiales_tipo_marco'], $vessel->tipo_material_det, ['class' => 'form-control select2', 'style' => 'width: 100%']) }}
                </div>

                <div class="form-group {{ $errors->has('distancia_barras_det') ? 'has-error' : '' }}">
                    <label for="">Distancia entre las barras (hasta 4 pulgadas)</label>
                    {{ Form::text('distancia_barras_det', $vessel->distancia_barras_det, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('distancia_barras_det') }}</span>
                </div>

                <div class="form-group {{ $errors->has('altura_parrilla_det') ? 'has-error' : '' }}">
                    <label for="">Altura de la parrilla (mínimo 32*32 pulgadas)</label>
                    {{ Form::text('altura_parrilla_det', $vessel->altura_parrilla_det, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('altura_parrilla_det') }}</span>
                </div>

                <div class="form-group {{ $errors->has('ancho_parrilla_det') ? 'has-error' : '' }}">
                    <label for="">Achura de la parrilla (mínimo 32*32 pulgadas)</label>
                    {{ Form::text('ancho_parrilla_det', $vessel->ancho_parrilla_det, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('ancho_parrilla_det') }}</span>
                </div>

                <div class="form-group {{ $errors->has('angulo_inclinacion_derecho_det') ? 'has-error' : '' }}">
                    <label for="">Angulo de Inclinación Derecho (30 – 55 grados, ideal 45 grados)</label>
                    {{ Form::text('angulo_inclinacion_derecho_det', $vessel->angulo_inclinacion_derecho_det, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('angulo_inclinacion_derecho_det') }}</span>
                </div>

                <div class="form-group {{ $errors->has('angulo_inclinacion_izquierdo_det') ? 'has-error' : '' }}">
                    <label for="">Angulo de Inclinación Izquierdo (30 – 55 grados, 45 grados)</label>
                    {{ Form::text('angulo_inclinacion_izquierdo_det', $vessel->angulo_inclinacion_izquierdo_det, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('angulo_inclinacion_izquierdo_det') }}</span>
                </div>

                <div class="form-group {{ $errors->has('amplitud_minima_escape_solapa') ? 'has-error' : '' }}">
                    <label for="">Amplitud mínima de escape de la solapa (71 pulgadas)</label>
                    {{ Form::text('amplitud_minima_escape_solapa', $vessel->amplitud_minima_escape_solapa, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('amplitud_minima_escape_solapa') }}</span>
                </div>

                <div class="form-group {{ $errors->has('amplitud_minima_escape_solapa_doble') ? 'has-error' : '' }}">
                    <label for="">Amplitud mínima de escape de la solapa DET cobertura doble  (mínimo 56 pulgadas)</label>
                    {{ Form::text('amplitud_minima_escape_solapa_doble', $vessel->amplitud_minima_escape_solapa_doble, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('amplitud_minima_escape_solapa_doble') }}</span>
                </div>

                <div class="form-group {{ $errors->has('longitud_relinga_inferior') ? 'has-error' : '' }}">
                    <label for=""> Longitud de la relinga inferior (15 pulgadas)</label>
                    {{ Form::text('longitud_relinga_inferior', $vessel->longitud_relinga_inferior, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('longitud_relinga_inferior') }}</span>
                </div>

                <div class="form-group {{ $errors->has('traslape') ? 'has-error' : '' }}">
                    <label for="">Traslape (máximo 15 pulgadas)</label>
                    {{ Form::text('traslape', $vessel->traslape, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('traslape') }}</span>
                </div>

                <div class="form-group {{ $errors->has('redes_repuesto') ? 'has-error' : '' }}">
                    <label for="">Redes de repuesto</label>
                    {{ Form::text('redes_repuesto', $vessel->redes_repuesto, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('redes_repuesto') }}</span>
                </div>

                <div class="form-group {{ $errors->has('parrillas_repuesto') ? 'has-error' : '' }}">
                    <label for="">Parrillas de repuesto</label>
                    {{ Form::text('parrillas_repuesto', $vessel->parrillas_repuesto, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('parrillas_repuesto') }}</span>
                </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

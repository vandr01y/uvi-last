<div class="tab-pane {{ $active_tab == 10 ? 'active' : '' }}" id="form_10">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Fotografias de la embarcación</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            {{ Form::open(['route' => ['update_vessel_path', $vessel->id], 'method' => 'PUT', 'files' => true]) }}
            {{ csrf_field() }}
            <input type="hidden" name="form_tab" value="10">
            <div class="box-body">
                <div class="form-group {{ count($errors->get('photos.*')) > 0 ? 'has-error' : '' }}">
                    <label for="">Fotografías</label>
                    {{ Form::file('photos[]', ['id' => 'vessel-files', 'class' => 'form-control', 'multiple' => 'true']) }}
                    <p class="help-block text-muted">Formatos admitidos: PNG, JPG, JPEG. Tamaño Máximo: 2MB</p>
                    <span class="help-block text-danger" id="help-block-error-files"></span>
                    <span class="help-block">
                        @foreach($errors->get('photos.*') as $messages)
                            @foreach($messages as $message)
                                {{ $message }}
                            @endforeach
                        @endforeach
                    </span>
                    @if($vessel->exists && count($vessel->vr_pictures) > 0)
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Imágenes actuales: </label>

                                    <div class="row">
                                    @foreach ($vessel->vr_pictures AS $picture)
                                        <div class="col-md-3 text-center vessel-img-container" data-id='{{ $picture->id }}'>
                                            {{ Html::image($constants['asset_vessels_images_path'] . $picture->picture, 'Foto Buque', ['class' => 'img-responsive img-rounded'])}}
                                            <span class="btn btn-xs btn-danger delete-vessel-img" data-id='{{ $picture->id }}'><i class="fa fa-trash"></i></span>
                                        </div>
                                    @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" id="submit-form-10" class="btn btn-success">Guardar</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

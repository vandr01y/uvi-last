<div class="tab-pane {{ $active_tab == 2 ? 'active' : '' }}" id="form_2">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Identificación Regional</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            {{ Form::open(['route' => ['update_vessel_path', $vessel->id], 'method' => 'PUT']) }}
            {{ csrf_field() }}
            <input type="hidden" name="form_tab" value="2">
            <div class="box-body">

                <div class="form-group">
                    <label for="">Organización Regional Pesquera - OROP</label>
                    {{ Form::select('orop_id', $refs['orops'], $vessel->orop_id, ['class' => 'form-control select2', 'style' => 'width: 100%']) }}
                </div>
                <div class="form-group {{ $errors->has('identificador_regional_cuerpo') ? 'has-error' : '' }}">
                    <label for="">Identificador Regional del Cuerpo </label>
                    {{ Form::text('identificador_regional_cuerpo', $vessel->identificador_regional_cuerpo, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('identificador_regional_cuerpo') }}</span>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

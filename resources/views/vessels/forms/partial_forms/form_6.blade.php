<div class="tab-pane {{ $active_tab == 6 ? 'active' : '' }}" id="form_6">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Propiedad</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            {{ Form::open(['route' => ['update_vessel_path', $vessel->id], 'method' => 'PUT']) }}
            {{ csrf_field() }}
            <input type="hidden" name="form_tab" value="6">
            <div class="box-body">

                <div class="form-group {{ $errors->has('nombre_propietario') ? 'has-error' : '' }}">
                    <label for="">Nombre del propietario</label>
                    {{ Form::text('nombre_propietario', $vessel->nombre_propietario, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('nombre_propietario') }}</span>
                </div>

                <div class="form-group {{ $errors->has('numero_omi_propietario') ? 'has-error' : '' }}">
                    <label for="">Número OMI del Propietario de la empresa </label>
                    {{ Form::text('numero_omi_propietario', $vessel->numero_omi_propietario, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('numero_omi_propietario') }}</span>
                </div>

                <div class="form-group">
                    <label for="">Dirección del propietario/Detalles de contacto</label>
                    {{ Form::textarea('direccion_propietario', $vessel->direccion_propietario, ['class'=>'form-control', 'rows'=>'3'])}}
                </div>

                <div class="form-group {{ $errors->has('ciudad_propietario') ? 'has-error' : '' }}">
                    <label for="">Ciudad del propietario </label>
                    {{ Form::text('ciudad_propietario', $vessel->ciudad_propietario, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('ciudad_propietario') }}</span>
                </div>

                <div class="form-group {{ $errors->has('codigo_postal_propietario') ? 'has-error' : '' }}">
                    <label for="">Código Postal del propietario </label>
                    {{ Form::text('codigo_postal_propietario', $vessel->codigo_postal_propietario, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('codigo_postal_propietario') }}</span>
                </div>

                <div class="form-group">
                    <label for="">Nacionalidad del propietario</label>
                    {{ Form::select('nacionalidad_propietario', $refs['countries'], $vessel->nacionalidad_propietario, ['class'=>'form-control select2', 'style' => 'width: 100%'])}}
                </div>

                <div class="form-group {{ $errors->has('nombre_operador') ? 'has-error' : '' }}">
                    <label for="">Nombre del gerente / operador</label>
                    {{ Form::text('nombre_operador', $vessel->nombre_operador, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('nombre_operador') }}</span>
                </div>

                <div class="form-group {{ $errors->has('numero_omi_operador') ? 'has-error' : '' }}">
                    <label for="">Operador/manager Compañía Número OMI</label>
                    {{ Form::text('numero_omi_operador', $vessel->numero_omi_operador, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('numero_omi_operador') }}</span>
                </div>

                <div class="form-group">
                    <label for="">Dirección del operador/gerente / Detalles de contacto</label>
                    {{ Form::textarea('direccion_operador', $vessel->direccion_operador, ['class'=>'form-control', 'rows'=>'3'])}}
                </div>

                <div class="form-group {{ $errors->has('ciudad_operador') ? 'has-error' : '' }}">
                    <label for="">Ciudad del operador/gerente </label>
                    {{ Form::text('ciudad_operador', $vessel->ciudad_operador, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('ciudad_operador') }}</span>
                </div>

                <div class="form-group {{ $errors->has('codigo_postal_operador') ? 'has-error' : '' }}">
                    <label for="">Código Postal del operador/gerente </label>
                    {{ Form::text('codigo_postal_operador', $vessel->codigo_postal_operador, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('codigo_postal_operador') }}</span>
                </div>

                <div class="form-group">
                    <label for="">Nacionalidad del Gerente /Operador</label>
                    {{ Form::select('nacionalidad_operador', $refs['countries'], $vessel->nacionalidad_operador, ['class'=>'form-control select2', 'style' => 'width: 100%'])}}
                </div>

                <div class="form-group {{ $errors->has('nombre_administrador') ? 'has-error' : '' }}">
                    <label for="">Nombre del administrador o Agencia Marítima y empresa afiliadora en Colombia/Información de contacto</label>
                    {{ Form::text('nombre_administrador', $vessel->nombre_administrador, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('nombre_administrador') }}</span>
                </div>

                <div class="form-group {{ $errors->has('numero_omi_administrador') ? 'has-error' : '' }}">
                    <label for="">Numero IMO de la compañía administradora u operador (Agencia Marítima)</label>
                    {{ Form::text('numero_omi_administrador', $vessel->numero_omi_administrador, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('numero_omi_administrador') }}</span>
                </div>

                <div class="form-group">
                    <label for="">Dirección y contacto del administrador /Agencia Marítima/ Empresa afiliadora en Colombia</label>
                    {{ Form::textarea('direccion_administrador', $vessel->direccion_administrador, ['class' => 'form-control', 'rows' => '3']) }}
                </div>

                <div class="form-group {{ $errors->has('ciudad_administrador') ? 'has-error' : '' }}">
                    <label for="">Ciudad del administrador </label>
                    {{ Form::text('ciudad_administrador', $vessel->ciudad_administrador, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('ciudad_administrador') }}</span>
                </div>

                <div class="form-group {{ $errors->has('codigo_postal_administrador') ? 'has-error' : '' }}">
                    <label for="">Código Postal del administrador </label>
                    {{ Form::text('codigo_postal_administrador', $vessel->codigo_postal_administrador, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('codigo_postal_administrador') }}</span>
                </div>

                <div class="form-group">
                    <label for="">Nacionalidad de administrador /Agencia Marítima/ Empresa afiliadora en Colombia</label>
                    {{ Form::select('nacionalidad_administrador', $refs['countries'], $vessel->nacionalidad_administrador, ['class'=>'form-control select2', 'style' => 'width: 100%'])}}
                </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

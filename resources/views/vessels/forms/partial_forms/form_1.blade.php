<div class="tab-pane {{ $active_tab == 1 ? 'active' : '' }}" id="form_1">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Identificación</h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
        <div role="form">
            @if($vessel->exists)
                {{ Form::open(['route' => ['update_vessel_path', $vessel->id], 'method' => 'PUT']) }}
            @else
                {{ Form::open(['route' => 'store_vessel_path', 'method' => 'POST']) }}
            @endif
                {{ csrf_field() }}
            <input type="hidden" name="form_tab" value="1">
            <div class="box-body">
                <div class="form-group">
                    <small class="text-muted"><span class="text-danger">*</span> Campos obligatorios</small>
                </div>

                <div class="form-group {{ $errors->has('numero_omi') ? 'has-error' : '' }}">
                    <label for=""><span class="text-danger">*</span> Número OMI</label>
                    {{ Form::text('numero_omi', $vessel->numero_omi, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('numero_omi') }}</span>
                </div>

                <div class="form-group {{ $errors->has('marcado_externo') ? 'has-error' : '' }}">
                    <label for="">Marcados Externo</label>
                    {{ Form::text('marcado_externo', $vessel->marcado_externo, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('marcado_externo') }}</span>
                </div>

                <div class="form-group {{ $errors->has('mlri') ? 'has-error' : '' }}">
                    <label for="">Indicativo internacional de radio (MLRI)</label>
                    {{ Form::text('mlri', $vessel->mlri, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('mlri') }}</span>
                </div>

                <div class="form-group {{ $errors->has('mmsi') ? 'has-error' : '' }}">
                    <label for="">Identidad del Servicio Móvil Marítimo -(MMSI)</label>
                    {{ Form::text('mmsi', $vessel->mmsi, ['class' => 'form-control']) }}
                    <span class="help-block">{{ $errors->first('mmsi') }}</span>
                </div>

                <div class="form-group">
                    <label for="">Indicador VMS</label>

                    <div class="radio">
                      <label> {{ Form::radio('indicador_vms', '1', (!$vessel->exists || ($vessel->exists && $vessel->indicador_vms == 1))) }} Sí </label>
                    </div>

                    <div class="radio">
                      <label> {{ Form::radio('indicador_vms', '0', ($vessel->exists && $vessel->indicador_vms == 0)) }} No </label>
                    </div>

                </div>

                <div class="form-group">
                    <label for="">Tipo de VMS (RFMOs)</label>
                    {{ Form::select('tipo_vms', $refs['vms_types'], $vessel->tipo_vms, ['class' => 'form-control select2']) }}
                </div>

                <div class="form-group">
                    <label for="">Detalles del VMS</label>
                    {{ Form::textarea('detalles_vms', $vessel->detalles_vms, ['class' => 'form-control', 'rows' => '3']) }}
                </div>

                <div class="form-group">
                    <label for="">Sistema AIS</label>
                    <div class="radio">
                      <label> {{ Form::radio('sistema_ais', '1', (!$vessel->exists || ($vessel->exists && $vessel->sistema_ais == 1))) }} Sí </label>
                    </div>

                    <div class="radio">
                      <label> {{ Form::radio('sistema_ais', '0', ($vessel->exists && $vessel->sistema_ais == 0)) }} No </label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="">Detalles del AIS</label>
                    {{ Form::textarea('detalles_ais', $vessel->detalles_ais, ['class' => 'form-control', 'rows' => '3']) }}
                </div>

                <div class="form-group">
                    <label for="">Estado</label>
                    <div class="radio">
                      <label> {{ Form::radio('status', '1', (!$vessel->exists || ($vessel->exists && $vessel->status == 1))) }} Activo </label>
                    </div>

                    <div class="radio">
                      <label> {{ Form::radio('status', '0', ($vessel->exists && $vessel->status == 0)) }} Inactivo </label>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- .box box-primary -->
</div>
<!-- .tab-pane -->

@extends('layouts.app')
    @section('page_header')
        @include('common.page_header', ['page_header' => 'Registrar Buque'])
    @endsection

    @section('content')
        @include('common.validation_errors', ['general' => true])
        @include('vessels.forms.CreateUpdateForm', ['vessel' => $vessel])
    @endsection

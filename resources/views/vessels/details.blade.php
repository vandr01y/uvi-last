@extends('layouts.app')

    @section('page_header')
        @include('common.page_header', ['page_header' => 'Detalles del buque'])
    @endsection

    @section('content')
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <i></i> <h3 class="box-title">Identificación</h3>
                        </div>
                        <div class="box-body">
                            <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                        <td><b>Número OMI</b></td>
                                        <td>{{ $vessel->numero_omi }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Marcado Externo</b></td>
                                        <td>{{ $vessel->marcado_externo }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Indicativo internacional de radio (MLRI)</b></td>
                                        <td>{{ $vessel->mlri }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Identidad del Servicio Móvil Marítimo -(MMSI)</b></td>
                                        <td>{{ $vessel->mmsi }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Indicador VMS</b></td>
                                        <td>{{ $vessel->indicador_vms == 1 ? 'Sí' : 'No' }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Tipo de VMS (RFMOs)</b></td>
                                        <td>{{ $vessel->tipo_vms }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Detalles del VMS</b></td>
                                        <td>{{ $vessel->detalles_vms }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Sistema AIS</b></td>
                                        <td>{{ $vessel->sistema_ais == 1 ? 'Sí' : 'No' }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Detalles del AIS</b></td>
                                        <td>{{ $vessel->detalles_ais }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Estado</b></td>
                                        <td>{{ $vessel->status == 1 ? 'Activo' : 'Inactivo' }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- box -->
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <i></i> <h3 class="box-title">Registro</h3>
                        </div>
                        <div class="box-body">
                            <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                        <td><b><span>Estado actual del pabellón</b></td>
                                        <td>{{ isset($vessel->ref_pais_pabellon->name) ? $vessel->ref_pais_pabellon->name : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Estado actual del pabellón - Fecha de Registro</b></td>
                                        <td>{{ $vessel->estado_actual_pabellon_fecha_registro }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Número de registro nacional</b></td>
                                        <td>{{ $vessel->numero_registro_nacional }}</td>
                                    </tr>
                                    <tr>
                                        <td><b><span>Nombre del barco</b></td>
                                        <td>{{ $vessel->nombre_barco }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Nombre del barco en ingles</b></td>
                                        <td>{{ $vessel->nombre_barco_ingles }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Puerto de registro</b></td>
                                        <td>{{ isset($vessel->ref_puerto_registro->name) ? $vessel->ref_puerto_registro->name : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Tipo de barco</b></td>
                                        <td>{{ isset($vessel->ref_tipo_barco->name) ? $vessel->ref_tipo_barco->name : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Funcionamiento</b></td>
                                        <td>{{ $vessel->funcionamiento == 1 ? 'Sí' : 'No' }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- box -->
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <i></i> <h3 class="box-title">Construcción</h3>
                        </div>
                        <div class="box-body">
                            <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                        <td><b>Año de construcción</b></td>
                                        <td>{{ $vessel->anio_construccion }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>País de construcción</b></td>
                                        <td>{{ isset($vessel->ref_pais_construccion->name) ? $vessel->ref_pais_construccion->name : '' }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- box -->
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <i></i> <h3 class="box-title">Características del permiso de pesca.</h3>
                        </div>
                        <div class="box-body">
                            <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                        <td><b>¿Tiene permiso de pesca vigente?</b></td>
                                        <td>{{ $vessel->permiso_vigente == 1 ? 'Sí' : 'No' }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Número del permiso ó resolución mediante la cual se otorga el permiso de pesca (No. xxx del d/m/a).</b></td>
                                        <td>{{ $vessel->numero_permiso }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Número del permiso ó última resolución vigente mediante la cual se otorga el permiso de pesca (No. xxx del d/m/a).</b></td>
                                        <td>{{ $vessel->ultima_resolucion }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Vigencia del último permiso de pesca comercial (años)</b></td>
                                        <td>{{ $vessel->vigencia_permiso }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Fecha de inicio del último permiso de pesca otorgado a la embarcación</b></td>
                                        <td>{{ $vessel->fecha_inicio_permiso }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Fecha de terminación del último permiso de pesca otorgado a la embarcación</b></td>
                                        <td>{{ $vessel->fecha_final_permiso }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Fecha de inicio la patente de pesca</b></td>
                                        <td>{{ $vessel->fecha_inicio_patente }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Fecha de terminación la patente de pesca</b></td>
                                        <td>{{ $vessel->fecha_final_patente }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Zona de pesca autorizada</b></td>
                                        <td>{{ isset($vessel->ref_authorised_area->name) ? $vessel->ref_authorised_area->name : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Puerto de desembarco autorizado en Colombia</b></td>
                                        <td>{{ isset($vessel->ref_puerto_desembarco->name) ? $vessel->ref_puerto_desembarco->name : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Pesquería autorizada - Especies Objetivo</b></td>
                                        <td>{{ isset($vessel->ref_pesqueria_autorizada->name) ? $vessel->ref_pesqueria_autorizada->name : '' }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- box -->
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <i></i> <h3 class="box-title">DET</h3>
                        </div>
                        <div class="box-body">
                            <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                        <td><b>Tipo de marco - material (tubo ó barra plana)</b></td>
                                        <td>{{ $vessel->tipo_material_det }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Distancia entre las barras (hasta 4 pulgadas)</b></td>
                                        <td>{{ $vessel->distancia_barras_det }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Altura de la parrilla (mínimo 32*32 pulgadas)</b></td>
                                        <td>{{ $vessel->altura_parrilla_det }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Achura de la parrilla (mínimo 32*32 pulgadas)</b></td>
                                        <td>{{ $vessel->ancho_parrilla_det }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Angulo de Inclinación Derecho (30 – 55 grados, ideal 45 grados)</b></td>
                                        <td>{{ $vessel->angulo_inclinacion_derecho_det }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Angulo de Inclinación Izquierdo (30 – 55 grados, 45 grados)</b></td>
                                        <td>{{ $vessel->angulo_inclinacion_izquierdo_det }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Amplitud mínima de escape de la solapa (71 pulgadas)</b></td>
                                        <td>{{ $vessel->amplitud_minima_escape_solapa }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Amplitud mínima de escape de la solapa DET cobertura doble  (mínimo 56 pulgadas)</b></td>
                                        <td>{{ $vessel->amplitud_minima_escape_solapa_doble }}</td>
                                    </tr>
                                    <tr>
                                        <td><b> Longitud de la relinga inferior (15 pulgadas)</b></td>
                                        <td>{{ $vessel->longitud_relinga_inferior }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Traslape (máximo 15 pulgadas)</b></td>
                                        <td>{{ $vessel->traslape }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Redes de repuesto</b></td>
                                        <td>{{ $vessel->redes_repuesto }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Parrillas de repuesto</b></td>
                                        <td>{{ $vessel->parrillas_repuesto }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- box -->
            </div>

            <!-- RIGHT -->
            <div class="col-md-6">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <i></i> <h3 class="box-title">Fotografías</h3>
                        </div>
                        <div class="box-body">
                            @if(isset($vessel->vr_pictures) && count($vessel->vr_pictures) == 1)
                                {{ Html::image($constants['asset_vessels_images_path'] . $vessel->vr_pictures->first()->picture, 'Foto del buque', ['class' => 'img-responsive img-rounded'])}}
                            @elseif (isset($vessel->vr_pictures) && count($vessel->vr_pictures) > 1)
                                <div id="carousel-{{$vessel->id}}" class="carousel slide" data-ride="carousel" data-interval="true">

                                    <ol class="carousel-indicators">
                                        @foreach ($vessel->vr_pictures as $picture)
                                            <li data-target="#carousel-{{$vessel->id}}" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
                                        @endforeach
                                    </ol>

                                    <div class="carousel-inner">
                                        @foreach ($vessel->vr_pictures as $picture)
                                            <div class="item {{ $loop->first ? 'active' : '' }}">
                                                {{ Html::image($constants['asset_vessels_images_path'] . $picture->picture, "Foto: " . $loop->iteration) }}
                                                <div class="carousel-caption">
                                                    Foto {{ $loop->iteration }}
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>

                                    <a class="left carousel-control" href="#carousel-{{$vessel->id}}" data-slide="prev">
                                      <span class="fa fa-angle-left"></span>
                                    </a>
                                    <a class="right carousel-control" href="#carousel-{{$vessel->id}}" data-slide="next">
                                      <span class="fa fa-angle-right"></span>
                                    </a>
                                </div>
                            @else
                                <h4 class="text-center text-muted">El buque no cuenta con fotografías</h4>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- box -->
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <i></i> <h3 class="box-title">Identificación Regional</h3>
                        </div>
                        <div class="box-body">
                            <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                        <td><b>Organización Regional Pesquera - OROP</b></td>
                                        <td>{{ isset($vessel->ref_orop->name) ? $vessel->ref_orop->name : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Identificador Regional del Cuerpo </b></td>
                                        <td>{{ $vessel->identificador_regional_cuerpo }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- box -->
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <i></i> <h3 class="box-title">Dimensiones</h3>
                        </div>
                        <div class="box-body">
                            <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                        <td><b>Longitud total (LOA)(m)</b></td>
                                        <td>{{ $vessel->longitud_total }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Eslora entre perpendiculares (LBP) (m)</b></td>
                                        <td>{{ $vessel->eslora_perpendiculares }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Longitud registrada (m)</b></td>
                                        <td>{{ $vessel->longitud_registrada }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Manga / extrema amplitud (m)</b></td>
                                        <td>{{ $vessel->manga }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Puntal (m)</b></td>
                                        <td>{{ $vessel->puntal }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Calado (m)</b></td>
                                        <td>{{ $vessel->calado }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Peso muerto</b></td>
                                        <td>{{ $vessel->peso_muerto }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Tonelaje neto (NT)</b></td>
                                        <td>{{ $vessel->tonelaje_neto }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Tonelaje de registro neto (TRN)</b></td>
                                        <td>{{ $vessel->tonelaje_registro_neto }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Arqueo bruto (GT)</b></td>
                                        <td>{{ $vessel->arqueo_bruto }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Tonelaje de registro bruto (TRB)</b></td>
                                        <td>{{ $vessel->tonelaje_registro_bruto }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Potencia del motor principal/s </b></td>
                                        <td>{{ $vessel->potencia_motor_principal }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Unidad de Potencia</b></td>
                                        <td>{{ ($vessel->ref_power_unit->name) ? $vessel->ref_power_unit->name : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Material casco</b></td>
                                        <td>{{ ($vessel->ref_hull_material->name) ? $vessel->ref_hull_material->name : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Capacidad de la bodega de pescado</b></td>
                                        <td>{{ $vessel->capacidad_bodega_pescado }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Tipo de la bodega de pescado</b></td>
                                        <td>{{ ($vessel->ref_fish_hold_type->name) ? $vessel->ref_fish_hold_type->name : '' }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- box -->
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <i></i> <h3 class="box-title">Propiedad</h3>
                        </div>
                        <div class="box-body">
                            <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                        <td><b>Nombre del propietario</b></td>
                                        <td>{{ $vessel->nombre_propietario }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Número OMI del Propietario de la empresa </b></td>
                                        <td>{{ $vessel->numero_omi_propietario }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Dirección del propietario/Detalles de contacto</b></td>
                                        <td>{{ $vessel->direccion_propietario }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Ciudad del propietario </b></td>
                                        <td>{{ $vessel->ciudad_propietario }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Código Postal del propietario </b></td>
                                        <td>{{ $vessel->codigo_postal_propietario }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Nacionalidad del propietario</b></td>
                                        <td>{{ isset($vessel->ref_pais_propietario->name) ? $vessel->ref_pais_propietario->name : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Nombre del gerente / operador</b></td>
                                        <td>{{ $vessel->nombre_operador }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Operador/manager Compañía Número OMI</b></td>
                                        <td>{{ $vessel->numero_omi_operador }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Dirección del operador/gerente / Detalles de contacto</b></td>
                                        <td>{{ $vessel->direccion_operador }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Ciudad del operador/gerente </b></td>
                                        <td>{{ $vessel->ciudad_operador }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Código Postal del operador/gerente </b></td>
                                        <td>{{ $vessel->codigo_postal_operador }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Nacionalidad del Gerente /Operador</b></td>
                                        <td>{{ isset($vessel->ref_pais_gerente_operador->name) ? $vessel->ref_pais_gerente_operador->name : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Nombre del administrador o Agencia Marítima y empresa afiliadora en Colombia/Información de contacto</b></td>
                                        <td>{{ $vessel->nombre_administrador }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Numero IMO de la compañía administradora u operador (Agencia Marítima)</b></td>
                                        <td>{{ $vessel->numero_omi_administrador }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Dirección y contacto del administrador /Agencia Marítima/ Empresa afiliadora en Colombia</b></td>
                                        <td>{{ $vessel->direccion_administrador }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Ciudad del administrador </b></td>
                                        <td>{{ $vessel->ciudad_administrador }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Código Postal del administrador </b></td>
                                        <td>{{ $vessel->codigo_postal_administrador }}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Nacionalidad de administrador /Agencia Marítima/ Empresa afiliadora en Colombia</b></td>
                                        <td>{{ isset($vessel->ref_pais_administrador->name) ? $vessel->ref_pais_administrador->name : '' }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- box -->
            </div>
            <!-- END RIGHT -->
        </div>
        <!-- .row -->
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="box box-primary">
                    <div class="box-header">
                        <i></i> <h3 class="box-title">Artes y/o métodos de pesca autorizadas</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-hover">
                            <tbody>
                                <tr>
                                    <td><b>Denominación del arte de pesca tipo red</b></td>
                                    <td>{{ isset($vessel->ref_arte_pesca_red->name) ? $vessel->ref_arte_pesca_red->name : '' }}</td>
                                </tr>
                                <tr>
                                    <td><b>Ojo de malla (pulgadas)</b></td>
                                    <td>{{ $vessel->ojo_malla }}</td>
                                </tr>
                                <tr>
                                    <td><b>Ojo de malla del copo (pulgadas)</b></td>
                                    <td>{{ $vessel->ojo_malla_copo }}</td>
                                </tr>
                                <tr>
                                    <td><b>Longitud de la red (Brazas)</b></td>
                                    <td>{{ $vessel->longitud_red }}</td>
                                </tr>
                                <tr>
                                    <td><b>Longitud de la relinga superior (Redes arrastre) en pies</b></td>
                                    <td>{{ $vessel->longitud_relinga_superior }}</td>
                                </tr>
                                <tr>
                                    <td><b>Ancho de la red (Brazas)</b></td>
                                    <td>{{ $vessel->ancho_red }}</td>
                                </tr>
                                <tr>
                                    <td><b>Cantidad de paños o piezas</b></td>
                                    <td>{{ $vessel->cantidad_piezas }}</td>
                                </tr>
                                <tr>
                                    <td><b>Material de la red</b></td>
                                    <td>{{ isset($vessel->ref_material_red->name) ? $vessel->ref_material_red->name : '' }}</td>
                                </tr>
                                <tr>
                                    <td><b>Denominación del arte de pesca tipo Sedal ó de anzuelo</b></td>
                                    <td>{{ isset($vessel->ref_arte_pesca_anzuelo->name) ? $vessel->ref_arte_pesca_anzuelo->name : '' }}</td>
                                </tr>
                                <tr>
                                    <td><b>Tipo de anzuelo</b></td>
                                    <td>{{ isset($vessel->ref_tipo_anzuelo->name) ? $vessel->ref_tipo_anzuelo->name : '' }}</td>
                                </tr>
                                <tr>
                                    <td><b>Tamaño y tipo de anzuelo</b></td>
                                    <td>{{ $vessel->tamanio_anzuelo }}</td>
                                </tr>
                                <tr>
                                    <td><b>Cantidad de anzuelos</b></td>
                                    <td>{{ $vessel->cantidad_anzuelos }}</td>
                                </tr>
                                <tr>
                                    <td><b>Longitud de la línea madre</b></td>
                                    <td>{{ $vessel->longitud_linea_madre }}</td>
                                </tr>
                                <tr>
                                    <td><b>Material de la línea madre</b></td>
                                    <td>{{ isset($vessel->ref_material_linea_madre->name) ? $vessel->ref_material_linea_madre->name : '' }}</td>
                                </tr>
                                <tr>
                                    <td><b>Material de las bajantes</b></td>
                                    <td>{{ isset($vessel->ref_material_bajante->name) ? $vessel->ref_material_bajante->name : '' }}</td>
                                </tr>
                                <tr>
                                    <td><b>Cantidad total de líneas</b></td>
                                    <td>{{ $vessel->cantidad_total_lineas }}</td>
                                </tr>
                                <tr>
                                    <td><b>Denominación del arte de pesca tipo Trampas o nasas</b></td>
                                    <td>{{ isset($vessel->ref_arte_pesca_trampa->name) ? $vessel->ref_arte_pesca_trampa->name : '' }}</td>
                                </tr>
                                <tr>
                                    <td><b>Cantidad de trampas o nasas</b></td>
                                    <td>{{ $vessel->cantidad_trampas }}</td>
                                </tr>
                                <tr>
                                    <td><b>Material principal de la trampa o nasa</b></td>
                                    <td>{{ isset($vessel->ref_materiales_trampa->name) ? $vessel->ref_materiales_trampa->name : '' }}</td>
                                </tr>
                                <tr>
                                    <td><b>Tipo de artefactos de herir o aferrar</b></td>
                                    <td>{{ isset($vessel->ref_tipo_artefactos_herir->name) ? $vessel->ref_tipo_artefactos_herir->name : '' }}</td>
                                </tr>
                                <tr>
                                    <td><b>Cantidad de artefactos</b></td>
                                    <td>{{ $vessel->cantidad_artefactos }}</td>
                                </tr>
                                <tr>
                                    <td><b>Material del artefacto</b></td>
                                    <td>{{ isset($vessel->ref_material_artefactos->name) ? $vessel->ref_material_artefactos->name : '' }}</td>
                                </tr>
                                <tr>
                                    <td><b>Otros</b></td>
                                    <td>{{ $vessel->arte_pesca_otros }}</td>
                                </tr>
                                <tr>
                                    <td><b>Uso de Dispositivos Agregadores de Peces - FAD (Fish Aggregating Device)</b></td>
                                    <td>{{ $vessel->fad == 1 ? 'Sí' : 'No' }}</td>
                                </tr>
                                <tr>
                                    <td><b>Tipo de FAD utilizados</b></td>
                                    <td>{{ isset($vessel->ref_tipo_fad->name) ? $vessel->ref_tipo_fad->name : '' }}</td>
                                </tr>
                                <tr>
                                    <td><b>Cantidad de FAD utilizados</b></td>
                                    <td>{{ $vessel->cantidad_fad }}</td>
                                </tr>
                                <tr>
                                    <td><b>Componentes del FAD</b></td>
                                    <td>{{ $vessel->componentes_fad }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- box -->
        </div>
    @endsection

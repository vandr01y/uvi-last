@extends('layouts.app')

    @section('page_header')
        @include('common.page_header', ['page_header' => 'Sanciones - Buques Artesanales'])
    @endsection

    @section('content')
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Listado de sanciones de buques artesanales</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                            <table class="table table-bordered table-hover dt-responsive" id="sanciones-table">
                                <thead>
                                    <tr>
                                        <th>NUR</th>
                                        <th class="text-center">Buque</th>
                                        <th class="text-center">Infracción en proceso de investigación</th>
                                        <th class="text-center">Fecha de sanción</th>
                                        <th class="text-center">Valor Multa</th>
                                        <th class="text-center">Estado de la sanción</th>
                                        <th class="text-center">Archivos</th>
                                        <th class="text-center">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($sanciones as $sancion)
                                        <tr>
                                            <td class="text-center">{{ $sancion->nur }}</td>
                                            <td class="text-center">{{ $sancion->rel_buque_artesanal->nombre }}</td>
                                            <td class="text-center"><b>{{ $sancion->ref_infraccion_proceso->name }}</b></td>
                                            <td class="text-center">{{ $sancion->fecha_sancion }}</td>
                                            <td class="text-center">$ {{ number_format($sancion->valor_multa, 2, '.',' ') }}</td>
                                            <td class="text-center">{{ $sancion->ref_sanciones_estados->name }}</td>
                                            <td class="text-center">
                                                @if (count($sancion->rel_files) > 0)
                                                    <a href="{{ route('buque_artesanal_sancion_download_attachments_zip', ['sancion' => $sancion->id]) }}" target="_blank"><i class="fa fa-file-archive-o fa-lg"></i></a>
                                                @else
                                                    <span class="text-muted">Sin Archivos</span>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <a href="{{route('edit_buque_artesanal_sancion_path', ['sancion' => $sancion->id])}}" class="btn btn-xs btn-info">Editar</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        {{ $sanciones->render() }}
                    </div>
                    <!-- /.box-body -->
                </div>
                  <!-- /.box -->
            </div>
        </div>
    @endsection
    @section('plugins_css')
        {{ Html::style('plugins/datatables/dataTables.bootstrap.css') }}
        {{ Html::style('plugins/datatables/responsive.bootstrap.min.css') }}
    @endsection
    @section('plugins_js')
        {{ Html::script('plugins/datatables/jquery.dataTables.min.js') }}
        {{ Html::script('plugins/datatables/dataTables.bootstrap.min.js') }}
        <script type="text/javascript">
            $(document).ready(inicio);

            function inicio()
            {
                var table = $("#sanciones-table");
                var last_column = $(table).find("thead > tr > th").length - 1;

                $(table).DataTable({
                    paging: false,
                    lengthChange: false,
                    searching: true,
                    ordering: true,
                    info: true,
                    autoWidth: false,
                    //columnDefs: [], // Buscar por el atributo alt en las imagenes
                    aoColumnDefs: [{ "bSortable" : false, "aTargets" : [ last_column ] }],
                    language: DATATABLES_LAN
                });
            }
        </script>
    @endsection

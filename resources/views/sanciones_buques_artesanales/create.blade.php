@extends('layouts.app')
    @section('page_header')
        @include('common.page_header', ['page_header' => 'Registrar Sanción - Buque Artesanal'])
    @endsection

    @section('content')
        @include('common.validation_errors', ['general' => true])
        @include('sanciones_buques_artesanales.forms.CreateUpdateForm', ['sancion' => $sancion, 'refs' => $refs])
    @endsection

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ config('app.name', 'UVI') }}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  {{ Html::style('plugins/Bootstrap/bootstrap.min.css') }}
  <!-- Font Awesome -->
  {{ Html::style('css/FontAwesome/fontawesome.min.css') }}
  <!-- Ionicons -->
  {{ Html::style('css/Ionicons/ionicons.min.css') }}
  <!-- Theme style -->
  {{ Html::style('css/AdminLTE/AdminLTE.min.css') }}

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="{{ url('/login') }}"><b>{{ config('app.name', 'UVI') }}</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Iniciar sesión</p>

    @yield('content')

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
{{ Html::script('plugins/jQuery/jquery-2.2.3.min.js') }}
<!-- Bootstrap 3.3.6 -->
{{ Html::script('plugins/Bootstrap/bootstrap.min.js') }}
</body>
</html>

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVesselDetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vessel_dets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vessel_id');
            $table->integer('tipo_marco_material_id'); // REFERENCIA
            $table->string('distancia_entre_barras');
            $table->string('altura_ancho_parrilla');
            $table->string('angulo_inclinacion_derecho');
            $table->string('angulo_inclinacion_izquierdo');
            $table->string('amplitud_minima_escape_solapa');
            $table->string('amplitud_minima_escape_solapa_det_cobertura_doble');
            $table->string('longitud_relinga_inferior');
            $table->string('traslape');
            $table->integer('redes_repuesto');
            $table->integer('parrillas_repuesto');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vessel_dets');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVesselCaracteristicasPermisoPescaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vessel_caracteristicas_permiso_pesca', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vessel_id');
            $table->integer('permiso_pesca_vigente');
            $table->string('numero_permiso_resolucion');
            $table->string('numero_permiso_ultima_resolucion_vigente');
            $table->integer('anios_vigencia_ultimo_permiso_pesca_comercial');
            $table->date('fecha_inicio_ultimo_permiso_pesca')->nullable();
            $table->date('fecha_terminacion_ultimo_permiso_pesca')->nullable();
            $table->date('fecha_inicio_patente_pesca')->nullable();
            $table->date('fecha_terminacion_patente_pesca')->nullable();
            $table->integer('zona_pesca_autorizada_id'); // REFERENCIA
            $table->integer('colombia_puerto_desembarco_autorizado_id'); // REFERENCIA
            $table->integer('pesqueria_autorizada_id'); // REFERENCIA
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vessel_caracteristicas_permiso_pesca');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuquesArtesanalesSancionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buques_artesanales_sanciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('buque_artesanal_id')->unsigned();
            $table->tinyInteger('presunta_violacion')->nullable();
            $table->string('nur')->default('');
            $table->integer('infraccion_proceso_id')->unsigned();
            $table->tinyInteger('sanciones_estatuto_pesquero')->default(1);
            $table->bigInteger('numero_acto_administrativo')->nullable();
            $table->date('fecha_sancion')->nullable();
            $table->integer('infraccion_sancionada_id')->nullable();
            $table->integer('tipo_sancion_id')->nullable();
            $table->integer('fallo_ejecutoriado_id')->nullable();
            $table->double('valor_multa')->nullable();
            $table->integer('estado_sancion_id')->nullable();
            $table->string('detalles')->default('');
            $table->tinyInteger('status')->default(1);
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buques_artesanales_sanciones');
    }
}

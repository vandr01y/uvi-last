<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVesselsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vessels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->tinyInteger('status');

            // FORM_1
            $table->string('numero_omi')->default(''); // REQUIRED
            $table->string('marcado_externo')->default('');
            $table->string('mlri')->default('');
            $table->string('mmsi')->default('');
            $table->tinyInteger('indicador_vms');
            $table->integer('tipo_vms')->unsigned(); // REFERENCIA
            $table->string('detalles_vms')->default('');
            $table->tinyInteger('sistema_ais');
            $table->string('detalles_ais')->default('');

            // FORM_2
            $table->integer('orop_id')->nullable(); // REFERENCIA
            $table->string('identificador_regional_cuerpo')->default('');

            // FORM_3
            $table->integer('estado_actual_pabellon')->nullable(); // REFERENCIA PAIS // REQUIRED
            $table->date('estado_actual_pabellon_fecha_registro')->nullable();
            $table->string('numero_registro_nacional')->default('');
            $table->string('nombre_barco')->default(''); // REQUIRED
            $table->string('nombre_barco_ingles')->default('');
            $table->integer('puerto_registro')->nullable(); // REFERENCIA
            $table->integer('tipo_barco')->nullable(); // REFERENCIA
            $table->tinyInteger('funcionamiento')->nullable();

            // FORM_4
            $table->double('longitud_total')->nullable(); // REQUIRED
            $table->double('eslora_perpendiculares')->nullable();
            $table->double('longitud_registrada')->nullable();
            $table->double('manga')->nullable();
            $table->double('puntal')->nullable();
            $table->double('calado')->nullable();
            $table->double('peso_muerto')->nullable();
            $table->double('tonelaje_neto')->nullable();
            $table->double('tonelaje_registro_neto')->nullable();
            $table->double('arqueo_bruto')->nullable(); // REQUIRED
            $table->double('tonelaje_registro_bruto')->nullable(); // REQUIRED
            $table->double('potencia_motor_principal')->nullable();
            $table->integer('unidad_potencia')->nullable(); // REFERENCIA
            $table->integer('material_casco')->nullable();
            $table->double('capacidad_bodega_pescado')->nullable();
            $table->integer('tipo_bodega_pescado')->nullable(); // REFERENCIA

            // FORM_5
            $table->integer('anio_construccion')->nullable();
            $table->integer('pais_construccion')->nullable(); // REFERENCIA PAISES

            // FORM_6
            $table->string('nombre_propietario')->default('');
            $table->string('numero_omi_propietario')->default('');
            $table->string('direccion_propietario')->default('');
            $table->string('ciudad_propietario')->default('');
            $table->integer('codigo_postal_propietario')->nullable(); // NUEVO
            $table->integer('nacionalidad_propietario')->nullable(); // REFERENCIA PAISES
            $table->string('nombre_operador')->default('');
            $table->string('numero_omi_operador')->default('');
            $table->string('direccion_operador')->default('');
            $table->string('ciudad_operador')->default('');
            $table->integer('codigo_postal_operador')->nullable(); // NUEVO
            $table->integer('nacionalidad_operador')->nullable(); // REFERENCIA PAISES
            $table->string('nombre_administrador')->default('');
            $table->string('numero_omi_administrador')->default('');
            $table->string('direccion_administrador')->default('');
            $table->string('ciudad_administrador')->default('');
            $table->integer('codigo_postal_administrador')->nullable(); // NUEVO
            $table->integer('nacionalidad_administrador')->nullable(); // REFERENCIA PAISES

            // FORM_7
            $table->tinyInteger('permiso_vigente')->nullable();
            $table->string('numero_permiso')->default('');
            $table->string('ultima_resolucion')->default('');
            $table->integer('vigencia_permiso')->nullable();
            $table->date('fecha_inicio_permiso')->nullable();
            $table->date('fecha_final_permiso')->nullable();
            $table->date('fecha_inicio_patente')->nullable();
            $table->date('fecha_final_patente')->nullable();
            $table->integer('zona_pesca_autorizada')->nullable(); // REFERENCIA
            $table->integer('puerto_desembarco')->nullable(); // REFERENCIAS
            $table->integer('pesqueria_autorizada')->nullable(); // REFERENCIA

            // FORM_8
            $table->integer('arte_pesca_red')->nullable(); // REFERENCIA
            $table->double('ojo_malla')->nullable();
            $table->double('ojo_malla_copo')->nullable();
            $table->double('longitud_red')->nullable();
            $table->double('longitud_relinga_superior')->nullable();
            $table->double('ancho_red')->nullable();
            $table->double('cantidad_piezas')->nullable();
            $table->integer('material_red')->nullable(); // REFERENCIA
            $table->integer('arte_pesca_anzuelo')->nullable(); // REFERENCIA
            $table->integer('tipo_anzuelo')->nullable(); // REFERENCIA
            $table->double('tamanio_anzuelo')->nullable();
            $table->integer('cantidad_anzuelos')->nullable();
            $table->string('longitud_linea_madre')->default('');
            $table->integer('material_linea_madre')->nullable(); // REFERENCIA
            $table->integer('material_bajantes')->nullable(); // REFERENCIA
            $table->integer('cantidad_total_lineas')->nullable();
            $table->integer('arte_pesca_trampa')->nullable(); // REFERENCIA
            $table->integer('cantidad_trampas')->nullable();
            $table->integer('materiales_trampas')->nullable(); // REFERENCIA
            $table->integer('tipo_artefactos_herir')->nullable(); // REFERENCIA
            $table->integer('cantidad_artefactos')->nullable();
            $table->integer('material_artefactos')->nullable(); // REFERENCIA
            $table->string('arte_pesca_otros')->default('');
            $table->tinyInteger('fad')->nullable();
            $table->integer('tipos_fad')->nullable(); // REFERENCIA
            $table->integer('cantidad_fad')->nullable();
            $table->string('componentes_fad')->default('');

            // FORM_9
            $table->integer('tipo_material_det')->nullable(); // REFERENCIA
            $table->string('distancia_barras_det')->default('');
            $table->string('altura_parrilla_det')->default('');
            $table->string('ancho_parrilla_det')->default('');
            $table->string('angulo_inclinacion_derecho_det')->default('');
            $table->string('angulo_inclinacion_izquierdo_det')->default('');
            $table->string('amplitud_minima_escape_solapa')->default('');
            $table->string('amplitud_minima_escape_solapa_doble')->default('');
            $table->string('longitud_relinga_inferior')->default('');
            $table->string('traslape')->default('');
            $table->integer('redes_repuesto')->nullable();
            $table->integer('parrillas_repuesto')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vessels');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVesselArtesMetodosPescaAutorizadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vessel_artes_metodos_pesca_autorizados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vessel_id');
            $table->integer('denominacion_arte_pesca_tipo_red_id'); // REFERENCIA
            $table->double('ojo_malla_pulgadas');
            $table->double('ojo_malla_del_copo_pulgadas');
            $table->double('longitud_red_brazas');
            $table->double('longitud_relinga_superior_pies');
            $table->double('ancho_red_brazas');
            $table->integer('cantidad_panos_piezas');
            $table->integer('material_la_red_id'); // REFERENCIA
            $table->integer('denominacion_arte_pesca_tipo_sedal_anzuelo_id'); // REFERENCIA
            $table->integer('tipo_anzuelo_id'); // REFERENCIA
            $table->string('tamanio_tipo_anzuelo');
            $table->integer('cantidad_anzuelos');
            $table->string('longitud_linea_madre');
            $table->integer('material_linea_madre_id'); // REFERENCIA
            $table->integer('material_bajantes_id'); // REFERENCIA
            $table->integer('cantidad_total_lineas');
            $table->integer('denominacion_arte_pesca_tipo_trampas_nasas_id'); // REFERENCIA
            $table->integer('cantidad_trampas_nasas');
            $table->integer('material_principal_trampa_nasa_id'); // REFERENCIA
            $table->integer('tipo_artefactos_herir_aferrar_id'); // REFERENCIA
            $table->integer('cantidad_artefactos');
            $table->integer('material_artefacto_id'); // REFERENCIA
            $table->string('otros');
            $table->integer('fad');
            $table->integer('tipo_fad_utilizados_id'); // REFERENCIA
            $table->integer('cantidad_fad_utilizados');
            $table->string('componentes_fad');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vessel_artes_metodos_pesca_autorizados');
    }
}

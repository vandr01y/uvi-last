<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuquesArtesanalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buques_artesanales', function (Blueprint $table) {
            $table->increments('id');
            // FORM_1
            $table->string('nombre')->default('');
            $table->string('matricula')->default('');
            $table->string('numero_identificacion_embarcacion')->default('');
            // FORM_2
            $table->integer('bandera_id')->nullable();
            $table->integer('puerto_id')->nullable();
            $table->integer('tipo_id')->nullable();
            $table->tinyInteger('estado_operacional')->nullable();
            $table->integer('cuenca_id')->nullable();
            $table->integer('departamento_id')->nullable();
            $table->integer('municipio_id')->nullable();
            $table->string('localidad')->default('');
            //$table->integer('lugar_desembarco_id')->nullable();
            $table->string('lugar_desembarco')->default('');
            $table->tinyInteger('permiso_pesca')->nullable();
            $table->bigInteger('numero_resolucion')->nullable();
            $table->integer('vigencia_permiso')->nullable();
            $table->date('fecha_inicio')->nullable();
            $table->date('fecha_finalizacion')->nullable();
            // FORM_3
            $table->integer('eslora')->nullable();
            $table->integer('manga')->nullable();
            $table->integer('puntal')->nullable();
            $table->integer('calado')->nullable();
            $table->integer('trn')->nullable();
            $table->integer('trb')->nullable();
            // FORM_4
            $table->integer('anio_construccion')->nullable();
            $table->string('lugar_construccion')->default('');
            $table->string('astillero_construccion')->default('');
            $table->string('carpintero_construccion')->default('');
            $table->integer('material_embarcacion_id')->nullable();
            // FORM_5
            $table->integer('tipo_propulsion_id')->nullable();
            $table->integer('tipo_motor_id')->nullable();
            $table->string('marca_motor')->default('');
            $table->string('potencia_motor')->default('');
            $table->bigInteger('numero_motor')->nullable();
            $table->string('tipo_combustible')->default('');
            // FORM_6
            $table->string('nombre_propietario')->default('');
            $table->bigInteger('numero_identificacion')->nullable();
            $table->string('direccion_residencia')->default('');
            $table->string('ciudad_municipio')->default('');
            $table->bigInteger('telefono_fijo')->nullable();
            $table->bigInteger('telefono_celular')->nullable();
            $table->tinyInteger('carne_pesca')->nullable();
            $table->bigInteger('numero_carne_pesca')->nullable();
            $table->date('fecha_finalizacion_carne_pesca')->nullable();
            $table->tinyInteger('afiliacion')->nullable();
            $table->string('nombre_organizacion')->default('');
            $table->integer('numero_tripulantes')->nullable();
            $table->text('nombre_tripulacion')->nullable();
            $table->integer('numero_documentos_tripulacion')->nullable();
            $table->text('direccion_residencia_tripulacion')->nullable();
            $table->text('telefono_contacto_tripulacion')->nullable();
            $table->text('numero_carne_pesca_tripulacion')->nullable();
            $table->date('fecha_finalizacion_carne_tripulacion')->nullable();
            // FORM_7
            $table->tinyInteger('radio_comunicaciones')->nullable();
            $table->string('marca_radio_comunicaciones')->default('');
            $table->double('frecuencia_asignada')->nullable();
            $table->tinyInteger('brujula')->nullable();
            $table->string('marca_brujula')->default('');
            $table->tinyInteger('gps')->nullable();
            $table->string('marca_gps')->default('');
            $table->tinyInteger('chalecos_salvavidas')->nullable();
            $table->integer('numero_chalecos_salvavidas')->nullable();
            $table->integer('estado_condicion_chalecos_salvavidas_id')->nullable();
            // FORM_8
            $table->integer('tipo_red_id')->nullable();
            $table->double('ojo_malla')->nullable();
            $table->double('ojo_malla_copo')->nullable();
            $table->double('longitud_red')->nullable();
            $table->double('ancho_red')->nullable();
            $table->integer('cantidad_panos_piezas')->nullable();
            $table->integer('material_red_id')->nullable();
            $table->text('zonas_pesca_frecuentadas')->nullable();
            $table->integer('tipos_lineas_mano_id')->nullable();
            $table->integer('trampas_id')->nullable();
            $table->integer('cantidad_trampas')->nullable();
            $table->integer('material_principal_trampas_id')->nullable();
            $table->text('zonas_pesca_trampas')->nullable();
            $table->integer('tipos_artefactos_herir_aferrar_id')->nullable();
            $table->integer('cantidad_artefactos')->nullable();
            $table->integer('material_artefacto_id')->nullable();
            $table->text('zonas_pesca_artefactos')->nullable();
            $table->tinyInteger('otros')->nullable();
            $table->text('artes_metodos_poco_convencionales')->nullable();
            $table->integer('cantidad_artes_metodos_poco_convencionales')->nullable();
            $table->tinyInteger('uso_fad')->nullable();
            $table->integer('tipos_fad_id')->nullable();
            $table->integer('cantidad_fad')->nullable();

            $table->integer('user_id')->unsigned();
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buques_artesanales');
    }
}

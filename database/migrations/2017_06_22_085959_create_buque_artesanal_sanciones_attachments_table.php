<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuqueArtesanalSancionesAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buque_artesanal_sanciones_attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('buque_artesanal_sancion_id');
            $table->string('file');
            $table->string('name')->default('');
            $table->string('details')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buque_artesanal_sanciones_attachments');
    }
}

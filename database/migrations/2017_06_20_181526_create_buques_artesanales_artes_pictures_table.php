<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuquesArtesanalesArtesPicturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buques_artesanales_artes_pictures', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('buque_artesanal_id')->unsigned();
            $table->string('nombre')->default('');
            $table->string('file')->default('');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buques_artesanales_artes_pictures');
    }
}

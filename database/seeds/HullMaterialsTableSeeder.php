<?php

use Illuminate\Database\Seeder;

class HullMaterialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('hull_materials')->insert([
            ['code' => 1, 'name' => 'Wood - Madera'],
            ['code' => 2, 'name' => 'Marine Plywood – Madera forrada'],
            ['code' => 3, 'name' => 'Aluminium – Aluminio'],
            ['code' => 4, 'name' => 'Iron/Steel - Hierro / Acero'],
            ['code' => 5, 'name' => 'Fibreglass – Fibra de vidrio'],
            ['code' => 6, 'name' => 'Rubber – Caucho'],
            ['code' => 7, 'name' => 'Cement – Cemento'],
            ['code' => 98, 'name' => 'Other – Otro'],
            ['code' => 99, 'name' => 'Unknown - Desconocido'],
        ]);
    }
}

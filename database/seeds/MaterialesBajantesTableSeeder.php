<?php

use Illuminate\Database\Seeder;

class MaterialesBajantesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('materiales_bajantes')->insert([
            ['name' => "Monofilamento"],
            ['name' => "Polifilamento"],
            ['name' => "Acero"],
            ['name' => "Otro, Cual?"],
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class DenominacionArtePescaTipoTrampasNasasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('denominacion_arte_pesca_tipo_trampas_nasas')->insert([
            ['name' => "Nasas para peces"],
            ['name' => "Nasas para crustáceos"],
            ['name' => "Nasas para moluscos"],
            ['name' => "Otro, Cual?"],
        ]);
    }
}

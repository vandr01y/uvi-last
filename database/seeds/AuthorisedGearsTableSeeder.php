<?php

use Illuminate\Database\Seeder;

class AuthorisedGearsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('authorised_gears')->insert([
                ['code' => 1, 'name' => "SURROUNDING NETS – Red de Cerco"],
                ['code' => 2, 'name' => "SEINE NETS"],
                ['code' => 3, 'name' => "TRAWLS – Trampas"],
                ['code' => 4, 'name' => "DREDGES –"],
                ['code' => 5, 'name' => "LIFT NETS"],
                ['code' => 6, 'name' => "FALLING GEAR"],
                ['code' => 7, 'name' => "GILLNETS AND ENTANGLING NETS"],
                ['code' => 8, 'name' => "TRAPS"],
                ['code' => 9, 'name' => "HOOKS AND LINES – Ganchos y líneas"],
                ['code' => 10, 'name' => "GRAPPLING AND WOUNDING"],
                ['code' => 11, 'name' => "HARVESTING MACHINES"],
                ['code' => 20, 'name' => "MISCELLANEOUS GEAR"],
                ['code' => 25, 'name' => "RECREATIONAL FISHING GEAR"],
                ['code' => 99, 'name' => "Unknown - Desconocido"],
        ]);
    }
}

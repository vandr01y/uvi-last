<?php

use Illuminate\Database\Seeder;

class BuquesArtesanalesArtePescaTipoRedTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('buques_artesanales_arte_pesca_tipo_red')->insert([
                ['name' => 'Red de tiro'],
                ['name' => 'Red de Enmalle'],
                ['name' => 'Red de Cerco'],
                ['name' => 'Red de Arrastre'],
                ['name' => 'Redes Izadas'],
                ['name' => 'Otro, Cual?'],
                ['name' => 'Chinchoro'],
                ['name' => 'Chinchorro Camaronero'],
                ['name' => 'Red de enmalle - camaronera'],
                ['name' => 'Red de enmalle - chichiguera'],
                ['name' => 'Red de enmalle - chechera'],
                ['name' => 'Red de enmalle - transparente'],
                ['name' => 'Red de enmalle - sierrera'],
                ['name' => 'Red de enmalle - caritera'],
                ['name' => 'Red de enmalle - robalera'],
                ['name' => 'Red de enmalle - jurelera'],
                ['name' => 'Red de enmalle - transparente'],
                ['name' => 'Red de enmalle - langostera'],
                ['name' => 'Red de enmalle - chuchera'],
                ['name' => 'Atarraya'],
                ['name' => 'Changa'],
        ]);
    }
}

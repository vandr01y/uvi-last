<?php

use Illuminate\Database\Seeder;

class OutcomesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('outcomes')->insert([
                ['code' => 1, 'name' => "Buque bajo investigación"],
                ['code' => 2, 'name' => "Advertencia, llamado de atención"],
                ['code' => 3, 'name' => "Adición / deducción de puntos de penalización"],
                ['code' => 4, 'name' => "Pena monetaria"],
                ['code' => 5, 'name' => "Retención temporal o permanente de capturas, artes, equipos y buques"],
                ['code' => 6, 'name' => "Restricción, suspensión o revocación de una autorización / licencia de pesca"],
                ['code' => 7, 'name' => "Inhabilitación temporal o permanente para poseer o solicitar una autorización / licencia de pesca"],
                ['code' => 8, 'name' => "Pérdida de la cuota de pesca"],
                ['code' => 9, 'name' => "Reembolso de la ayuda financiera"],
                ['code' => 10, 'name' => "Prohibición temporal o permanente del acceso a la asistencia pública, subvenciones o ayuda financiera"],
                ['code' => 98, 'name' => "Otros resultados del informe en el campo 'Detalles'"],
                ['code' => 99, 'name' => "No hay sanción, motivo del informe en el campo 'Detalles'"]
        ]);
    }
}

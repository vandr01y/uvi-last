<?php

use Illuminate\Database\Seeder;

class MaterialesLineaMadreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('materiales_linea_madre')->insert([
                ['name' => "Monofilamento"],
                ['name' => "Multifilamento"],
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class InfringementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('infringements')->insert([
                ['code' => 1, 'name' => "Pesca sin licencia, autorización o permiso válido"],
                ['code' => 2, 'name' => "Si no se mantienen registros exactos de las capturas y datos relacionados con las capturas o si se presentan datos erróneos de las capturas"],
                ['code' => 3, 'name' => "Pesca en zona cerrada, pesca en temporada de veda o pesca sin cuota"],
                ['code' => 4, 'name' => "Pesca dirigida a una población que está sujeta a veda o cuya pesca está prohibida"],
                ['code' => 5, 'name' => "Uso de equipo de pesca prohibido"],
                ['code' => 6, 'name' => "Falsificación u ocultación de las marcas, identidad o registro de un buque pesquero"],
                ['code' => 7, 'name' => "Ocultación, manipulación o eliminación de pruebas relacionadas con una investigación"],
                ['code' => 8, 'name' => "Varias violaciones que, en conjunto, constituyen un grave desprecio de las medidas de conservación y ordenación"],
                ['code' => 98, 'name' => "Otros, denunciar infracción en el campo 'Detalles'"],
                ['code' => 99, 'name' => "Sin infracción / infracción aparente"]
        ]);
    }
}

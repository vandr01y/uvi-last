<?php

use Illuminate\Database\Seeder;

class TiposArtefactosHerirAferrarTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipos_artefactos_herir_aferrar')->insert([
            ['name' => "Arpones"],
            ['name' => "Lanzas"],
            ['name' => "Flechas"],
            ['name' => "Pinchos"],
            ['name' => "Horcas"],
            ['name' => "Tenazas"],
            ['name' => "Otro, Cual?"],
        ]);
    }
}

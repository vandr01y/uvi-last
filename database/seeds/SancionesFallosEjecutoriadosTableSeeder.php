<?php

use Illuminate\Database\Seeder;

class SancionesFallosEjecutoriadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sanciones_fallos_ejecutoriados')->insert([
            ['code' => 1, 'name' => "EJECUTORIADO"],
            ['code' => 2, 'name' => "RECURSO DE REPOSICIÓN"],
        ]);
    }
}

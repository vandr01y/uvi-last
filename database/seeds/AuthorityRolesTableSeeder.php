<?php

use Illuminate\Database\Seeder;

class AuthorityRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('authority_roles')->insert([
            ['code' => 1, 'name' => "Estado de abanderamiento"],
            ['code' => 2, 'name' => "Estado del puerto"],
            ['code' => 3, 'name' => "Estado costero"],
            ['code' => 4, 'name' => "Esquema de inspección de las OROP"],
            ['code' => 5, 'name' => "Inspertor ocular - autoridad pesquera"],
            ['code' => 98, 'name' => "Other – Otro"]
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class MaterialesArtefactosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('materiales_artefactos')->insert([
            ['name' => "Acero"],
            ['name' => "Hierro"],
            ['name' => "Madera"],
            ['name' => "Otro, Cual?"],
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class VesselTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vessel_types')->insert([
            ['name' => 'Arrastrero'],
            ['name' => 'Cerquero'],
            ['name' => 'Barco con red izada'],
            ['name' => 'Barco de línea'],
            ['name' => 'Barco con trampa'],
            ['name' => 'Arrastrero cerquero'],
            ['name' => 'Barco con líneas de mano'],
            ['name' => 'Barco polivalente']
        ]);
    }
}

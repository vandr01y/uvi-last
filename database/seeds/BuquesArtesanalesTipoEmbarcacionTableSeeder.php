<?php

use Illuminate\Database\Seeder;

class BuquesArtesanalesTipoEmbarcacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('buque_artesanal_tipo_embarcacion')->insert([
                ['name' => 'Cayuco'],
                ['name' => 'Viento y marea'],
                ['name' => 'King fiber'],
                ['name' => 'pangas'],
                ['name' => 'Go fast'],
                ['name' => 'Lancha'],
                ['name' => 'Barco'],
        ]);
    }
}

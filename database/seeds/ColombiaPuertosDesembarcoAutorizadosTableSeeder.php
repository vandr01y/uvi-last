<?php

use Illuminate\Database\Seeder;

class ColombiaPuertosDesembarcoAutorizadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('colombia_puertos_desembarco_autorizados')->insert([
            ['name' => "BUENAVENTURA"],
            ['name' => "TUMACO"],
            ['name' => "TOLU"],
            ['name' => "CARTAGENA"],
            ['name' => "BARRANQUILLA"],
            ['name' => "SAN ANDRES"],
        ]);
    }
}

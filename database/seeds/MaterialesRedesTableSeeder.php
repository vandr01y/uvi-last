<?php

use Illuminate\Database\Seeder;

class MaterialesRedesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('materiales_redes')->insert([
                ['name' => "Monofilamento"],
                ['name' => "Multifilamento"],
                ['name' => "Otro, ¿Cuál?"],
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);

        $this->call(AuthorisationHoldersTableSeeder::class);
        $this->call(AuthorisedAreasTableSeeder::class);
        $this->call(AuthorisedGearsTableSeeder::class);
        $this->call(AuthorityRolesTableSeeder::class);
        $this->call(ColombiaPuertosDesembarcoAutorizadosTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(DenominacionArtePescaTipoRedesTableSeeder::class);
        $this->call(DenominacionArtePescaTipoSedalAnzuelosTableSeeder::class);
        $this->call(DenominacionArtePescaTipoTrampasNasasTableSeeder::class);
        $this->call(FishHoldTypesTableSeeder::class);
        $this->call(HullMaterialsTableSeeder::class);
        $this->call(InfringementsTableSeeder::class);
        $this->call(MaterialesLineaMadreTableSeeder::class);
        $this->call(MaterialesArtefactosTableSeeder::class);
        $this->call(MaterialesBajantesTableSeeder::class);
        $this->call(MaterialesRedesTableSeeder::class);
        $this->call(MaterialesTipoMarcoTableSeeder::class);
        $this->call(MaterialesTrampasNasasTableSeeder::class);
        $this->call(MaterialesPrincipalesTrampasNasas::class);
        $this->call(OropsTableSeeder::class);
        $this->call(OutcomesTableSeeder::class);
        $this->call(PesqueriaAutorizadasTableSeeder::class);
        $this->call(PowerUnitsTableSeeder::class);
        $this->call(ReasonsForRevocationsTableSeeder::class);
        $this->call(RegistrationPortsTableSeeder::class);
        $this->call(ReportTypesTableSeeder::class);
        $this->call(TiposAnzuelosTableSeeder::class);
        $this->call(TiposArtefactosHerirAferrarTableSeeder::class);
        $this->call(TiposFadTableSeeder::class);
        $this->call(VesselTypesTableSeeder::class);
        $this->call(VmsTypesTableSeeder::class);
        $this->call(ZonasPescaAutorizadasTableSeeder::class);
        $this->call(SancionesTiposInfraccionesTableSeeder::class);
        $this->call(SancionesTiposTableSeeder::class);
        $this->call(SancionesFallosEjecutoriadosTableSeeder::class);
        $this->call(SancionesEstadosTableSeeder::class);
        $this->call(BuquesArtesanalesTipoEmbarcacionTableSeeder::class);
        $this->call(BuquesArtesanalesCuencasTableSeeder::class);
        $this->call(ColombiaDepartamentosTableSeeder::class);
        $this->call(ColombiaMunicipiosTableSeeder::class);
        $this->call(BuquesArtesanalesTiposMotorTableSeeder::class);
        $this->call(BuquesArtesanalesTiposPropulsionTableSeeder::class);
        $this->call(ChalecosSalvavidasEstadosTableSeeder::class);
        $this->call(BuquesArtesanalesArtePescaTipoRedTableSeeder::class);
        $this->call(BuquesArtesanalesLineasManoTableSeeder::class);
    }
}

<?php

use Illuminate\Database\Seeder;

class PesqueriaAutorizadasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pesqueria_autorizadas')->insert([
                ['name' => "Camarón de Aguas Someras - CAS"],
                ['name' => "Camarón de Aguas Profundas CAP"],
                ['name' => "Pesca Blanca"],
                ['name' => "Atún"],
                ['name' => "Atún y Pesca Blanca"],
                ['name' => "Caracol"],
                ['name' => "Langosta"],
                ['name' => "Otra"],
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class DenominacionArtePescaTipoRedesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('denominacion_arte_pesca_tipo_redes')->insert([
            ['name' => "Red de tiro"],
            ['name' => "Red de Enmalle"],
            ['name' => "Red de Cerco"],
            ['name' => "Red de Arrastre"],
            ['name' => "Redes Izadas"],
            ['name' => "Arte de anzuelo"],
            ['name' => "Nasas - trampas"],
            ['name' => "Otro, Cual?"],
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class ReasonsForRevocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reasons_for_revocations')->insert([
                ['code' => 1, 'name' => "Violación de las condiciones del permiso"],
                ['code' => 2, 'name' => "Sanción en vigor por infracción de las condiciones del permiso de pesca"],
                ['code' => 3, 'name' => "Número máximo de sanciones acumuladas o infracciones graves comprobada."],
                ['code' => 4, 'name' => "Reducción de las posibilidades de pesca en una medida que no permita acuerdos en el permiso de pesca."],
                ['code' => 5, 'name' => "Pesca no permitida por la legislación que regula la pesca, o por el Estado u organización internacional que regula la pesca en el área pesquera"],
                ['code' => 6, 'name' => "Revocación del permiso de pesca o supresión del registro del Estado de abanderamiento del buque indicado en la autorización"],
                ['code' => 7, 'name' => "No uso del buque por tiempo o periodo estipulado"],
                ['code' => 8, 'name' => "No informar de los cambios en la propiedad y el uso del buque"],
                ['code' => 9, 'name' => "Incumplimiento de los requisitos de información de datos o monitoreo de la posición del buque"],
                ['code' => 10, 'name' => "Incumplimiento de la presencia de observadores o inspectores según se requiera"],
                ['code' => 11, 'name' => "No pago de derechos o tasas de pesca"],
                ['code' => 12, 'name' => "Cuota alcanzada"],
                ['code' => 98, 'name' => "Other – Otro"]
        ]);
    }
}

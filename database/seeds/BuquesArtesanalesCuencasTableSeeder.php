<?php

use Illuminate\Database\Seeder;

class BuquesArtesanalesCuencasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('buque_artesanal_cuencas')->insert([
                ['name' => 'Caribe'],
                ['name' => 'Pacífico'],
                ['name' => 'San Andrés'],
                ['name' => 'Otro, Cual?'],
        ]);
    }
}

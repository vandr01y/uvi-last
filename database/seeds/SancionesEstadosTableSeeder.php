<?php

use Illuminate\Database\Seeder;

class SancionesEstadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sanciones_estados')->insert([
            ['code' => 1, 'name' => "CUMPLIDA"],
            ['code' => 2, 'name' => "INCUMPLIDA"],
        ]);
    }
}

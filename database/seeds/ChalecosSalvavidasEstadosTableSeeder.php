<?php

use Illuminate\Database\Seeder;

class ChalecosSalvavidasEstadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('chalecos_salvavidas_estados')->insert([
                    ['name' => 'BUENO'],
                    ['name' => 'REGULAR'],
                    ['name' => 'MALO'],
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class PowerUnitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('power_units')->insert([
            ['code' => 'KW', 'name' => "Kilowatt"],
            ['code' => 'HP', 'name' => "Horse Power"],
            ['code' => 'OT', 'name' => "Other"]
        ]);
    }
}

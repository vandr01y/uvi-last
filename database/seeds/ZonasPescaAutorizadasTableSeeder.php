<?php

use Illuminate\Database\Seeder;

class ZonasPescaAutorizadasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('zonas_pesca_autorizadas')->insert([
                ['name' => "Caribe"],
                ['name' => "Pacífico"],
                ['name' => "Oceáno Pacífico Oriental - OPO"],
                ['name' => "San Andrés y Providencia"],
                ['name' => "Otro"],
        ]);
    }
}

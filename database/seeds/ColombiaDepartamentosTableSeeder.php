<?php

use Illuminate\Database\Seeder;

class ColombiaDepartamentosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('colombia_departamentos')->insert([
            ['code' => 8, 'name' => 'ATLÁNTICO'], // id=1
        	['code' => 11, 'name' => 'BOGOTÁ, D.C.'], // id=2
        	['code' => 13, 'name' => 'BOLÍVAR'], // id=3
        	['code' => 15, 'name' => 'BOYACÁ'], // id=4
        	['code' => 17, 'name' => 'CALDAS'], // id=5
        	['code' => 18, 'name' => 'CAQUETÁ'], // id=6
        	['code' => 19, 'name' => 'CAUCA'], // id=7
        	['code' => 20, 'name' => 'CESAR'], // id=8
        	['code' => 23, 'name' => 'CÓRDOBA'], // id=9
        	['code' => 25, 'name' => 'CUNDINAMARCA'], // id=10
        	['code' => 27, 'name' => 'CHOCÓ'], // id=11
        	['code' => 41, 'name' => 'HUILA'], // id=12
        	['code' => 44, 'name' => 'LA GUAJIRA'], // id=13
        	['code' => 47, 'name' => 'MAGDALENA'], // id=14
        	['code' => 50, 'name' => 'META'], // id=15
        	['code' => 52, 'name' => 'NARIÑO'], // id=16
        	['code' => 54, 'name' => 'NORTE DE SANTANDER'], // id=17
        	['code' => 63, 'name' => 'QUINDIO'], // id=18
        	['code' => 66, 'name' => 'RISARALDA'], // id=19
        	['code' => 68, 'name' => 'SANTANDER'], // id=20
        	['code' => 70, 'name' => 'SUCRE'], // id=21
        	['code' => 73, 'name' => 'TOLIMA'], // id=22
        	['code' => 76, 'name' => 'VALLE DEL CAUCA'], // id=23
        	['code' => 81, 'name' => 'ARAUCA'], // id=24
        	['code' => 85, 'name' => 'CASANARE'], // id=25
        	['code' => 86, 'name' => 'PUTUMAYO'], // id=26
        	['code' => 88, 'name' => 'ARCHIPIÉLAGO DE SAN ANDRÉS, PROVIDENCIA Y SANTA CATALINA'], // id=27
        	['code' => 91, 'name' => 'AMAZONAS'], // id=28
        	['code' => 94, 'name' => 'GUAINÍA'], // id=29
        	['code' => 95, 'name' => 'GUAVIARE'], // id=30
        	['code' => 97, 'name' => 'VAUPÉS'], // id=31
        	['code' => 99, 'name' => 'VICHADA'], // id=32
        ]);
    }
}

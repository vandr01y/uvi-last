<?php

use Illuminate\Database\Seeder;

class TiposFadTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipos_fad')->insert([
            ['name' => "FAD Natural"],
            ['name' => "FAD Artificial"],
            ['name' => "Payao"],
            ['name' => "Arrecife artificial"],
            ['name' => "Casita cubana"],
            ['name' => "Otro, Cual?"],
        ]);
    }
}

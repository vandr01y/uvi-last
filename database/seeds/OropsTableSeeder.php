<?php

use Illuminate\Database\Seeder;

class OropsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orops')->insert([
            ['siglas' => 'ICCAT', 'name'=> 'Comisión Internacional para la Conservación del Atún Atlántico (CICAA)'],
            ['siglas' => 'IATTC', 'name'=> 'Comisión Interamericana del Atún Tropical (CIAT)'],
            ['siglas' => 'SPRFMO', 'name'=> 'Organización Regional de Ordenación Pesquera del Pacífico Sur'],
            ['siglas' => 'Comisión', 'name'=> ' Pesca del Atlántico Centro Occidental (COPACO)'],
            ['siglas' => 'CIAT', 'name'=> 'Centro Internacional de Agricultura Tropical.']
        ]);
    }
}

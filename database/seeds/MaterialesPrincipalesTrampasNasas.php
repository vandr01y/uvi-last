<?php

use Illuminate\Database\Seeder;

class MaterialesPrincipalesTrampasNasas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('materiales_principales_trampas_nasas')->insert([
            ['name' => 'Madera y malla metálica'],
            ['name' => 'Madera y malla plastica'],
            ['name' => 'Madera y malla en polifilamento'],
            ['name' => 'Varilla y malla metálica'],
            ['name' => 'Varilla y malla plastica'],
            ['name' => 'Varilla y malla en polifilamento'],
            ['name' => 'Plástico'],
            ['name' => 'Otro, Cual?'],
        ]);
    }
}

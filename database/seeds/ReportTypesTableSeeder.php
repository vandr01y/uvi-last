<?php

use Illuminate\Database\Seeder;

class ReportTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('report_types')->insert([
            ['code' => 1, 'name' => "Inspección del puerto"],
            ['code' => 2, 'name' => "Inspección en el mar"],
            ['code' => 3, 'name' => "Inspección de transbordo - buque receptor"],
            ['code' => 4, 'name' => "Inspección de transbordo - buque donante"],
            ['code' => 5, 'name' => "Observación del buque"],
            ['code' => 98, 'name' => "Other – Otro"]
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class MaterialesTipoMarcoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('materiales_tipo_marco')->insert([
            ['name' => "Tubo"],
            ['name' => "Barra plana"],
        ]);
    }
}

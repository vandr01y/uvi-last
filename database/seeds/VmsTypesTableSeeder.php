<?php

use Illuminate\Database\Seeder;

class VmsTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vms_types')->insert([
                ['code' => 1, 'name' => "Nacional"],
                ['code' => 2, 'name' => "OROP"],
                ['code' => 8, 'name' => "Otros"],
                ['code' => 9, 'name' => "Desconocido"],
        ]);
    }
}

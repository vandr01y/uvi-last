<?php

use Illuminate\Database\Seeder;

class RegistrationPortsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('registration_ports')->insert([
            ['name' => 'Buenaventura'],
            ['name' => 'Tumaco'],
            ['name' => 'Cartagena'],
            ['name' => 'Coveñas'],
            ['name' => 'Guapi'],
            ['name' => 'Golfito, Costa Rica'],
            ['name' => 'Guayaquil'],
            ['name' => 'Manta'],
            ['name' => 'Honduras'],
            ['name' => 'Roatan'],
            ['name' => 'Kensennuma'],
            ['name' => 'Japón'],
            ['name' => 'Toba-Japón'],
            ['name' => 'Busan'],
            ['name' => 'Korea'],
            ['name' => 'Ensenada'],
            ['name' => 'Nicaragua'],
            ['name' => 'Managua'],
            ['name' => 'Panamá'],
            ['name' => 'Taiwán'],
            ['name' => 'Sucre, Venezuela'],
            ['name' => 'Caracas'],
            ['name' => 'Venezuela'],
            ['name' => 'pendiente'],
            ['name' => 'Ecuador'],
            ['name' => 'Puerto Corinto'],
            ['name' => 'Santa Marta'],
            ['name' => 'Providencia'],
            ['name' => 'Callao-Perú'],
            ['name' => 'Colombia']
        ]);
    }
}

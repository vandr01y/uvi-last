<?php

use Illuminate\Database\Seeder;

class SancionesTiposInfraccionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sanciones_tipos_infracciones')->insert([
            ['code' => 1, 'name' => 'Pesca sin permiso o sin patente de pesca'],
            ['code' => 2, 'name' => 'Sobrepasar la cuota de pesca'],
            ['code' => 3, 'name' => 'Pesca Comercial Industrial en Zonas de Reserva de Pesca Artesanal'],
            ['code' => 4, 'name' => 'Pesca Comercial en Área Protegida – Extraer recursos de áreas reservadas'],
            ['code' => 5, 'name' => 'Usar artes de pesca no autorizados'],
            ['code' => 6, 'name' => 'Descartes de Fauna Acompañante'],
            ['code' => 7, 'name' => 'Pesca o comercialización de especies sin TMC'],
            ['code' => 8, 'name' => 'Transbordos'],
            ['code' => 9, 'name' => 'Incumplimiento en Zonas 1, 2, 3- Pesca Pacífico'],
            ['code' => 10, 'name' => 'Pesca Industrial de CAS en DRMI - Pacífico'],
            ['code' => 11, 'name' => 'No llevar paño protector para delfines'],
            ['code' => 12, 'name' => 'No llevar equipos de protección'],
            ['code' => 13, 'name' => 'LMD'],
            ['code' => 14, 'name' => 'Pesca durante Veda de atún'],
            ['code' => 15, 'name' => 'Pesca durante Veda de Camarón'],
            ['code' => 16, 'name' => 'No DETS'],
            ['code' => 17, 'name' => 'Captura dirigida a pez vela, marlín, pez espada'],
            ['code' => 18, 'name' => 'Aleteo de tiburón'],
            ['code' => 19, 'name' => 'Pesca dirigida a tiburones'],
            ['code' => 20, 'name' => 'transporte de contrabando'],
            ['code' => 21, 'name' => 'Otras, ¿cuál?'],
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class AuthorisationHoldersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('authorisation_holders')->insert([
                ['code' => 1, 'name' => "Buque"],
                ['code' => 2, 'name' => "Propietario del buque"],
                ['code' => 3, 'name' => "Operador / Gerente de Buques"],
                ['code' => 4, 'name' => "Capitán del barco"]
        ]);
    }
}

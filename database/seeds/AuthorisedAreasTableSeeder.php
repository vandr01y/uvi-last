<?php

use Illuminate\Database\Seeder;

class AuthorisedAreasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('authorised_areas')->insert([
                ['code' => 31, 'name' => "Atlantic, Western Central"],
                ['code' => 77, 'name' => "Pacific, Eastern Central"],
                ['code' => 87, 'name' => "Pacific, Southeast "],
                ['code' => 98, 'name' => "Other – Otro"],
                ['code' => 99, 'name' => "Unknown - Desconocido"]
        ]);
    }
}

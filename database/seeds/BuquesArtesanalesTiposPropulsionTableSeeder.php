<?php

use Illuminate\Database\Seeder;

class BuquesArtesanalesTiposPropulsionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('buque_artesanal_tipos_propulsion')->insert([
                ['name' => 'Vela'],
                ['name' => 'Remo'],
                ['name' => 'Vara'],
                ['name' => 'Motor Fuera de Borda'],
                ['name' => 'Motor Interno'],
                ['name' => 'Otro'],
                ['name' => 'Desconocido'],
        ]);
    }
}

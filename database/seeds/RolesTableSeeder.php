<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name' => 'Administrador'
            ],
            [
                'name' => 'Usuario regional'
            ],
            [
                'name' => 'Director AUNAP'
            ],
            [
                'name' => 'ARMADOR'
            ],
            [
                'name' => 'Usuario Externo - MINAGRICULTURA'
            ],
            [
                'name' => 'Usuario Externo - DIMAR'
            ],
            [
                'name' => 'Usuario Externo - GUARDACOSTAS'
            ],
            [
                'name' => 'Usuario Externo - UPME'
            ],
            [
                'name' => 'Usuario Externo - CIUDADANO'
            ]
        ]);
    }
}

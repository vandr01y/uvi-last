<?php

use Illuminate\Database\Seeder;

class SancionesTiposTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sanciones_tipos')->insert([
                ['code' => 1, 'name' => 'Conminación por escrito'],
                ['code' => 2, 'name' => 'Multa'],
                ['code' => 3, 'name' => 'Suspensión temporal del permiso, Autorización, concesión o patente, según sea el caso'],
                ['code' => 4, 'name' => 'Revocatoria del permiso, Autorización, concesión o patente, según sea el caso'],
                ['code' => 5, 'name' => 'Decomiso de embarcaciones'],
                ['code' => 6, 'name' => 'Decomiso de productos'],
                ['code' => 7, 'name' => 'Decomiso de productos'],
                ['code' => 8, 'name' => 'Cierre temporal o clausura definitiva del establecimiento'],
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class FishHoldTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fish_hold_types')->insert([
                ['code' => 1, 'name' => 'Fresh – Fresco'],
                ['code' => 2, 'name' => 'Live – Vivo'],
                ['code' => 3, 'name' => 'Frozen – Congelado'],
                ['code' => 98, 'name' => 'Other – Otro'],
                ['code' => 99, 'name' => 'Unknown - Desconocido']
        ]);
    }
}

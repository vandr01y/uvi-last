<?php

use Illuminate\Database\Seeder;

class BuquesArtesanalesTiposMotorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('buque_artesanal_tipos_motor')->insert([
                ['name' => 'Motor Fuera de Borda'],
                ['name' => 'Motor Interno'],
                ['name' => 'Ninguno'],
        ]);
    }
}

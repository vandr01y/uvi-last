<?php

use Illuminate\Database\Seeder;

class TiposAnzuelosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipos_anzuelos')->insert([
                ['name' =>"Jota"],
                ['name' =>"Circular"],
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class BuquesArtesanalesLineasManoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('buques_artesanales_lineas_mano')->insert([
                ['name' => "Long line"],
                ['name' => "Espinel"],
                ['name' => "Palangre de fondo"],
                ['name' => "Palangre de deriva"],
                ['name' => "Curricanes"],
                ['name' => "Ballestilla"],
                ['name' => "Cordel"],
                ['name' => "Otro, Cual?"],
        ]);
    }
}

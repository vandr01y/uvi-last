const DATATABLES_LAN = {
        emptyTable: "No hay información disponible",
        info: "Mostrando _START_ a _END_ de _TOTAL_ registros",
        infoEmpty: "No hay coincidencias",
        infoFiltered: "(Filtrando de _MAX_ registros totales)",
        lengthMenu: "Mostrar _MENU_ registros",
        loadingRecords: "Cargando...",
        processing: "Procesando...",
        search: "Buscar:",
        zeroRecords: "No se encontraron registros",
        paginate: {
            first: "Inicio",
            last: "Final",
            next: "Siguiente",
            previous: "Anterior"
        }
};

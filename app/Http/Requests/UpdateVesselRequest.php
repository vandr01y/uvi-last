<?php

namespace Uvi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateVesselRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'numero_omi' => 'required_if:form_tab,1|numeric|digits_between:1,20',
            'estado_actual_pabellon' => 'required_if:form_tab,3|numeric',
            'nombre_barco' => 'required_if:form_tab,3',
            'longitud_total' => 'required_if:form_tab,4|numeric',
            'arqueo_bruto' => 'required_if:form_tab,4|numeric',
            'tonelaje_registro_bruto' => 'required_if:form_tab,4|numeric',
            'indicador_vms' => 'numeric|boolean', // FORM_1
            'marcado_externo' => 'string|max:100',
            'mlri' => 'string|max:100',
            'mmsi' => 'string|max:100',
            'tipo_vms' => 'numeric',
            'sistema_ais' => 'numeric|boolean',
            'status' => 'numeric|boolean',
            'orop_id' => 'numeric', // FORM_2
            'identificador_regional_cuerpo' => 'string|max:100',
            'puerto_registro' => 'numeric', // FORM_3
            'numero_registro_nacional' => 'string|max:100',
            'nombre_barco' => 'string|max:100',
            'nombre_barco_ingles' => 'string|max:100',
            'tipo_barco' => 'numeric',
            'funcionamiento' => 'numeric|boolean',
            'eslora_perpendiculares' => 'numeric', // FORM_4
            'longitud_registrada' => 'numeric',
            'manga' => 'numeric',
            'puntal' => 'numeric',
            'calado' => 'numeric',
            'peso_muerto' => 'numeric',
            'tonelaje_neto' => 'numeric',
            'tonelaje_registro_neto' => 'numeric',
            'potencia_motor_principal' => 'numeric',
            'unidad_potencia' => 'numeric',
            'material_casco' => 'numeric',
            'capacidad_bodega_pescado' => 'numeric',
            'tipo_bodega_pescado' => 'numeric',
            'anio_construccion' => 'numeric', // FORM_5
            'pais_construccion' => 'numeric',
            'nombre_propietario' => 'string|max:100', // FORM_6
            'numero_omi_propietario' => 'numeric|digits_between:1,20',
            'ciudad_propietario' => 'string|max:100',
            'codigo_postal_propietario' => 'numeric',
            'nacionalidad_propietario' => 'numeric',
            'nombre_operador' => 'string|max:100',
            'numero_omi_operador' => 'numeric|digits_between:1,20',
            'ciudad_operador' => 'string|max:100',
            'codigo_postal_operador' => 'numeric',
            'nacionalidad_operador' => 'numeric',
            'nombre_administrador' => 'string|max:100',
            'numero_omi_administrador' => 'numeric|digits_between:1,20',
            'ciudad_administrador' => 'string|max:100',
            'codigo_postal_administrador' => 'numeric',
            'nacionalidad_administrador' => 'numeric',
            'permiso_vigente' => 'numeric|boolean', // FORM_7
            'numero_permiso' => 'string|max:100',
            'ultima_resolucion' => 'string|max:100',
            'vigencia_permiso' => 'numeric',
            'zona_pesca_autorizada' => 'numeric',
            'puerto_desembarco' => 'numeric',
            'pesqueria_autorizada' => 'numeric',
            'arte_pesca_red' => 'numeric', // FORM_8
            'ojo_malla' => 'numeric',
            'ojo_malla_copo' => 'numeric',
            'longitud_red' => 'numeric',
            'ancho_red' => 'numeric',
            'cantidad_piezas' => 'numeric',
            'material_red' => 'numeric',
            'arte_pesca_anzuelo' => 'numeric',
            'tipo_anzuelo' => 'numeric',
            'tamanio_anzuelo' => 'numeric',
            'cantidad_anzuelos' => 'numeric',
            'longitud_linea_madre' => 'string|max:100',
            'material_linea_madre' => 'numeric',
            'material_bajantes' => 'numeric',
            'cantidad_total_lineas' => 'numeric',
            'arte_pesca_otros' => 'string|max:100',
            'arte_pesca_trampa' => 'numeric',
            'cantidad_trampas' => 'numeric',
            'materiales_trampas' => 'numeric',
            'tipo_artefactos_herir' => 'numeric',
            'cantidad_artefactos' => 'numeric',
            'material_artefactos' => 'numeric',
            'fad' => 'numeric|boolean',
            'tipos_fad' => 'numeric',
            'cantidad_fad' => 'numeric',
            'tipo_material_det' => 'numeric', // FORM_9
            'distancia_barras_det' => 'string|max:100',
            'altura_parrilla_det' => 'string|max:100',
            'ancho_parrilla_det' => 'string|max:100',
            'angulo_inclinacion_derecho_det' => 'string|max:100',
            'angulo_inclinacion_izquierdo_det' => 'string|max:100',
            'amplitud_minima_escape_solapa' => 'string|max:100',
            'amplitud_minima_escape_solapa_doble' => 'string|max:100',
            'longitud_relinga_inferior' => 'string|max:100',
            'traslape' => 'string|max:100',
            'redes_repuesto' => 'numeric',
            'parrillas_repuesto' => 'numeric',
        ];

        $files = count($this->input('photos'));
        foreach(range(0, $files) as $index)
        {
            $rules['photos.' . $index] = 'mimes:png,jpeg,jpg,gif|image|max:2048';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            '*.required' => 'Ingrese el campo',
            '*.numeric' => 'Ingrese un número',
            '*.boolean' => 'Elija una opción',
            '*.digits_between' => 'El número debe tener entre :min y :max dígitos',
            '*.required_if' => 'Ingrese el campo',
            '*.uploaded' => 'Ha fallado la subida del archivo, revise los requerimientos',
            '*.max' => 'El campo no debe ser mayor a :max caracteres',
            'photos.*.image' => 'Formato de archivo incorrecto',
            'photos.*.mimes' => 'Formato de archivo incorrecto',
            'photos.*.max' => 'El tamaño del archivo supera los :max bytes',
        ];
    }
}

<?php

namespace Uvi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBuqueArtesanalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nombre' => 'required_if:form_tab,1|max:100',
            'matricula' => 'max:100',
            'numero_identificacion_embarcacion' => 'max:100',
            //FORM_2
            'bandera_id' => 'numeric',
            'puerto_id' => 'numeric',
            'tipo_id' => 'numeric',
            'estado_operacional' => 'required_if:form_tab,2|numeric|boolean',
            'cuenca_id' => 'required_if:form_tab,2|numeric',
            'departamento_id' => 'required_if:form_tab,2|numeric',
            'municipio_id' => 'required_if:form_tab,2|numeric',
            'localidad' => 'required_if:form_tab,2',
            'lugar_desembarco' => 'required_if:form_tab,2|max:100',
            'permiso_pesca' => 'required_if:form_tab,2|numeric|boolean',
            'numero_resolucion' => 'numeric',
            'vigencia_permiso' => 'numeric',
            //FORM_3
            'eslora' => 'required_if:form_tab,3|numeric',
            'manga' => 'required_if:form_tab,3|numeric',
            'puntal' => 'required_if:form_tab,3|numeric',
            'calado' => 'numeric',
            'trn' => 'numeric',
            'trb' => 'numeric',
            //FORM_4
            'lugar_construccion' => 'max:100',
            'astillero_construccion' => 'max:100',
            'carpintero_construccion' => 'max:150',
            'material_embarcacion_id' => 'required_if:form_tab,4|numeric',
            //FORM_5
            'tipo_propulsion_id' => 'required_if:form_tab,5|numeric',
            'tipo_motor_id' => 'numeric',
            'marca_motor' => 'max:100',
            'potencia_motor' => 'max:100',
            'numero_motor' => 'numeric|',
            'tipo_combustible' => 'max:100',
            //FORM_6
            'nombre_propietario' => 'required_if:form_tab,6|max:100',
            'numero_identificacion' => 'required_if:form_tab,6|numeric',
            'direccion_residencia' => 'required_if:form_tab,6|max:200',
            'ciudad_municipio' => 'required_if:form_tab,6|max:100',
            'telefono_fijo' => 'numeric',
            'telefono_celular' => 'numeric',
            'carne_pesca' => 'required_if:form_tab,6|numeric|boolean',
            'numero_carne_pesca' => 'numeric',
            'afiliacion' => 'required_if:form_tab,6|numeric|boolean',
            'nombre_organizacion' => 'max:100',
            'numero_tripulantes' => 'required_if:form_tab,6|numeric',
            'nombre_tripulacion' => 'max:250',
            'numero_documentos_tripulacion' => 'numeric',
            'direccion_residencia_tripulacion' => 'max:500',
            'telefono_contacto_tripulacion' => 'max:500',
            'numero_carne_pesca_tripulacion' => 'max:500',
            // FORM_7
            'radio_comunicaciones' => 'required_if:form_tab,7|numeric|boolean',
            'marca_radio_comunicaciones' => 'max:100',
            'frecuencia_asignada' => 'numeric',
            'brujula' => 'numeric|boolean',
            'marca_brujula' => 'max:100',
            'gps' => 'required_if:form_tab,7|numeric|boolean',
            'marca_gps' => 'max:100',
            'chalecos_salvavidas' => 'required_if:form_tab,7|numeric|boolean',
            'numero_chalecos_salvavidas' => 'numeric',
            'estado_condicion_chalecos_salvavidas_id' => 'numeric',
            //FORM_8
            'tipo_red_id' => 'numeric ',
            'ojo_malla' => 'numeric',
            'ojo_malla_copo' => 'numeric',
            'longitud_red' => 'numeric',
            'ancho_red' => 'numeric',
            'cantidad_panos_piezas' => 'numeric',
            'material_red_id' => 'numeric',
            'zonas_pesca_frecuentadas' => 'max:255',
            'tipos_lineas_mano_id' => 'numeric',
            'trampas_id' => 'numeric',
            'cantidad_trampas' => 'numeric',
            'material_principal_trampas_id' => 'numeric',
            'zonas_pesca_trampas' => 'max:255',
            'tipos_artefactos_herir_aferrar_id' => 'numeric',
            'cantidad_artefactos' => 'numeric',
            'material_artefacto_id' => 'numeric',
            'zonas_pesca_artefactos' => 'max:255',
            'otros' => 'required_if:form_tab,8|numeric|boolean',
            'artes_metodos_poco_convencionales' => 'max:255',
            'cantidad_artes_metodos_poco_convencionales' => 'numeric',
            'uso_fad' => 'required_if:form_tab,8|numeric|boolean',
            'tipos_fad_id' => 'numeric',
            'cantidad_fad' => 'numeric',
        ];

        if($this->input('form_tab') == 8)
        {
            $files = count($this->input('photos_8')) - 1;
            foreach(range(0, $files) as $index)
            {
                $rules['photos_8.' . $index] = 'mimes:png,jpeg,jpg,gif|image|max:2048';
            }
        }

        if($this->input('form_tab') == 9)
        {
            $files = count($this->input('photos_9')) - 1;
            foreach(range(0, $files) as $index)
            {
                $rules['photos_9.' . $index] = 'mimes:png,jpeg,jpg,gif|image|max:2048';
            }
        }

        return $rules;
    }

    public function messages()
    {
        return [
            '*.required' => 'Ingrese el campo',
            '*.required_if' => 'Ingrese el campo',
            '*.numeric' => 'El campo debe ser numérico',
            '*.boolean' => 'Elija una opción',
            '*.max' => 'El campo no debe ser mayor a :max caracteres',
            'photos_f8.*.image' => 'Tipo de archivo incorrecto',
            'photos_f8.*.mimes' => 'Formato de archivo incorrecto',
            'photos_f8.*.max' => 'El tamaño del archivo supera los :max bytes',
            'photos_f9.*.image' => 'Tipo de archivo incorrecto',
            'photos_f9.*.mimes' => 'Formato de archivo incorrecto',
            'photos_f9.*.max' => 'El tamaño del archivo supera los :max bytes',
        ];
    }
}

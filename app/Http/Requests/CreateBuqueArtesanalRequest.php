<?php

namespace Uvi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateBuqueArtesanalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nombre' => 'required|max:100',
            'matricula' => 'max:100',
            'numero_identificacion_embarcacion' => 'max:100',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            '*.required' => 'Ingrese el campo',
            '*.max' => 'El campo no debe ser mayor a :max caracteres',
        ];
    }
}

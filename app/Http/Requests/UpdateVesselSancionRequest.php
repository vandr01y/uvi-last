<?php

namespace Uvi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateVesselSancionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'vessel_id' => 'required|numeric',
            'presunta_violacion' => 'required|boolean',
            'nur' => 'required|numeric',
            'infraccion_proceso_id' => 'required',
            'sanciones_estatuto_pesquero' => 'required|boolean',
            'numero_acto_administrativo' => 'required|numeric|digits_between:1,17',
            'fecha_sancion' => 'required',
            'infraccion_sancionada_id' => 'numeric',
            'tipo_sancion_id' => 'numeric',
            'fallo_ejecutoriado_id' => 'numeric',
            'valor_multa' => 'required|numeric',
            'estado_sancion_id' => 'required',
            'status' => 'required|boolean',
        ];

        $files = count($this->input('attachments'));
        foreach(range(0, $files) as $index)
        {
            $rules['attachments.' . $index] = 'mimes:pdf|max:5120';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            '*.required' => 'Se requiere la información',
            '*.numeric' => 'Ingrese un número',
            '*.digits_between' => 'El numero debe tener de :min a :max dígitos',
            '*.boolean' => 'Elija una opción',
            'attachments.*.mimes' => 'Formato de archivo incorrecto',
            'attachments.*.max' => 'El tamaño del archivo supera los :max bytes',
        ];
    }
}

<?php

namespace Uvi\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateVesselRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            //'numero_omi' => 'required|numeric|digits_between:1,20',
            'numero_omi' => 'required|max:20',
            'marcado_externo' => 'string|max:100',
            'mlri' => 'string|max:100',
            'mmsi' => 'string|max:100',
            'indicador_vms' => 'numeric|boolean',
            'tipo_vms' => 'numeric',
            'sistema_ais' => 'numeric|boolean',
            'status' => 'numeric|boolean',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            '*.required' => 'Ingrese el campo',
            '*.numeric' => 'Ingrese un número',
            '*.boolean' => 'Elija una opción',
            '*.digits_between' => 'El número debe tener entre :min y :max dígitos',
            '*.max' => 'El campo no debe ser mayor a :max caracteres',
        ];
    }
}

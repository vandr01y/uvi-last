<?php

namespace Uvi\Http\Controllers;

use Uvi\Vessel;

// REFERENCES
use Uvi\ReferencesModels\Vessels\RefVmsTypes;
use Uvi\ReferencesModels\Vessels\RefOrops;
use Uvi\ReferencesModels\Vessels\RefCountries;
use Uvi\ReferencesModels\Vessels\RefVesselTypes;
use Uvi\ReferencesModels\Vessels\RefRegistrationPorts;
use Uvi\ReferencesModels\Vessels\RefPowerUnits;
use Uvi\ReferencesModels\Vessels\RefHullMaterials;
use Uvi\ReferencesModels\Vessels\RefFishHoldTypes;
use Uvi\ReferencesModels\Vessels\RefAuthorisationHolders;
use Uvi\ReferencesModels\Vessels\RefAuthorisedAreas;

// Vessels Relationships
use Uvi\RelationshipsModels\Vessels\VesselPictures;

// CUSTOM REFERENCES
use Uvi\ReferencesModels\Vessels\RefZonasPescaAutorizadas;
use Uvi\ReferencesModels\Vessels\RefColombiaPuertosDesembarcoAutorizados;
use Uvi\ReferencesModels\Vessels\RefPesqueriaAutorizadas;
use Uvi\ReferencesModels\Vessels\RefDenominacionArtePescaTipoRedes;
use Uvi\ReferencesModels\Vessels\RefMaterialesRedes;
use Uvi\ReferencesModels\Vessels\RefDenominacionArtePescaTipoSedalAnzuelos;
use Uvi\ReferencesModels\Vessels\RefTiposAnzuelos;
use Uvi\ReferencesModels\Vessels\RefMaterialesLineaMadre;
use Uvi\ReferencesModels\Vessels\RefMaterialesBajantes;
use Uvi\ReferencesModels\Vessels\RefDenominacionArtePescaTipoTrampasNasas;
use Uvi\ReferencesModels\Vessels\RefMaterialesPrincipalesTrampasNasas;
use Uvi\ReferencesModels\Vessels\RefTiposArtefactosHerirAferrar;
use Uvi\ReferencesModels\Vessels\RefMaterialesArtefactos;
use Uvi\ReferencesModels\Vessels\RefTiposFads;
use Uvi\ReferencesModels\Vessels\RefMaterialesTipoMarco;

// COMMON
use Uvi\Http\Controllers\GlobalController;
use \DB;
use \File;
use Illuminate\Http\Request;
use Uvi\Http\Requests\CreateVesselRequest;
use Uvi\Http\Requests\UpdateVesselRequest;

class VesselsController extends Controller
{
    // REFERENCIAS
    private $refs;
    private $constants;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->refs = [
            'vms_types' => RefVmsTypes::where('status', 1)->orderBy('id')->pluck('name', 'id'),
            'orops' => RefOrops::where('status', 1)->orderBy('id')->pluck('name','id'),
            'countries' => RefCountries::where('status', 1)->orderBy('id')->pluck('name','id'),
            'vessel_types' => RefVesselTypes::where('status', 1)->orderBy('id')->pluck('name','id'),
            'registration_ports' => RefRegistrationPorts::where('status', 1)->orderBy('id')->pluck('name','id'),
            'power_units' => RefPowerUnits::where('status', 1)->orderBy('id')->pluck('name','id'),
            'hull_materials' => RefHullMaterials::where('status', 1)->orderBy('id')->pluck('name','id'),
            'fish_hold_types' => RefFishHoldTypes::where('status', 1)->orderBy('id')->pluck('name','id'),
            'zona_pesca_autorizadas' => RefZonasPescaAutorizadas::where('status', 1)->orderBy('id')->pluck('name','id'),
            'colombia_puertos_desembarco_autorizados' => RefColombiaPuertosDesembarcoAutorizados::where('status', 1)->orderBy('id')->pluck('name','id'),
            'pesqueria_autorizadas' => RefPesqueriaAutorizadas::where('status', 1)->orderBy('id')->pluck('name','id'),
            'denominacion_arte_pesca_tipo_redes' => RefDenominacionArtePescaTipoRedes::where('status', 1)->orderBy('id')->pluck('name','id'),
            'materiales_redes' => RefMaterialesRedes::where('status', 1)->orderBy('id')->pluck('name','id'),
            'denominacion_arte_pesca_tipo_sedal_anzuelos' => RefDenominacionArtePescaTipoSedalAnzuelos::where('status', 1)->orderBy('id')->pluck('name','id'),
            'tipos_anzuelos' => RefTiposAnzuelos::where('status', 1)->orderBy('id')->pluck('name','id'),
            'materiales_linea_madre' => RefMaterialesLineaMadre::where('status', 1)->orderBy('id')->pluck('name','id'),
            'materiales_bajantes' => RefMaterialesBajantes::where('status', 1)->orderBy('id')->pluck('name','id'),
            'denominacion_arte_pesca_tipo_trampas_nasas' => RefDenominacionArtePescaTipoTrampasNasas::where('status', 1)->orderBy('id')->pluck('name','id'),
            'materiales_principales_trampas_nasas' => RefMaterialesPrincipalesTrampasNasas::where('status', 1)->orderBy('id')->pluck('name','id'),
            'tipos_artefactos_herir_aferrar' => RefTiposArtefactosHerirAferrar::where('status', 1)->orderBy('id')->pluck('name','id'),
            'materiales_artefactos' => RefMaterialesArtefactos::where('status', 1)->orderBy('id')->pluck('name','id'),
            'tipos_fad' => RefTiposFads::where('status', 1)->orderBy('id')->pluck('name','id'),
            'materiales_tipo_marco' => RefMaterialesTipoMarco::where('status', 1)->orderBy('id')->pluck('name','id'),
        ];

        $this->constants = [
            'vessels_images_path' => 'img/vessels/',
            'asset_vessels_images_path' => 'img/vessels/',
        ];
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vessels = Vessel::with('ref_tipo_barco', 'ref_pais_pabellon', 'vr_pictures')->orderBy('id', 'asc')->get();

        return view('vessels.index')->with(['vessels' => $vessels, 'constants' => $this->constants]);
    }

    public function create()
    {
        $vessel = new Vessel;

        return view('vessels.create')->with(['vessel' => $vessel, 'refs' => $this->refs, 'constants' => $this->constants]);
    }

    public function details(Vessel $vessel)
    {
        return view('vessels.details')->with(['vessel' => $vessel, 'refs' => $this->refs, 'constants' => $this->constants]);
    }

    public function store(Vessel $vessel, CreateVesselRequest $request)
    {
        $result = false;
        $db_error = '';

        try{
            $vessel->fill($request->only([
                'numero_omi',
                'marcado_externo',
                'mlri',
                'mmsi',
                'indicador_vms',
                'tipo_vms',
                'detalles_vms',
                'sistema_ais',
                'detalles_ais',
                'status'
            ]));
            $vessel->user_id = auth()->user()->id;

            $vessel->save();
            $result = true;
        }
        catch(\Exception $e)
        {
            $result = false;
            $db_error = $e->getMessage();
        }

        session()->flash('alert_success', $result);
        session()->flash('alert_message', ($result ? 'Buque Registrado' : ('Ocurrió un error al registrar el buque: ' . $db_error)));

        $tab = $request->has('tab') ? ((int) $request['tab']) : 1;
        $tab++;
        return ($result) ? redirect()->route('edit_vessel_path', ['vessel' => $vessel->id, 'tab' => $tab]) : redirect()->route('create_vessel_path', ['tab' => $tab])->withInput($request->all());
    }

    public function edit(Vessel $vessel)
    {
        return view('vessels.edit')->with(['vessel' => $vessel, 'refs' => $this->refs, 'constants' => $this->constants]);
    }

    public function update(Vessel $vessel, UpdateVesselRequest $request)
    {

        $result = false;
        $db_error = '';

        if($request->has('form_tab'))
        {
            try{

                $tab = (int) $request['form_tab'];

                $make_update = true;

                switch($tab)
                {
                    case 1:
                    {
                        $vessel->fill($request->only([
                            'numero_omi',
                            'marcado_externo',
                            'mlri',
                            'mmsi',
                            'indicador_vms',
                            'tipo_vms',
                            'detalles_vms',
                            'sistema_ais',
                            'detalles_ais',
                            'status'
                        ]));
                        break;
                    }

                    case 2:
                    {
                        $vessel->orop_id = GlobalController::getNumeric($request['orop_id']);
                        $vessel->identificador_regional_cuerpo = $request['identificador_regional_cuerpo'];
                        break;
                    }

                    case 3:
                    {
                        $vessel->estado_actual_pabellon = GlobalController::getNumeric($request['estado_actual_pabellon']);
                        $vessel->estado_actual_pabellon_fecha_registro = GlobalController::getDate($request['estado_actual_pabellon_fecha_registro']);
                        $vessel->numero_registro_nacional = $request['numero_registro_nacional'];
                        $vessel->nombre_barco = $request['nombre_barco'];
                        $vessel->nombre_barco_ingles = $request['nombre_barco_ingles'];
                        $vessel->puerto_registro = GlobalController::getNumeric($request['puerto_registro']);
                        $vessel->tipo_barco = GlobalController::getNumeric($request['tipo_barco']);
                        $vessel->funcionamiento = GlobalController::getNumeric($request['funcionamiento']);
                        break;
                    }

                    case 4:
                    {
                        $vessel->longitud_total = GlobalController::getNumeric($request['longitud_total']);
                        $vessel->eslora_perpendiculares = GlobalController::getNumeric($request['eslora_perpendiculares']);
                        $vessel->longitud_registrada = GlobalController::getNumeric($request['longitud_registrada']);
                        $vessel->manga = GlobalController::getNumeric($request['manga']);
                        $vessel->puntal = GlobalController::getNumeric($request['puntal']);
                        $vessel->calado = GlobalController::getNumeric($request['calado']);
                        $vessel->peso_muerto = GlobalController::getNumeric($request['peso_muerto']);
                        $vessel->tonelaje_neto = GlobalController::getNumeric($request['tonelaje_neto']);
                        $vessel->tonelaje_registro_neto = GlobalController::getNumeric($request['tonelaje_registro_neto']);
                        $vessel->arqueo_bruto = GlobalController::getNumeric($request['arqueo_bruto']);
                        $vessel->tonelaje_registro_bruto = GlobalController::getNumeric($request['tonelaje_registro_bruto']);
                        $vessel->potencia_motor_principal = GlobalController::getNumeric($request['potencia_motor_principal']);
                        $vessel->unidad_potencia = GlobalController::getNumeric($request['unidad_potencia']);
                        $vessel->material_casco = GlobalController::getNumeric($request['material_casco']);
                        $vessel->capacidad_bodega_pescado = GlobalController::getNumeric($request['capacidad_bodega_pescado']);
                        $vessel->tipo_bodega_pescado = GlobalController::getNumeric($request['tipo_bodega_pescado']);
                        break;
                    }

                    case 5:
                    {
                        $vessel->anio_construccion = GlobalController::getNumeric($request['anio_construccion']);
                        $vessel->pais_construccion = GlobalController::getNumeric($request['pais_construccion']);
                        break;
                    }

                    case 6:
                    {
                        $vessel->nombre_propietario = $request['nombre_propietario'];
                        $vessel->numero_omi_propietario = $request['numero_omi_propietario'];
                        $vessel->direccion_propietario = $request['direccion_propietario'];
                        $vessel->ciudad_propietario = $request['ciudad_propietario'];
                        $vessel->codigo_postal_propietario = GlobalController::getNumeric($request['codigo_postal_propietario']);
                        $vessel->nacionalidad_propietario = GlobalController::getNumeric($request['nacionalidad_propietario']);
                        $vessel->nombre_operador = $request['nombre_operador'];
                        $vessel->numero_omi_operador = $request['numero_omi_operador'];
                        $vessel->direccion_operador = $request['direccion_operador'];
                        $vessel->ciudad_operador = $request['ciudad_operador'];
                        $vessel->codigo_postal_operador = GlobalController::getNumeric($request['codigo_postal_operador']);
                        $vessel->nacionalidad_operador = GlobalController::getNumeric($request['nacionalidad_operador']);
                        $vessel->nombre_administrador = $request['nombre_administrador'];
                        $vessel->numero_omi_administrador = $request['numero_omi_administrador'];
                        $vessel->direccion_administrador = $request['direccion_administrador'];
                        $vessel->ciudad_administrador = $request['ciudad_administrador'];
                        $vessel->codigo_postal_administrador = GlobalController::getNumeric($request['codigo_postal_administrador']);
                        $vessel->nacionalidad_administrador = GlobalController::getNumeric($request['nacionalidad_administrador']);
                        break;
                    }

                    case 7:
                    {
                        $vessel->permiso_vigente = GlobalController::getNumeric($request['permiso_vigente']);
                        $vessel->numero_permiso = $request['numero_permiso'];
                        $vessel->ultima_resolucion = $request['ultima_resolucion'];
                        $vessel->vigencia_permiso = GlobalController::getNumeric($request['vigencia_permiso']);
                        $vessel->fecha_inicio_permiso = GlobalController::getDate($request['fecha_inicio_permiso']);
                        $vessel->fecha_final_permiso = GlobalController::getDate($request['fecha_final_permiso']);
                        $vessel->fecha_inicio_patente = GlobalController::getDate($request['fecha_inicio_patente']);
                        $vessel->fecha_final_patente = GlobalController::getDate($request['fecha_final_patente']);
                        $vessel->zona_pesca_autorizada = GlobalController::getNumeric($request['zona_pesca_autorizada']);
                        $vessel->puerto_desembarco = GlobalController::getNumeric($request['puerto_desembarco']);
                        $vessel->pesqueria_autorizada = GlobalController::getNumeric($request['pesqueria_autorizada']);
                        break;
                    }

                    case 8:
                    {
                        $vessel->arte_pesca_red = GlobalController::getNumeric($request['arte_pesca_red']);
                        $vessel->ojo_malla = GlobalController::getNumeric($request['ojo_malla']);
                        $vessel->ojo_malla_copo = GlobalController::getNumeric($request['ojo_malla_copo']);
                        $vessel->longitud_red = GlobalController::getNumeric($request['longitud_red']);
                        $vessel->longitud_relinga_superior = GlobalController::getNumeric($request['longitud_relinga_superior']);
                        $vessel->ancho_red = GlobalController::getNumeric($request['ancho_red']);
                        $vessel->cantidad_piezas = GlobalController::getNumeric($request['cantidad_piezas']);
                        $vessel->material_red = GlobalController::getNumeric($request['material_red']);
                        $vessel->arte_pesca_anzuelo = GlobalController::getNumeric($request['arte_pesca_anzuelo']);
                        $vessel->tipo_anzuelo = GlobalController::getNumeric($request['tipo_anzuelo']);
                        $vessel->tamanio_anzuelo = GlobalController::getNumeric($request['tamanio_anzuelo']);
                        $vessel->cantidad_anzuelos = GlobalController::getNumeric($request['cantidad_anzuelos']);
                        $vessel->longitud_linea_madre = $request['longitud_linea_madre'];
                        $vessel->material_linea_madre = GlobalController::getNumeric($request['material_linea_madre']);
                        $vessel->material_bajantes = GlobalController::getNumeric($request['material_bajantes']);
                        $vessel->cantidad_total_lineas = GlobalController::getNumeric($request['cantidad_total_lineas']);
                        $vessel->arte_pesca_trampa = GlobalController::getNumeric($request['arte_pesca_trampa']);
                        $vessel->cantidad_trampas = GlobalController::getNumeric($request['cantidad_trampas']);
                        $vessel->materiales_trampas = GlobalController::getNumeric($request['materiales_trampas']);
                        $vessel->tipo_artefactos_herir = GlobalController::getNumeric($request['tipo_artefactos_herir']);
                        $vessel->cantidad_artefactos = GlobalController::getNumeric($request['cantidad_artefactos']);
                        $vessel->material_artefactos = GlobalController::getNumeric($request['material_artefactos']);
                        $vessel->arte_pesca_otros = $request['arte_pesca_otros'];
                        $vessel->fad = GlobalController::getNumeric($request['fad']);
                        $vessel->tipos_fad = GlobalController::getNumeric($request['tipos_fad']);
                        $vessel->cantidad_fad = GlobalController::getNumeric($request['cantidad_fad']);
                        $vessel->componentes_fad = $request['componentes_fad'];
                        break;
                    }

                    case 9:
                    {
                        $vessel->tipo_material_det = GlobalController::getNumeric($request['tipo_material_det']);
                        $vessel->distancia_barras_det = $request['distancia_barras_det'];
                        $vessel->altura_parrilla_det = $request['altura_parrilla_det'];
                        $vessel->ancho_parrilla_det = $request['ancho_parrilla_det'];
                        $vessel->angulo_inclinacion_derecho_det = $request['angulo_inclinacion_derecho_det'];
                        $vessel->angulo_inclinacion_izquierdo_det = $request['angulo_inclinacion_izquierdo_det'];
                        $vessel->amplitud_minima_escape_solapa = $request['amplitud_minima_escape_solapa'];
                        $vessel->amplitud_minima_escape_solapa_doble = $request['amplitud_minima_escape_solapa_doble'];
                        $vessel->longitud_relinga_inferior = $request['longitud_relinga_inferior'];
                        $vessel->traslape = $request['traslape'];
                        $vessel->redes_repuesto = GlobalController::getNumeric($request['redes_repuesto']);
                        $vessel->parrillas_repuesto = GlobalController::getNumeric($request['parrillas_repuesto']);
                        break;
                    }

                    case 10:
                    {
                        if($request->hasFile('photos'))
                        {
                            $files = $request->file('photos');

                            foreach($files as $image)
                            {
                                $vp = new VesselPictures;

                                $file_name = GlobalController::getRandomFileName($image->getClientOriginalExtension(), $this->constants['vessels_images_path']);
                                $image->move($this->constants['vessels_images_path'], $file_name);

                                $vp->vessel_id = $vessel->id;
                                $vp->picture = $file_name;

                                $vp->save();
                            }
                        }
                        $make_update = false;
                        break;
                    }
                }

                if($make_update)
                {
                    $vessel->update();
                }

                $result = true;
            }
            catch(\Exception $e)
            {
                $result = false;
                $db_error = $e->getMessage();
            }
        }

        session()->flash('alert_success', $result);
        session()->flash('alert_message', ($result ? 'Buque Actualizado' : ('Ocurrió un error al actualizar el buque' . $db_error)));

        $tab = ($result) ? ($tab < 10 ? ($tab + 1) : 1) : $tab;
        return ($result) ? redirect()->route('edit_vessel_path', ['vessel' => $vessel->id, 'tab' => $tab]) : redirect()->route('edit_vessel_path', ['vessel' => $vessel->id, 'tab' => $tab])->withInput($request->all());
    }

    // AJAX

    public function deleteImage($id)
    {
        $image = VesselPictures::find($id);

        $response = [];
        $success = false;

        if(!is_null($image))
        {
            $picture_path = $this->constants['vessels_images_path'] . $image->picture;

            if(File::exists($picture_path))
            {
                File::delete($picture_path);
            }

            try{ $success = $image->delete(); }
            catch(\Illuminate\Database\QueryException $e)
            { $success = false;}
        }

        $response['success'] = $success;

        return json_encode($response);
    }
}

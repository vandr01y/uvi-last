<?php

namespace Uvi\Http\Controllers;

use Uvi\BuqueArtesanal;

// REFERENCES
use Uvi\ReferencesModels\Vessels\RefCountries;
use Uvi\ReferencesModels\Vessels\RefRegistrationPorts;
use Uvi\ReferencesModels\BuquesArtesanales\RefBuqueArtesanalTipoEmbarcacion;
use Uvi\ReferencesModels\BuquesArtesanales\RefBuqueArtesanalCuencas;
use Uvi\ReferencesModels\BuquesArtesanales\RefColombiaDepartamentos;
use Uvi\ReferencesModels\BuquesArtesanales\RefColombiaMunicipios;
use Uvi\ReferencesModels\Vessels\RefHullMaterials;
use Uvi\ReferencesModels\BuquesArtesanales\RefBuquesArtesanalesTiposPropulsion;
use Uvi\ReferencesModels\BuquesArtesanales\RefBuquesArtesanalesTiposMotor;
use Uvi\ReferencesModels\BuquesArtesanales\RefChalecosSalvavidasEstados;
use Uvi\ReferencesModels\BuquesArtesanales\RefBuqueArtesanalArtePescaTipoRed;
use Uvi\ReferencesModels\BuquesArtesanales\RefBuquesArtesanalesLineasMano;
use Uvi\ReferencesModels\Vessels\RefMaterialesRedes;
use Uvi\ReferencesModels\Vessels\RefDenominacionArtePescaTipoTrampasNasas;
use Uvi\ReferencesModels\Vessels\RefMaterialesTrampasNasas;
use Uvi\ReferencesModels\Vessels\RefTiposArtefactosHerirAferrar;
use Uvi\ReferencesModels\Vessels\RefMaterialesArtefactos;
use Uvi\ReferencesModels\Vessels\RefTiposFads;

// Buques Relationships
use Uvi\RelationshipsModels\BuquesArtesanales\BuquesArtesanalesPictures;
use Uvi\RelationshipsModels\BuquesArtesanales\BuquesArtesanalesArtesPictures;

// COMMON
use Uvi\Http\Controllers\GlobalController;
use \DB;
use \File;
use Illuminate\Http\Request;
//use Request;
use Uvi\Http\Requests\CreateBuqueArtesanalRequest;
use Uvi\Http\Requests\UpdateBuqueArtesanalRequest;

class BuquesArtesanalesController extends Controller
{
    // REFERENCIAS
    private $refs;
    private $constants;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->refs = [
            'X' => RefCountries::where('status', 1)->orderBy('id')->pluck('name','id'),
            'countries' => RefCountries::where('status', 1)->orderBy('id')->pluck('name','id'),
            'registration_ports' => RefRegistrationPorts::where('status', 1)->orderBy('id')->pluck('name','id'),
            'tipo_embarcacion' => RefBuqueArtesanalTipoEmbarcacion::where('status', 1)->orderBy('id')->pluck('name','id'),
            'cuencas' => RefBuqueArtesanalCuencas::where('status', 1)->orderBy('id')->pluck('name','id'),
            'colombia_departamentos' => RefColombiaDepartamentos::where('status', 1)->orderBy('id')->pluck('name','id'),
            'colombia_municipios' => RefColombiaMunicipios::where('status', 1)->orderBy('id')->pluck('name','id'),
            'hull_materials' => RefHullMaterials::where('status', 1)->orderBy('id')->pluck('name','id'),
            'tipos_propulsion' => RefBuquesArtesanalesTiposPropulsion::where('status', 1)->orderBy('id')->pluck('name','id'),
            'tipos_motor' => RefBuquesArtesanalesTiposMotor::where('status', 1)->orderBy('id')->pluck('name','id'),
            'chalecos_salvavidas_estados' => RefChalecosSalvavidasEstados::where('status', 1)->orderBy('id')->pluck('name','id'),
            'arte_pesca_tipo_red' => RefBuqueArtesanalArtePescaTipoRed::where('status', 1)->orderBy('id')->pluck('name','id'),
            'materiales_redes' => RefMaterialesRedes::where('status', 1)->orderBy('id')->pluck('name','id'),
            'lineas_mano' => RefBuquesArtesanalesLineasMano::where('status', 1)->orderBy('id')->pluck('name','id'),
            'trampas_nasas' => RefDenominacionArtePescaTipoTrampasNasas::where('status', 1)->orderBy('id')->pluck('name','id'),
            'materiales_trampas_nasas' => RefMaterialesTrampasNasas::where('status', 1)->orderBy('id')->pluck('name','id'),
            'tipos_artefactos' => RefTiposArtefactosHerirAferrar::where('status', 1)->orderBy('id')->pluck('name','id'),
            'materiales_artefactos' => RefMaterialesArtefactos::where('status', 1)->orderBy('id')->pluck('name','id'),
            'tipos_fad' => RefTiposFads::where('status', 1)->orderBy('id')->pluck('name','id'),
        ];

        $this->constants = [
            'artes_photos_upload_path' => 'img/buques_artesanales/artes_pesca/',
            'embarcaciones_photos_upload_path' => 'img/buques_artesanales/embarcaciones/',
            'asset_artes_photos_path' => 'img/buques_artesanales/artes_pesca/',
            'asset_embarcaciones_photos_path' => 'img/buques_artesanales/embarcaciones/',
        ];
    }

    public function index()
    {
        $buques = BuqueArtesanal::orderBy('id', 'asc')->get();

        return view('buques_artesanales.index')->with(['buques' => $buques, 'constants' => $this->constants]);
    }

    public function create()
    {
        $buque = new BuqueArtesanal;

        return view('buques_artesanales.create')->with(['buque' => $buque, 'refs' => $this->refs, 'constants' => $this->constants]);
    }

    public function store(BuqueArtesanal $buque, CreateBuqueArtesanalRequest $request)
    {
        $result = false;
        $db_error = '';

        try{
            $buque->fill($request->only([
                'nombre',
                'matricula',
                'numero_identificacion_embarcacion',
                'status',
            ]));

            $buque->user_id = auth()->user()->id;

            $buque->save();
            $result = true;
        }
        catch(\Exception $e)
        {
            $result = false;
            $db_error = $e->getMessage();
        }

        session()->flash('alert_success', $result);
        session()->flash('alert_message', ($result ? 'Buque Registrado' : ('Ocurrió un error al registrar el buque: ' . $db_error)));

        $tab = $request->has('tab') ? ((int) $request['tab']) : 1;
        if($tab <  9)
        {
            $tab++;
        }
        return ($result) ? redirect()->route('edit_buque_artesanal_path', ['buque' => $buque->id, 'tab' => $tab]) : redirect()->route('create_buque_artesanal_path', ['tab' => $tab])->withInput($request->all());
    }

    public function edit(BuqueArtesanal $buque)
    {
        return view('buques_artesanales.edit')->with(['buque' => $buque, 'refs' => $this->refs, 'constants' => $this->constants]);
    }

    public function update(BuqueArtesanal $buque, UpdateBuqueArtesanalRequest $request)
    {
        $db_error = '';

        try
        {
            $result = false;
            $update = true;
            $tab = (int) $request['form_tab'];

            switch((int)$tab)
            {
                case 1:
                {
                    $buque->fill($request->only([
                        'nombre',
                        'matricula',
                        'numero_identificacion_embarcacion',
                        'status',
                    ]));

                    break;
                }
                case 2:
                {
                    $buque->bandera_id = GlobalController::getIntegerReference($request['bandera_id']);
                    $buque->puerto_id = GlobalController::getIntegerReference($request['puerto_id']);
                    $buque->tipo_id = GlobalController::getIntegerReference($request['tipo_id']);
                    $buque->estado_operacional = GlobalController::getNumeric($request['estado_operacional']);
                    $buque->cuenca_id = GlobalController::getIntegerReference($request['cuenca_id']);
                    $buque->departamento_id = GlobalController::getIntegerReference($request['departamento_id']);
                    $buque->municipio_id = GlobalController::getIntegerReference($request['municipio_id']);
                    $buque->localidad = $request['localidad'];
                    $buque->lugar_desembarco = $request['lugar_desembarco'];
                    $buque->permiso_pesca = GlobalController::getNumeric($request['permiso_pesca']);
                    $buque->numero_resolucion = GlobalController::getNumeric($request['numero_resolucion']);
                    $buque->vigencia_permiso = GlobalController::getNumeric($request['vigencia_permiso']);
                    $buque->fecha_inicio = GlobalController::getDate($request['fecha_inicio']);
                    $buque->fecha_finalizacion = GlobalController::getDate($request['fecha_finalizacion']);
                    break;
                }
                case 3:
                {
                    $buque->eslora = GlobalController::getNumeric($request['eslora']);
                    $buque->manga = GlobalController::getNumeric($request['manga']);
                    $buque->puntal = GlobalController::getNumeric($request['puntal']);
                    $buque->calado = GlobalController::getNumeric($request['calado']);
                    $buque->trn = GlobalController::getNumeric($request['trn']);
                    $buque->trb = GlobalController::getNumeric($request['trb']);
                    break;
                }
                case 4:
                {
                    $buque->anio_construccion = GlobalController::getNumeric($request['anio_construccion']);
                    $buque->lugar_construccion = $request['lugar_construccion'];
                    $buque->astillero_construccion = $request['astillero_construccion'];
                    $buque->carpintero_construccion = $request['carpintero_construccion'];
                    $buque->material_embarcacion_id = GlobalController::getIntegerReference($request['material_embarcacion_id']);
                    break;
                }
                case 5:
                {
                    $buque->tipo_propulsion_id = GlobalController::getIntegerReference($request['tipo_propulsion_id']);
                    $buque->tipo_motor_id = GlobalController::getIntegerReference($request['tipo_motor_id']);
                    $buque->marca_motor = $request['marca_motor'];
                    $buque->potencia_motor = $request['potencia_motor'];
                    $buque->numero_motor = GlobalController::getNumeric($request['numero_motor']);
                    $buque->tipo_combustible = $request['tipo_combustible'];
                    break;
                }
                case 6:
                {
                    // CHECK TEXTAREA
                    $buque->nombre_propietario = $request['nombre_propietario'];
                    $buque->numero_identificacion = GlobalController::getNumeric($request['numero_identificacion']);
                    $buque->direccion_residencia = $request['direccion_residencia'];
                    $buque->ciudad_municipio = $request['ciudad_municipio'];
                    $buque->telefono_fijo = GlobalController::getNumeric($request['telefono_fijo']);
                    $buque->telefono_celular = GlobalController::getNumeric($request['telefono_celular']);
                    $buque->carne_pesca = GlobalController::getNumeric($request['carne_pesca']);
                    $buque->numero_carne_pesca = GlobalController::getNumeric($request['numero_carna_pesca']);
                    $buque->fecha_finalizacion_carne_pesca = GlobalController::getDate($request['fecha_finalizacion_carne_pesca']);
                    $buque->afiliacion = GlobalController::getNumeric($request['afiliacion']);
                    $buque->nombre_organizacion = $request['nombre_organizacion'];
                    $buque->numero_tripulantes = GlobalController::getNumeric($request['numero_tripulantes']);
                    $buque->nombre_tripulacion = $request['nombre_tripulacion'];
                    $buque->numero_documentos_tripulacion = GlobalController::getNumeric($request['numero_documentos_tripulacion']);
                    $buque->direccion_residencia_tripulacion = $request['direccion_residencia_tripulacion'];
                    $buque->telefono_contacto_tripulacion = $request['telefono_contacto_tripulacion'];
                    $buque->numero_carne_pesca_tripulacion = $request['numero_carne_pesca_tripulacion'];
                    $buque->fecha_finalizacion_carne_tripulacion = GlobalController::getDate($request['fecha_finalizacion_carne_tripulacion']);
                    break;
                }
                case 7:
                {
                    $buque->radio_comunicaciones = GlobalController::getNumeric($request['radio_comunicaciones']);
                    $buque->marca_radio_comunicaciones = $request['marca_radio_comunicaciones'];
                    $buque->frecuencia_asignada = GlobalController::getNumeric($request['frecuencia_asignada']);
                    $buque->brujula = $request['brujula'];
                    $buque->marca_brujula = $request['marca_brujula'];
                    $buque->gps = GlobalController::getNumeric($request['gps']);
                    $buque->marca_gps = $request['marca_gps'];
                    $buque->chalecos_salvavidas = GlobalController::getNumeric($request['chalecos_salvavidas']);
                    $buque->numero_chalecos_salvavidas = GlobalController::getNumeric($request['numero_chalecos_salvavidas']);
                    $buque->estado_condicion_chalecos_salvavidas_id = GlobalController::getIntegerReference($request['estado_condicion_chalecos_salvavidas_id']);
                    break;
                }
                case 8:
                {
                    $buque->tipo_red_id = GlobalController::getIntegerReference($request['tipo_red_id']);
                    $buque->ojo_malla = GlobalController::getNumeric($request['ojo_malla']);
                    $buque->ojo_malla_copo = GlobalController::getNumeric($request['ojo_malla_copo']);
                    $buque->longitud_red = GlobalController::getNumeric($request['longitud_red']);
                    $buque->ancho_red = GlobalController::getNumeric($request['ancho_red']);
                    $buque->cantidad_panos_piezas = GlobalController::getNumeric($request['cantidad_panos_piezas']);
                    $buque->material_red_id = GlobalController::getIntegerReference($request['material_red_id']);
                    $buque->zonas_pesca_frecuentadas = $request['zonas_pesca_frecuentadas'];
                    $buque->tipos_lineas_mano_id = GlobalController::getIntegerReference($request['tipos_lineas_mano_id']);
                    $buque->trampas_id = GlobalController::getIntegerReference($request['trampas_id']);
                    $buque->cantidad_trampas = GlobalController::getNumeric($request['cantidad_trampas']);
                    $buque->material_principal_trampas_id = GlobalController::getIntegerReference($request['material_principal_trampas_id']);
                    $buque->zonas_pesca_trampas = $request['zonas_pesca_trampas'];
                    $buque->tipos_artefactos_herir_aferrar_id = GlobalController::getIntegerReference($request['tipos_artefactos_herir_aferrar_id']);
                    $buque->cantidad_artefactos = GlobalController::getNumeric($request['cantidad_artefactos']);
                    $buque->material_artefacto_id = GlobalController::getIntegerReference($request['material_artefacto_id']);
                    $buque->zonas_pesca_artefactos = $request['zonas_pesca_artefactos'];
                    $buque->otros = $request['otros'];
                    $buque->artes_metodos_poco_convencionales = $request['artes_metodos_poco_convencionales'];
                    $buque->cantidad_artes_metodos_poco_convencionales = GlobalController::getNumeric($request['cantidad_artes_metodos_poco_convencionales']);
                    $buque->uso_fad = GlobalController::getNumeric($request['uso_fad']);
                    $buque->tipos_fad_id = GlobalController::getIntegerReference($request['tipos_fad_id']);
                    $buque->cantidad_fad = GlobalController::getNumeric($request['cantidad_fad']);

                    if($request->hasFile('photos_f8'))
                    {
                        $files = $request->file('photos_f8');

                        foreach($files as $image)
                        {
                            $ba_ap = new BuquesArtesanalesArtesPictures;

                            $file_name = GlobalController::getRandomFileName($image->getClientOriginalExtension(), $this->constants['artes_photos_upload_path']);
                            $image->move($this->constants['artes_photos_upload_path'], $file_name);

                            $ba_ap->buque_artesanal_id = $buque->id;
                            $ba_ap->file = $file_name;

                            $ba_ap->save();
                        }
                    }

                    break;
                }
                case 9:
                {
                    if($request->hasFile('photos_f9'))
                    {
                        $files = $request->file('photos_f9');

                        foreach($files as $image)
                        {
                            $ba_e = new BuquesArtesanalesPictures;

                            $file_name = GlobalController::getRandomFileName($image->getClientOriginalExtension(), $this->constants['embarcaciones_photos_upload_path']);
                            $image->move($this->constants['embarcaciones_photos_upload_path'], $file_name);

                            $ba_e->buque_artesanal_id = $buque->id;
                            $ba_e->file = $file_name;

                            $ba_e->save();
                        }
                    }
                    $update = false;
                    break;
                }
            }

            if($update)
            {
                $buque->update();
            }

            $result = true;
        }
        catch(\Exception $e)
        {
            $result = false;
            $db_error = $e->getMessage();
            dd($e);
        }

        session()->flash('alert_success', $result);
        session()->flash('alert_message', ($result ? 'Buque Actualizado' : ('Ocurrió un error al actualizar el buque: ' . $db_error)));

        $tab = ($result) ? ($tab < 9 ? ($tab + 1) : 1) : $tab;
        return ($result) ? redirect()->route('edit_buque_artesanal_path', ['buque' => $buque->id, 'tab' => $tab]) : redirect()->route('edit_buque_artesanal_path', ['buque' => $buque->id, 'tab' => $tab])->withInput($request->all());
    }

    // AJAX

    public function deleteImageArte(BuquesArtesanalesArtesPictures $imagen, Request $request)
    {
        $response = [];
        $success = false;

        if($request->ajax() && !is_null($imagen))
        {
            $picture_path = $this->constants['artes_photos_upload_path'] . $imagen->file;

            if(File::exists($picture_path))
            {
                File::delete($picture_path);
            }

            try{ $success = $imagen->delete(); }
            catch(\Illuminate\Database\QueryException $e)
            { $success = false;}
        }

        $response['success'] = $success;

        return json_encode($response);
    }



    public function deleteImage(BuquesArtesanalesPictures $imagen, Request $request)
    {
        $response = [];
        $success = false;

        if($request->ajax() && !is_null($imagen))
        {
            $picture_path = $this->constants['embarcaciones_photos_upload_path'] . $imagen->file;

            if(File::exists($picture_path))
            {
                File::delete($picture_path);
            }

            try{ $success = $imagen->delete(); }
            catch(\Illuminate\Database\QueryException $e)
            { $success = false;}
        }

        $response['success'] = $success;
        return json_encode($response);
    }
}

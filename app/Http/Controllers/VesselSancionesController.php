<?php

namespace Uvi\Http\Controllers;

use Uvi\VesselSancion;

// REFERENCIAS
use Uvi\Vessel;
use Uvi\ReferencesModels\Sanciones\RefFallosEjecutoriados;
use Uvi\ReferencesModels\Sanciones\RefSancionesEstados;
use Uvi\ReferencesModels\Sanciones\RefSancionesTipos;
use Uvi\ReferencesModels\Sanciones\RefSancionesTiposInfracciones;

// COMMON
use Uvi\Http\Controllers\GlobalController;
use \DB;
use \File;
use Illuminate\Http\Request;
use Uvi\Http\Requests\CreateVesselSancionRequest;
use Uvi\Http\Requests\UpdateVesselSancionRequest;

// RELACIONES
use Uvi\RelationshipsModels\VesselSanciones\VesselSancionesAttachments;

class VesselSancionesController extends Controller
{
    // REFERENCIAS
    private $refs;
    private $constants;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->refs = [
            'vessels' => Vessel::select(DB::raw("CONCAT(numero_omi, ' - ', (CASE WHEN nombre_barco != '' THEN nombre_barco ELSE 'SIN NOMBRE' END)) AS name"), 'id')->where('status', 1)->orderBy('status', 1)->pluck('name', 'id'),
            'sanciones_tipos_infracciones' => RefSancionesTiposInfracciones::where('status', 1)->orderBy('id')->pluck('name', 'id'),
            'sanciones_tipos' => RefSancionesTipos::where('status', 1)->orderBy('id')->pluck('name', 'id'),
            'sanciones_fallos_ejecutoriados' => RefFallosEjecutoriados::where('status', 1)->orderBy('id')->pluck('name', 'id'),
            'sanciones_estados' => RefSancionesEstados::where('status', 1)->orderBy('id')->pluck('name', 'id'),
        ];

        $this->constants = [
            'upload_path' => storage_path('app/vessels/sanciones/attachments/'),
        ];
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sanciones = VesselSancion::with('rel_vessel', 'ref_infraccion_proceso', 'ref_sanciones_estados', 'rel_files')->paginate(10);

        return view('sanciones.index')->with(['sanciones' => $sanciones]);
    }

    public function create()
    {
        $sancion = new VesselSancion; // TABLA

        return view('sanciones.create')->with(['sancion' => $sancion, 'refs' => $this->refs]);
    }

    public function store(VesselSancion $sancion, CreateVesselSancionRequest $request)
    {
        $result = false;
        $db_error = '';

        try{
            $sancion->vessel_id = GlobalController::getIntegerReference($request['vessel_id']);
            $sancion->presunta_violacion = GlobalController::getNumeric($request['presunta_violacion']);
            $sancion->nur = GlobalController::getNumeric($request['nur']);
            $sancion->infraccion_proceso_id = GlobalController::getIntegerReference($request['infraccion_proceso_id']);
            $sancion->sanciones_estatuto_pesquero = GlobalController::getNumeric($request['sanciones_estatuto_pesquero']);
            $sancion->numero_acto_administrativo = $request['numero_acto_administrativo'];
            $sancion->fecha_sancion = GlobalController::getDate($request['fecha_sancion']);
            $sancion->infraccion_sancionada_id = GlobalController::getIntegerReference($request['infraccion_sancionada_id']);
            $sancion->tipo_sancion_id = GlobalController::getIntegerReference($request['tipo_sancion_id']);
            $sancion->fallo_ejecutoriado_id = GlobalController::getIntegerReference($request['fallo_ejecutoriado_id']);
            $sancion->valor_multa = GlobalController::getNumeric($request['valor_multa']);
            $sancion->estado_sancion_id = GlobalController::getIntegerReference($request['estado_sancion_id']);
            $sancion->detalles = $request['detalles'];
            $sancion->status = GlobalController::getNumeric($request['status']);
            $sancion->user_id = auth()->user()->id;

            $sancion->save();

            if($request->hasFile('attachments'))
            {
                $files = $request->file('attachments');

                if($request->hasFile('attachments'))
                {
                    $files = $request->file('attachments');

                    foreach($files as $file)
                    {
                        $vsa = new VesselSancionesAttachments;

                        $file_name = GlobalController::getRandomFileName($file->getClientOriginalExtension(), $this->constants['upload_path']);
                        $file->move($this->constants['upload_path'], $file_name);

                        $vsa->vessel_sancion_id = $sancion->id;
                        $vsa->name = GlobalController::cleanString(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME));
                        $vsa->file = $file_name;

                        $vsa->save();
                    }
                }
            }

            $result = true;
        }
        catch(\Exception $e)
        {
            $result = false;
            $db_error = $e->getMessage();
            //dd($e);
        }

        session()->flash('alert_success', $result);
        session()->flash('alert_message', ($result ? 'Sanción Registrada' : ('Ocurrió un error al registrar la sanción: ' . $db_error)));

        return ($result) ? redirect()->route('vessel_sanciones_list') : redirect()->route('create_vessel_sancion_path')->withInput($request->all());
    }

    public function edit(VesselSancion $sancion)
    {
        return view('sanciones.edit')->with(['sancion' => $sancion, 'refs' => $this->refs]);
    }

    public function update(VesselSancion $sancion, UpdateVesselSancionRequest $request)
    {
        $result = false;
        $db_error = '';
        try{
            $sancion->vessel_id = GlobalController::getIntegerReference($request['vessel_id']);
            $sancion->presunta_violacion = GlobalController::getNumeric($request['presunta_violacion']);
            $sancion->nur = GlobalController::getNumeric($request['nur']);
            $sancion->infraccion_proceso_id = GlobalController::getIntegerReference($request['infraccion_proceso_id']);
            $sancion->sanciones_estatuto_pesquero = GlobalController::getNumeric($request['sanciones_estatuto_pesquero']);
            $sancion->numero_acto_administrativo = $request['numero_acto_administrativo'];
            $sancion->fecha_sancion = GlobalController::getDate($request['fecha_sancion']);
            $sancion->infraccion_sancionada_id = GlobalController::getIntegerReference($request['infraccion_sancionada_id']);
            $sancion->tipo_sancion_id = GlobalController::getIntegerReference($request['tipo_sancion_id']);
            $sancion->fallo_ejecutoriado_id = GlobalController::getIntegerReference($request['fallo_ejecutoriado_id']);
            $sancion->valor_multa = GlobalController::getNumeric($request['valor_multa']);
            $sancion->estado_sancion_id = GlobalController::getIntegerReference($request['estado_sancion_id']);
            $sancion->detalles = $request['detalles'];
            $sancion->status = GlobalController::getNumeric($request['status']);
            $sancion->user_id = auth()->user()->id;

            $sancion->save();

            if($request->hasFile('attachments'))
            {
                $files = $request->file('attachments');

                foreach($files as $file)
                {
                    $vsa = new VesselSancionesAttachments;

                    $file_name = GlobalController::getRandomFileName($file->getClientOriginalExtension(), $this->constants['upload_path']);
                    $file->move($this->constants['upload_path'], $file_name);

                    $vsa->vessel_sancion_id = $sancion->id;
                    $vsa->name = GlobalController::cleanString(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME));
                    $vsa->file = $file_name;

                    $vsa->save();
                }
            }

            $sancion->update();

            $result = true;
        }
        catch(\Exception $e)
        {
            $result = false;
            $db_error = $e->getMessage();
        }

        session()->flash('alert_success', $result);
        session()->flash('alert_message', ($result ? 'Sanción Actualizada' : ('Ocurrió un error al actualizar la sanción: ' . $db_error)));

        return ($result) ? redirect()->route('vessel_sanciones_list') : redirect()->route('edit_vessel_sancion_path', ['sancion' => $sancion->id])->withInput($request->all());
    }

    // DOWNLOAD

    public function downloadAttachment(VesselSancionesAttachments $file)
    {
        $download = false;

        $vessel_sancion = VesselSancion::find($file->vessel_sancion_id);

        if(!is_null($file) && !is_null($vessel_sancion))
        {
            $file_path = $this->constants['upload_path'] . $file->file;

            if(File::exists($file_path))
            {
                $headers = array('Content-Type: application/pdf');
                $download = true;
            }
        }

        return ($download) ? response()->download($file_path, ($file->name . ".pdf"), $headers) : redirect()->route('vessel_sanciones_list');
    }

    public function downloadAttachmentsZip(VesselSancion $sancion)
    {
        $files = array();

        if(!is_null($sancion) && count($sancion->rel_files) > 0)
        {
            foreach($sancion->rel_files AS $file)
            {
                $file_path = $this->constants['upload_path'] . $file->file;
                if(File::exists($file_path))
                {
                    $files[] = array('path' => $file_path, 'name' => ($file->id . "_" . $file->name . ".pdf"));
                }
            }

            $zip_name = "uvi_sancion_industrial_" . $sancion->id . '_attachments';
        }

        return count($files) > 0 ? GlobalController::createAndDownloadZip($zip_name ,$files) : redirect()->route('vessel_sanciones_list');
    }

    // AJAX

    public function deleteAttachment(VesselSancionesAttachments $file)
    {
        $response = [];
        $success = false;

        if(!is_null($file))
        {
            $file_path = $this->constants['upload_path'] . $file->file;

            if(File::exists($file_path))
            {
                File::delete($file_path);
            }

            try{ $success = $file->delete(); }
            catch(\Illuminate\Database\QueryException $e)
            { $success = false; }
        }

        $response['success'] = $success;

        return json_encode($response);
    }
}

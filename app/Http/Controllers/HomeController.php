<?php

namespace Uvi\Http\Controllers;

use Uvi\User;
use Uvi\Vessel;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('id', '<>', 1)->get();
        $vessels = Vessel::orderBy('id', 'asc');

        return view('home')->with(['users' => $users, 'vessels' => $vessels]);
    }
}

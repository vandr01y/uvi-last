<?php

namespace Uvi\Http\Controllers;

use DateTime;
use ZipArchive;
use \DB;
use \File;
use Illuminate\Http\Request;

class GlobalController extends Controller
{
    public function __construct()
    {

    }

    static function parseInt($val, $default = 0)
    {
        return (trim($val) != '' && preg_match("/^-?[1-9][0-9]*$/D", $val)) ? ((int)$val) : $default;
    }

    static function getNumeric($val, $default = NULL)
    {
        return is_numeric($val) ? ($val + 0) : $default;
    }

    static function getDate($val, $format='Y-m-d', $default = NULL)
    {
        if(!empty($val))
        {
            $obj_date = new DateTime();
            $d = $obj_date->createFromFormat($format, $val);
            return ($d && $d->format($format) === $val) ? $val : $default;
        }
        else
        {
            return $default;
        }
    }

    static function getRandomFileName($extension = '', $path = '')
    {
        $path = trim($path);
        $path = strlen($path) > 0 ? (substr($path, -1) == '/' ? $path : ($path . '/')) : null;
        $seed = strtoupper(md5(uniqid(rand(),true)));
        $file_name = substr($seed,0,8) . '-' . substr($seed,8,4) . '-' . substr($seed,12,4). '-' . substr($seed,16,4). '-' . substr($seed,20);

        if(strlen(trim($extension)) > 0)
        {
            $file_name .= ('.' . $extension);
        }

        return !is_null($path) ? (File::exists($path . $file_name) ? self::getRandomFileName($extension, $path) : $file_name) : $file_name;
    }

    static function getIntegerReference($val, $default = NULL)
    {
        $val_holder = is_null($val) ? $val : self::parseInt($val, null);
        return (!is_null($val_holder)) ? $val_holder : $default;
    }

    static function cleanString($str)
    {
        $string = str_replace(' ', '_', $str); // Replaces all spaces with hyphens.
        $string = str_replace('-', '_', $string);
        $string = preg_replace('/[^A-Za-z0-9\_]/', '', $string); // Removes special chars.

        return preg_replace('/\_+/', '_', $string); // Replaces multiple hyphens with single one.
    }

    static function createAndDownloadZip($name = 'files', $files)
    {
        $public_dir = 'tmp/';

        $zipFileName = $name . '.zip';

        $zip = new ZipArchive;

        if ($zip->open($public_dir . '/' . $zipFileName, ZipArchive::CREATE) === TRUE)
        {
            foreach($files AS $file)
            {
                $zip->addFile($file['path'], $file['name']);
            }

            $zip->close();
        }

        $headers = array('Content-Type' => 'application/octet-stream',);
        $filetopath=$public_dir.'/'.$zipFileName;

        if(file_exists($filetopath))
        {
            return response()->download($filetopath,$zipFileName,$headers)->deleteFileAfterSend(true);
        }

        return ['status'=>'file does not exist'];
    }
}

?>

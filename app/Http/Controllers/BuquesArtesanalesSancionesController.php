<?php

namespace Uvi\Http\Controllers;

use Uvi\BuqueArtesanalSancion;

// REFERENCIAS
use Uvi\BuqueArtesanal;
use Uvi\ReferencesModels\Sanciones\RefFallosEjecutoriados;
use Uvi\ReferencesModels\Sanciones\RefSancionesEstados;
use Uvi\ReferencesModels\Sanciones\RefSancionesTipos;
use Uvi\ReferencesModels\Sanciones\RefSancionesTiposInfracciones;

// COMMON
use Uvi\Http\Controllers\GlobalController;
use \DB;
use \File;
use Illuminate\Http\Request;
use Uvi\Http\Requests\CreateBuqueArtesanalSancionRequest;
use Uvi\Http\Requests\UpdateBuqueArtesanalSancionRequest;

// RELACIONES
use Uvi\RelationshipsModels\BuqueArtesanalSanciones\BuqueArtesanalSancionesAttachments;

class BuquesArtesanalesSancionesController extends Controller
{
    // REFERENCIAS
    private $refs;
    private $constants;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->refs = [
            'buques_artesanales' => BuqueArtesanal::where('status', 1)->orderBy('status', 1)->pluck('nombre', 'id'),
            'sanciones_tipos_infracciones' => RefSancionesTiposInfracciones::where('status', 1)->orderBy('id')->pluck('name', 'id'),
            'sanciones_tipos' => RefSancionesTipos::where('status', 1)->orderBy('id')->pluck('name', 'id'),
            'sanciones_fallos_ejecutoriados' => RefFallosEjecutoriados::where('status', 1)->orderBy('id')->pluck('name', 'id'),
            'sanciones_estados' => RefSancionesEstados::where('status', 1)->orderBy('id')->pluck('name', 'id'),
        ];

        $this->constants = [
            'upload_path' => storage_path('app/buques_artesanales/sanciones/attachments/'),
        ];
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sanciones = BuqueArtesanalSancion::with('rel_buque_artesanal', 'ref_infraccion_proceso', 'ref_sanciones_estados', 'rel_files')->paginate(10);
        return view('sanciones_buques_artesanales.index')->with(['sanciones' => $sanciones]);
    }

    public function create()
    {
        $sancion = new BuqueArtesanalSancion; // TABLA

        return view('sanciones_buques_artesanales.create')->with(['sancion' => $sancion, 'refs' => $this->refs]);
    }

    public function store(BuqueArtesanalSancion $sancion, CreateBuqueArtesanalSancionRequest $request)
    {
        $result = false;
        $db_error = '';

        try{
            $sancion->buque_artesanal_id = GlobalController::getIntegerReference($request['buque_artesanal_id']);
            $sancion->presunta_violacion = GlobalController::getNumeric($request['presunta_violacion']);
            $sancion->nur = GlobalController::getNumeric($request['nur']);
            $sancion->infraccion_proceso_id = GlobalController::getIntegerReference($request['infraccion_proceso_id']);
            $sancion->sanciones_estatuto_pesquero = GlobalController::getNumeric($request['sanciones_estatuto_pesquero']);
            $sancion->numero_acto_administrativo = $request['numero_acto_administrativo'];
            $sancion->fecha_sancion = GlobalController::getDate($request['fecha_sancion']);
            $sancion->infraccion_sancionada_id = GlobalController::getIntegerReference($request['infraccion_sancionada_id']);
            $sancion->tipo_sancion_id = GlobalController::getIntegerReference($request['tipo_sancion_id']);
            $sancion->fallo_ejecutoriado_id = GlobalController::getIntegerReference($request['fallo_ejecutoriado_id']);
            $sancion->valor_multa = GlobalController::getNumeric($request['valor_multa']);
            $sancion->estado_sancion_id = GlobalController::getIntegerReference($request['estado_sancion_id']);
            $sancion->detalles = $request['detalles'];
            $sancion->status = GlobalController::getNumeric($request['status']);
            $sancion->user_id = auth()->user()->id;

            $sancion->save();

            if($request->hasFile('attachments'))
            {
                $files = $request->file('attachments');

                if($request->hasFile('attachments'))
                {
                    $files = $request->file('attachments');

                    foreach($files as $file)
                    {
                        $baa = new BuqueArtesanalSancionesAttachments;

                        $file_name = GlobalController::getRandomFileName($file->getClientOriginalExtension(), $this->constants['upload_path']);
                        $file->move($this->constants['upload_path'], $file_name);

                        $baa->buque_artesanal_sancion_id = $sancion->id;
                        $baa->name = GlobalController::cleanString(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME));
                        $baa->file = $file_name;

                        $baa->save();
                    }
                }
            }

            $result = true;
        }
        catch(\Exception $e)
        {
            $result = false;
            $db_error = $e->getMessage();
        }

        session()->flash('alert_success', $result);
        session()->flash('alert_message', ($result ? 'Sanción Registrada' : ('Ocurrió un error al registrar la sanción: ' . $db_error)));

        return ($result) ? redirect()->route('buques_artesanales_sanciones_list') : redirect()->route('create_buque_artesanal_sancion_path')->withInput($request->all());
    }

    public function edit(BuqueArtesanalSancion $sancion)
    {
        return view('sanciones_buques_artesanales.edit')->with(['sancion' => $sancion, 'refs' => $this->refs]);
    }

    public function update(BuqueArtesanalSancion $sancion, UpdateBuqueArtesanalSancionRequest $request)
    {
        $result = false;
        $db_error = '';
        try{
            $sancion->buque_artesanal_id = GlobalController::getIntegerReference($request['buque_artesanal_id']);
            $sancion->presunta_violacion = GlobalController::getNumeric($request['presunta_violacion']);
            $sancion->nur = GlobalController::getNumeric($request['nur']);
            $sancion->infraccion_proceso_id = GlobalController::getIntegerReference($request['infraccion_proceso_id']);
            $sancion->sanciones_estatuto_pesquero = GlobalController::getNumeric($request['sanciones_estatuto_pesquero']);
            $sancion->numero_acto_administrativo = $request['numero_acto_administrativo'];
            $sancion->fecha_sancion = GlobalController::getDate($request['fecha_sancion']);
            $sancion->infraccion_sancionada_id = GlobalController::getIntegerReference($request['infraccion_sancionada_id']);
            $sancion->tipo_sancion_id = GlobalController::getIntegerReference($request['tipo_sancion_id']);
            $sancion->fallo_ejecutoriado_id = GlobalController::getIntegerReference($request['fallo_ejecutoriado_id']);
            $sancion->valor_multa = GlobalController::getNumeric($request['valor_multa']);
            $sancion->estado_sancion_id = GlobalController::getIntegerReference($request['estado_sancion_id']);
            $sancion->detalles = $request['detalles'];
            $sancion->status = GlobalController::getNumeric($request['status']);
            $sancion->user_id = auth()->user()->id;

            $sancion->save();

            if($request->hasFile('attachments'))
            {
                $files = $request->file('attachments');

                foreach($files as $file)
                {
                    $baa = new BuqueArtesanalSancionesAttachments;

                    $file_name = GlobalController::getRandomFileName($file->getClientOriginalExtension(), $this->constants['upload_path']);
                    $file->move($this->constants['upload_path'], $file_name);

                    $baa->buque_artesanal_sancion_id = $sancion->id;
                    $baa->name = GlobalController::cleanString(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME));
                    $baa->file = $file_name;

                    $baa->save();
                }
            }

            $sancion->update();

            $result = true;
        }
        catch(\Exception $e)
        {
            $result = false;
            $db_error = $e->getMessage();
        }

        session()->flash('alert_success', $result);
        session()->flash('alert_message', ($result ? 'Sanción Actualizada' : ('Ocurrió un error al actualizar la sanción: ' . $db_error)));

        return ($result) ? redirect()->route('buques_artesanales_sanciones_list') : redirect()->route('edit_buque_artesanal_sancion_path', ['sancion' => $sancion->id])->withInput($request->all());
    }

    // DOWNLOAD

    public function downloadAttachment(BuqueArtesanalSancionesAttachments $file)
    {
        $download = false;

        $buque_sancion = BuqueArtesanalSancion::find($file->buque_artesanal_sancion_id);

        if(!is_null($file) && !is_null($buque_sancion))
        {
            $file_path = $this->constants['upload_path'] . $file->file;

            if(File::exists($file_path))
            {
                $headers = array('Content-Type: application/pdf');
                $download = true;
            }
        }

        return ($download) ? response()->download($file_path, ($file->name . ".pdf"), $headers) : redirect()->route('buques_artesanales_sanciones_list');
    }

    public function downloadAttachmentsZip(BuqueArtesanalSancion $sancion)
    {
        $files = array();

        if(!is_null($sancion) && count($sancion->rel_files) > 0)
        {
            foreach($sancion->rel_files AS $file)
            {
                $file_path = $this->constants['upload_path'] . $file->file;
                if(File::exists($file_path))
                {
                    $files[] = array('path' => $file_path, 'name' => ($file->id . "_" . $file->name . ".pdf"));
                }
            }

            $zip_name = "uvi_sancion_artesanal_" . $sancion->id . '_attachments';
        }

        return count($files) > 0 ? GlobalController::createAndDownloadZip($zip_name ,$files) : redirect()->route('buques_artesanales_sanciones_list');
    }

    // AJAX

    public function deleteAttachment(BuqueArtesanalSancionesAttachments $file)
    {
        $response = [];
        $success = false;

        if(!is_null($file))
        {
            $file_path = $this->constants['upload_path'] . $file->file;

            if(File::exists($file_path))
            {
                File::delete($file_path);
            }

            try{ $success = $file->delete(); }
            catch(\Illuminate\Database\QueryException $e)
            { $success = false; }
        }

        $response['success'] = $success;

        return json_encode($response);
    }
}

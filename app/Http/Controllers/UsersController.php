<?php

namespace Uvi\Http\Controllers;

use Uvi\User;
use Uvi\Rol;
use \File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

// REQUESTS
use Uvi\Http\Requests\CreateUserRequest;
use Uvi\Http\Requests\UpdateUserRequest;
use Uvi\Http\Requests\UpdateUserPasswordRequest;

class UsersController extends Controller
{
    //
    private $constants;

    public function __construct()
    {
        $this->constants = array(
            'upload_path' => 'img/avatars/'
        );
    }

    public function index()
    {
        $users = User::where('id', '<>', 1)->orderBy('id', 'asc')->paginate(10);

        return view('users.index')->with(['users' => $users]);

        //$posts = Post::all(); // Listar todos por orden ascendente
        //$posts = Post::orderBy('id', 'desc')->paginate(10);

        //return view('users.index')->with(['users' => $users]);
    }

    public function show(Post $post)
    {
        /*$post = Post::find($id_post);

        if(is_null($post))
        {
            abort(404);
        }*/

        return view('posts.show')->with(['post' => $post]);
    }

    public function create()
    {
        $user = new User;
        $roles = Rol::where('status', 1)->get();

        return view('users.create')->with(['user' => $user, 'roles' => $roles]);
    }

    public function store(CreateUserRequest $request)
    {
        $user = new User;

        $file_name = 'default.png';

        if($request->hasFile('image'))
        {
            $image = $request->file('image');
            $s = strtoupper(md5(uniqid(rand(),true)));
            $file_name = substr($s,0,8) . '-' . substr($s,8,4) . '-' . substr($s,12,4). '-' . substr($s,16,4). '-' . substr($s,20);
            $extension = $image->getClientOriginalExtension();

            $file_name .= ("." . $extension);

            $image->move($this->constants['upload_path'], $file_name);

            $img_file = $this->constants['upload_path'] . $file_name;

            self::resize_crop_image(160, 160, $img_file, $img_file);
        }

        $user->fill($request->only(['name', 'email', 'status', 'rol_id']));
        $user->password = bcrypt($request['password']);
        $user->profile_picture = $file_name;


        try{ $result = $user->save(); }
        catch(\Illuminate\Database\QueryException $e)
        { $result = false;}

        session()->flash('alert_success', $result);
        session()->flash('alert_message', ($result ? 'Usuario Registrado' : 'Ocurrió un error al registrar al usuario'));

        if(!$result)
        {
            return redirect()->route('create_user_path')->withInput($request->except(['password', 'password_confirmation']));
        }
        else
        {
            return redirect()->route('users_list');
        }

    }

    public function edit(User $user)
    {
        $roles = Rol::where('status', 1)->get();
        return view('users.edit')->with(['user' => $user, 'roles' => $roles]);
    }

    public function update(User $user, UpdateUserRequest $request)
    {
        $file_name = $user->profile_picture;

        $file_current_image = $this->constants['upload_path'] . $user->profile_picture;

        if($request->has('remove_img') && $request['remove_img'] == 1)
        {
            if($user->profile_picture != $file_name && File::exists($file_current_image))
            {
                File::delete($file_current_image);
            }
        }
        else
        {
            if($request->hasFile('image'))
            {
                if($user->profile_picture != $file_name && File::exists($file_current_image))
                {
                    File::delete($file_current_image);
                }

                $image = $request->file('image');

                $s = strtoupper(md5(uniqid(rand(),true)));
                $file_name = substr($s,0,8) . '-' . substr($s,8,4) . '-' . substr($s,12,4). '-' . substr($s,16,4). '-' . substr($s,20);
                $extension = $image->getClientOriginalExtension();

                $file_name .= ("." . $extension);

                $image->move($this->constants['upload_path'], $file_name);

                $img_file = $this->constants['upload_path'] . $file_name;

                self::resize_crop_image(160, 160, $img_file, $img_file);
            }
        }

        $user->fill($request->only(['name', 'status', 'rol_id']));
        $user->profile_picture = $file_name;


        try{ $result = $user->update(); }
        catch(\Illuminate\Database\QueryException $e)
        { $result = false; }

        session()->flash('alert_success', $result);
        session()->flash('alert_message', ($result ? 'Usuario Actualizado Correctamente' : 'Ocurrió un error al actualizar al usuario'));
        return redirect()->route('users_list');
    }

    /*public function delete(Post $post)
    {
        if($post->user_id == auth()->user()->id)
        {
            $post->delete();

            session()->flash('alert_success', true);
            session()->flash('alert_message', 'Post Successfully Deleted');
        }

        return redirect()->route('posts_path');
    }*/

    public function showProfile()
    {
        $user = User::find(auth()->user()->id);

        if(is_null($user))
        {
            redirect()->route('logout');
        }

        return view('users.profile')->with(['user' => $user]);
    }

    public function updateProfile(User $user, UpdateUserRequest $request)
    {
        $file_name = $user->profile_picture;

        $file_current_image = $this->constants['upload_path'] . $user->profile_picture;

        if($request->has('remove_img') && $request['remove_img'] == 1)
        {
            $file_name = "default.png";

            if($user->profile_picture != $file_name && File::exists($file_current_image))
            {
                File::delete($file_current_image);
            }
        }
        else
        {
            if($request->hasFile('image'))
            {
                if($user->profile_picture != $file_name && File::exists($file_current_image))
                {
                    File::delete($file_current_image);
                }

                $image = $request->file('image');

                $s = strtoupper(md5(uniqid(rand(),true)));
                $file_name = substr($s,0,8) . '-' . substr($s,8,4) . '-' . substr($s,12,4). '-' . substr($s,16,4). '-' . substr($s,20);
                $extension = $image->getClientOriginalExtension();

                $file_name .= ("." . $extension);

                $image->move($this->constants['upload_path'], $file_name);

                $img_file = $this->constants['upload_path'] . $file_name;

                self::resize_crop_image(160, 160, $img_file, $img_file);
            }
        }

        $user->fill($request->only(['name']));
        $user->profile_picture = $file_name;

        try{ $result = $user->update(); }
        catch(\Illuminate\Database\QueryException $e)
        { $result = false; }

        session()->flash('alert_success', $result);
        session()->flash('alert_message', ($result ? 'Perfil Actualizado Correctamente' : 'Ocurrió un error al actualizar el perfil'));
        return redirect()->route('show_profile');
    }

    public function updatePassword(User $user, UpdateUserPasswordRequest $request)
    {
        $validate_current_password = Hash::check($request['current_password'], $user->password);

        if (!$validate_current_password)
        {
            $error = ['validation_current_password' => 'La contraseña actual es incorrecta'];
            return redirect()->route('show_profile')->withErrors($error);
        }
        else
        {
            $user->password = bcrypt($request['password']);

            try{ $result = $user->update(); }
            catch(\Illuminate\Database\QueryException $e)
            { $result = false; }

            if(!$result)
            {
                session()->flash('alert_success', $result);
                session()->flash('alert_message', ($result ? 'Perfil Actualizado Correctamente' : 'Ocurrió un error al actualizar el perfil'));
                return redirect()->route('show_profile');
            }
            else
            {
                return redirect()->route('logout');
            }

        }
    }

    private function resize_crop_image($max_width, $max_height, $source_file, $dst_dir, $quality = 80)
    {
        $imgsize = getimagesize($source_file);
        $width = $imgsize[0];
        $height = $imgsize[1];
        $mime = $imgsize['mime'];

        switch($mime){
            case 'image/gif':
                $image_create = "imagecreatefromgif";
                $image = "imagegif";
                break;

            case 'image/png':
                $image_create = "imagecreatefrompng";
                $image = "imagepng";
                $quality = 7;
                break;

            case 'image/jpeg':
                $image_create = "imagecreatefromjpeg";
                $image = "imagejpeg";
                $quality = 80;
                break;

            default:
                return false;
                break;
        }

        $dst_img = imagecreatetruecolor($max_width, $max_height);
        $src_img = $image_create($source_file);

        $width_new = $height * $max_width / $max_height;
        $height_new = $width * $max_height / $max_width;
        //if the new width is greater than the actual width of the image, then the height is too large and the rest cut off, or vice versa
        if($width_new > $width){
            //cut point by height
            $h_point = (($height - $height_new) / 2);
            //copy image
            imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
        }else{
            //cut point by width
            $w_point = (($width - $width_new) / 2);
            imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
        }

        $image($dst_img, $dst_dir, $quality);

        if($dst_img)imagedestroy($dst_img);
        if($src_img)imagedestroy($src_img);
    }
}

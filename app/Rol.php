<?php

namespace Uvi;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model{

    protected $table = "roles";

    protected $fillable = ['name', 'status'];
}

?>

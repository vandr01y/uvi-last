<?php

namespace Uvi\ReferencesModels\Vessels;

use Illuminate\Database\Eloquent\Model;

class RefMaterialesTipoMarco extends Model{

    protected $table = "materiales_tipo_marco";

    protected $fillable = ['name', 'status'];
}

?>

<?php

namespace Uvi\ReferencesModels\Vessels;

use Illuminate\Database\Eloquent\Model;

class RefAuthorisationHolders extends Model{

    protected $table = "authorisation_holders";

    protected $fillable = ['code', 'name', 'status'];
}

?>

<?php

namespace Uvi\ReferencesModels\Vessels;

use Illuminate\Database\Eloquent\Model;

class RefZonasPescaAutorizadas extends Model{

    protected $table = "zonas_pesca_autorizadas";

    protected $fillable = ['name', 'status'];
}

?>

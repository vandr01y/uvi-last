<?php

namespace Uvi\ReferencesModels\Vessels;

use Illuminate\Database\Eloquent\Model;

class RefColombiaPuertosDesembarcoAutorizados extends Model{

    protected $table = "colombia_puertos_desembarco_autorizados";

    protected $fillable = ['name', 'status'];
}

?>

<?php

namespace Uvi\ReferencesModels\Vessels;

use Illuminate\Database\Eloquent\Model;

class RefMaterialesBajantes extends Model{

    protected $table = "materiales_bajantes";

    protected $fillable = ['name', 'status'];
}

?>

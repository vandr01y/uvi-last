<?php

namespace Uvi\ReferencesModels\Vessels;

use Illuminate\Database\Eloquent\Model;

class RefDenominacionArtePescaTipoSedalAnzuelos extends Model{

    protected $table = "denominacion_arte_pesca_tipo_sedal_anzuelos";

    protected $fillable = ['name', 'status'];
}

?>

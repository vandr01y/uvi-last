<?php

namespace Uvi\ReferencesModels\Vessels;

use Illuminate\Database\Eloquent\Model;

class RefTiposAnzuelos extends Model{

    protected $table = "tipos_anzuelos";

    protected $fillable = ['name', 'status'];
}

?>

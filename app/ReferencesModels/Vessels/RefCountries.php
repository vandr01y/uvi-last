<?php

namespace Uvi\ReferencesModels\Vessels;

use Illuminate\Database\Eloquent\Model;

class RefCountries extends Model{

    protected $table = "countries";

    protected $fillable = ['name', 'alpha_2', 'alpha_3', 'status'];
}

?>

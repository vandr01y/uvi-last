<?php

namespace Uvi\ReferencesModels\Vessels;

use Illuminate\Database\Eloquent\Model;

class RefMaterialesPrincipalesTrampasNasas extends Model{

    protected $table = "materiales_principales_trampas_nasas";

    protected $fillable = ['name', 'status'];
}

?>

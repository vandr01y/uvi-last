<?php

namespace Uvi\ReferencesModels\Vessels;

use Illuminate\Database\Eloquent\Model;

class RefMaterialesLineaMadre extends Model{

    protected $table = "materiales_linea_madre";

    protected $fillable = ['name', 'status'];
}

?>

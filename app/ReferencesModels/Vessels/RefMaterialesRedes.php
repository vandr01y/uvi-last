<?php

namespace Uvi\ReferencesModels\Vessels;

use Illuminate\Database\Eloquent\Model;

class RefMaterialesRedes extends Model{

    protected $table = "materiales_redes";

    protected $fillable = ['name', 'status'];
}

?>

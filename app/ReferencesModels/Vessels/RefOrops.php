<?php

namespace Uvi\ReferencesModels\Vessels;

use Illuminate\Database\Eloquent\Model;

class RefOrops extends Model{

    protected $table = "orops";

    protected $fillable = ['siglas', 'name', 'status'];
}

?>

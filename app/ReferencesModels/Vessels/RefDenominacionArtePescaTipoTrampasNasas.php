<?php

namespace Uvi\ReferencesModels\Vessels;

use Illuminate\Database\Eloquent\Model;

class RefDenominacionArtePescaTipoTrampasNasas extends Model{

    protected $table = "denominacion_arte_pesca_tipo_trampas_nasas";

    protected $fillable = ['name', 'status'];
}

?>

<?php

namespace Uvi\ReferencesModels\Vessels;

use Illuminate\Database\Eloquent\Model;

class RefHullMaterials extends Model{

    protected $table = "hull_materials";

    protected $fillable = ['code', 'name', 'status'];
}

?>

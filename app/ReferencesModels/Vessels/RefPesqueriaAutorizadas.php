<?php

namespace Uvi\ReferencesModels\Vessels;

use Illuminate\Database\Eloquent\Model;

class RefPesqueriaAutorizadas extends Model{

    protected $table = "pesqueria_autorizadas";

    protected $fillable = ['name', 'status'];
}

?>

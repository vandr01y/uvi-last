<?php

namespace Uvi\ReferencesModels\Vessels;

use Illuminate\Database\Eloquent\Model;

class RefVmsTypes extends Model{

    protected $table = "vms_types";

    protected $fillable = ['code', 'name', 'status'];
}

?>

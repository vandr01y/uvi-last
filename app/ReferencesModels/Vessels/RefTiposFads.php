<?php

namespace Uvi\ReferencesModels\Vessels;

use Illuminate\Database\Eloquent\Model;

class RefTiposFads extends Model{

    protected $table = "tipos_fad";

    protected $fillable = ['name', 'status'];
}

?>

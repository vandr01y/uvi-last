<?php

namespace Uvi\ReferencesModels\Vessels;

use Illuminate\Database\Eloquent\Model;

class RefVesselTypes extends Model{

    protected $table = "vessel_types";

    protected $fillable = ['name', 'status'];
}

?>

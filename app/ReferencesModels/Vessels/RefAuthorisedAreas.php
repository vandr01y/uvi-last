<?php

namespace Uvi\ReferencesModels\Vessels;

use Illuminate\Database\Eloquent\Model;

class RefAuthorisedAreas extends Model{

    protected $table = "authorised_areas";

    protected $fillable = ['code', 'name', 'status'];
}

?>

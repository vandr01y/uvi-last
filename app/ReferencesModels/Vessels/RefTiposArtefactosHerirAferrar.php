<?php

namespace Uvi\ReferencesModels\Vessels;

use Illuminate\Database\Eloquent\Model;

class RefTiposArtefactosHerirAferrar extends Model{

    protected $table = "tipos_artefactos_herir_aferrar";

    protected $fillable = ['name', 'status'];
}

?>

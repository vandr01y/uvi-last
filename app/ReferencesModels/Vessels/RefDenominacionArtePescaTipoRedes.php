<?php

namespace Uvi\ReferencesModels\Vessels;

use Illuminate\Database\Eloquent\Model;

class RefDenominacionArtePescaTipoRedes extends Model{

    protected $table = "denominacion_arte_pesca_tipo_redes";

    protected $fillable = ['name', 'status'];
}

?>

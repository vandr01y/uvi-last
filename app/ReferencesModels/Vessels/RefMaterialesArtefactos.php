<?php

namespace Uvi\ReferencesModels\Vessels;

use Illuminate\Database\Eloquent\Model;

class RefMaterialesArtefactos extends Model{

    protected $table = "materiales_artefactos";

    protected $fillable = ['name', 'status'];
}

?>

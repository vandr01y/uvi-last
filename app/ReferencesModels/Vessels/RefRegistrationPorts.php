<?php

namespace Uvi\ReferencesModels\Vessels;

use Illuminate\Database\Eloquent\Model;

class RefRegistrationPorts extends Model{

    protected $table = "registration_ports";

    protected $fillable = ['name', 'status'];
}

?>

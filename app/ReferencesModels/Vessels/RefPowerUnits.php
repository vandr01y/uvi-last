<?php

namespace Uvi\ReferencesModels\Vessels;

use Illuminate\Database\Eloquent\Model;

class RefPowerUnits extends Model{

    protected $table = "power_units";

    protected $fillable = ['code', 'name', 'status'];
}

?>

<?php

namespace Uvi\ReferencesModels\Vessels;

use Illuminate\Database\Eloquent\Model;

class RefFishHoldTypes extends Model{

    protected $table = "fish_hold_types";

    protected $fillable = ['code', 'name', 'status'];
}

?>

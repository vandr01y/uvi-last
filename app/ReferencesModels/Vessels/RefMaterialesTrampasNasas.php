<?php

namespace Uvi\ReferencesModels\Vessels;

use Illuminate\Database\Eloquent\Model;

class RefMaterialesTrampasNasas extends Model{

    protected $table = "materiales_trampas_nasas";

    protected $fillable = ['name', 'status'];
}

?>

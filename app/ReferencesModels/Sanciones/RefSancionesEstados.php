<?php

namespace Uvi\ReferencesModels\Sanciones;

use Illuminate\Database\Eloquent\Model;

class RefSancionesEstados extends Model{

    protected $table = "sanciones_estados";

    protected $fillable = ['code', 'name', 'status'];
}

?>

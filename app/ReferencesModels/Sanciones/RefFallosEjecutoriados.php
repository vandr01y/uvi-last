<?php

namespace Uvi\ReferencesModels\Sanciones;

use Illuminate\Database\Eloquent\Model;

class RefFallosEjecutoriados extends Model{

    protected $table = "sanciones_fallos_ejecutoriados";

    protected $fillable = ['code', 'name', 'status'];
}

?>

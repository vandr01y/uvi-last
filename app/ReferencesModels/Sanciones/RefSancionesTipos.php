<?php

namespace Uvi\ReferencesModels\Sanciones;

use Illuminate\Database\Eloquent\Model;

class RefSancionesTipos extends Model{

    protected $table = "sanciones_tipos";

    protected $fillable = ['code', 'name', 'status'];
}

?>

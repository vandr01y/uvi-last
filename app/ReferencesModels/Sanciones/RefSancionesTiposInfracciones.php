<?php

namespace Uvi\ReferencesModels\Sanciones;

use Illuminate\Database\Eloquent\Model;

class RefSancionesTiposInfracciones extends Model{

    protected $table = "sanciones_tipos_infracciones";

    protected $fillable = ['code', 'name', 'status'];
}

?>

<?php

namespace Uvi\ReferencesModels\BuquesArtesanales;

use Illuminate\Database\Eloquent\Model;

class RefColombiaDepartamentos extends Model{

    protected $table = "colombia_departamentos";

    protected $fillable = ['name', 'code','status'];
}

?>

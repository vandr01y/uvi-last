<?php

namespace Uvi\ReferencesModels\BuquesArtesanales;

use Illuminate\Database\Eloquent\Model;

class RefBuquesArtesanalesTiposMotor extends Model{

    protected $table = "buque_artesanal_tipos_motor";

    protected $fillable = ['name', 'status'];
}

?>

<?php

namespace Uvi\ReferencesModels\BuquesArtesanales;

use Illuminate\Database\Eloquent\Model;

class RefBuqueArtesanalTipoEmbarcacion extends Model{

    protected $table = "buque_artesanal_tipo_embarcacion";

    protected $fillable = ['name', 'status'];
}

?>

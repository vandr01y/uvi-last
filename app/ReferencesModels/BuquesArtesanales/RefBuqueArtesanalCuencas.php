<?php

namespace Uvi\ReferencesModels\BuquesArtesanales;

use Illuminate\Database\Eloquent\Model;

class RefBuqueArtesanalCuencas extends Model{

    protected $table = "buque_artesanal_cuencas";

    protected $fillable = ['name', 'status'];
}

?>

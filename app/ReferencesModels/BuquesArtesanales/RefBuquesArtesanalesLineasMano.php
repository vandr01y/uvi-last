<?php

namespace Uvi\ReferencesModels\BuquesArtesanales;

use Illuminate\Database\Eloquent\Model;

class RefBuquesArtesanalesLineasMano extends Model{

    protected $table = "buques_artesanales_lineas_mano";

    protected $fillable = ['name', 'status'];
}

?>

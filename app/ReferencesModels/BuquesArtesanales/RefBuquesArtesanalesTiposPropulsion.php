<?php

namespace Uvi\ReferencesModels\BuquesArtesanales;

use Illuminate\Database\Eloquent\Model;

class RefBuquesArtesanalesTiposPropulsion extends Model{

    protected $table = "buque_artesanal_tipos_propulsion";

    protected $fillable = ['name', 'status'];
}

?>

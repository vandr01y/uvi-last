<?php

namespace Uvi\ReferencesModels\BuquesArtesanales;

use Illuminate\Database\Eloquent\Model;

class RefChalecosSalvavidasEstados extends Model{

    protected $table = "chalecos_salvavidas_estados";

    protected $fillable = ['name', 'status'];
}

?>

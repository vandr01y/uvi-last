<?php

namespace Uvi\ReferencesModels\BuquesArtesanales;

use Illuminate\Database\Eloquent\Model;

class RefBuqueArtesanalArtePescaTipoRed extends Model{

    protected $table = "buques_artesanales_arte_pesca_tipo_red";

    protected $fillable = ['name', 'status'];
}

?>

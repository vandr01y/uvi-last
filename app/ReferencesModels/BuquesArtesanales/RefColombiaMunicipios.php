<?php

namespace Uvi\ReferencesModels\BuquesArtesanales;

use Illuminate\Database\Eloquent\Model;

class RefColombiaMunicipios extends Model{

    protected $table = "colombia_municipios";

    protected $fillable = ['name', 'departamento_id', 'status'];
}

?>

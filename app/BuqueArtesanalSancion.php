<?php

namespace Uvi;

use Illuminate\Database\Eloquent\Model;

// References
use Uvi\ReferencesModels\Sanciones\RefSancionesTiposInfracciones;
use Uvi\ReferencesModels\Sanciones\RefSancionesEstados;
// Relationships
use Uvi\BuqueArtesanal;
use Uvi\RelationshipsModels\BuqueArtesanalSanciones\BuqueArtesanalSancionesAttachments;

class BuqueArtesanalSancion extends Model{

    protected $table = "buques_artesanales_sanciones";


    public function rel_buque_artesanal()
    {
        return $this->belongsTo(BuqueArtesanal::Class, 'buque_artesanal_id');
    }

    public function ref_infraccion_proceso()
    {
        return $this->belongsTo(RefSancionesTiposInfracciones::Class, 'infraccion_proceso_id');
    }

    public function ref_sanciones_estados()
    {
        return $this->belongsTo(RefSancionesEstados::Class, 'estado_sancion_id');
    }

    public function rel_files()
    {
        return $this->hasMany(BuqueArtesanalSancionesAttachments::class, 'buque_artesanal_sancion_id');
    }
}

?>

<?php

namespace Uvi;

use Illuminate\Database\Eloquent\Model;

// REFERENCES
use Uvi\ReferencesModels\Vessels\RefCountries;
use Uvi\ReferencesModels\BuquesArtesanales\RefBuqueArtesanalTipoEmbarcacion;

// RELATIONSHIPS
use Uvi\RelationshipsModels\BuquesArtesanales\BuquesArtesanalesArtesPictures;
use Uvi\RelationshipsModels\BuquesArtesanales\BuquesArtesanalesPictures;

class BuqueArtesanal extends Model{

    protected $table = "buques_artesanales";

    protected $fillable = [
        'nombre',
        'matricula',
        'numero_identificacion_embarcacion',
        'status',
    ];

    public function ref_tipo_embarcacion()
    {
        return $this->belongsTo(RefBuqueArtesanalTipoEmbarcacion::Class, 'tipo_id');
    }

    public function ref_pais()
    {
        return $this->belongsTo(RefCountries::Class, 'bandera_id');
    }

    public function ba_e_pictures()
    {
        return $this->hasMany(BuquesArtesanalesPictures::Class, 'buque_artesanal_id');
    }

    public function ba_a_pictures()
    {
        return $this->hasMany(BuquesArtesanalesArtesPictures::Class,'buque_artesanal_id');
    }
}

?>

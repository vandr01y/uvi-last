<?php

namespace Uvi;

use Illuminate\Database\Eloquent\Model;

// REFERENCES
use Uvi\ReferencesModels\Vessels\RefVmsTypes;
use Uvi\ReferencesModels\Vessels\RefOrops;
use Uvi\ReferencesModels\Vessels\RefCountries;
use Uvi\ReferencesModels\Vessels\RefVesselTypes;
use Uvi\ReferencesModels\Vessels\RefRegistrationPorts;
use Uvi\ReferencesModels\Vessels\RefPowerUnits;
use Uvi\ReferencesModels\Vessels\RefHullMaterials;
use Uvi\ReferencesModels\Vessels\RefFishHoldTypes;
use Uvi\ReferencesModels\Vessels\RefAuthorisationHolders;
use Uvi\ReferencesModels\Vessels\RefAuthorisedAreas;

// CUSTOM REFERENCES
use Uvi\ReferencesModels\Vessels\RefZonasPescaAutorizadas;
use Uvi\ReferencesModels\Vessels\RefColombiaPuertosDesembarcoAutorizados;
use Uvi\ReferencesModels\Vessels\RefPesqueriaAutorizadas;
use Uvi\ReferencesModels\Vessels\RefDenominacionArtePescaTipoRedes;
use Uvi\ReferencesModels\Vessels\RefMaterialesRedes;
use Uvi\ReferencesModels\Vessels\RefDenominacionArtePescaTipoSedalAnzuelos;
use Uvi\ReferencesModels\Vessels\RefTiposAnzuelos;
use Uvi\ReferencesModels\Vessels\RefMaterialesLineaMadre;
use Uvi\ReferencesModels\Vessels\RefMaterialesBajantes;
use Uvi\ReferencesModels\Vessels\RefDenominacionArtePescaTipoTrampasNasas;
use Uvi\ReferencesModels\Vessels\RefMaterialesPrincipalesTrampasNasas;
use Uvi\ReferencesModels\Vessels\RefTiposArtefactosHerirAferrar;
use Uvi\ReferencesModels\Vessels\RefMaterialesArtefactos;
use Uvi\ReferencesModels\Vessels\RefTiposFads;
use Uvi\ReferencesModels\Vessels\RefMaterialesTipoMarco;

// RELATIONSHIPS
use Uvi\RelationshipsModels\Vessels\VesselPictures;

class Vessel extends Model{

    protected $table = "vessels";

    protected $fillable = [
        'numero_omi',
        'marcado_externo',
        'mlri',
        'mmsi',
        'indicador_vms',
        'tipo_vms',
        'detalles_vms',
        'sistema_ais',
        'detalles_ais',
        'status'
    ];

    // FORM_2
    public function ref_orop()
    {
        return $this->belongsTo(RefOrops::Class, 'orop_id');
    }

    // FORM_3
    public function ref_pais_pabellon()
    {
        return $this->belongsTo(RefCountries::Class, 'estado_actual_pabellon');
    }

    public function ref_puerto_registro()
    {
        return $this->belongsTo(RefRegistrationPorts::Class, 'puerto_registro');
    }

    public function ref_tipo_barco()
    {
        return $this->belongsTo(RefVesselTypes::Class, 'tipo_barco');
    }

    // FORM_4
    public function ref_power_unit()
    {
        return $this->belongsTo(RefPowerUnits::Class, 'unidad_potencia');
    }

    public function ref_hull_material()
    {
        return $this->belongsTo(RefHullMaterials::Class, 'material_casco');
    }

    public function ref_fish_hold_type()
    {
        return $this->belongsTo(RefFishHoldTypes::Class, 'tipo_bodega_pescado');
    }

    // FORM_5
    public function ref_pais_construccion()
    {
        return $this->belongsTo(RefCountries::Class, 'pais_construccion');
    }

    // FORM_6
    public function ref_pais_propietario()
    {
        return $this->belongsTo(RefCountries::Class, 'nacionalidad_propietario');
    }

    public function ref_pais_gerente_operador()
    {
        return $this->belongsTo(RefCountries::Class, 'nacionalidad_operador');
    }

    public function ref_pais_administrador()
    {
        return $this->belongsTo(RefCountries::Class, 'nacionalidad_administrador');
    }

    // FORM_7
    public function ref_authorised_area()
    {
        return $this->belongsTo(RefAuthorisedAreas::Class, 'zona_pesca_autorizada');
    }

    public function ref_puerto_desembarco()
    {
        return $this->belongsTo(RefColombiaPuertosDesembarcoAutorizados::Class, 'puerto_desembarco');
    }

    public function ref_pesqueria_autorizada()
    {
        return $this->belongsTo(RefPesqueriaAutorizadas::Class, 'pesqueria_autorizada');
    }

    // FORM_8
    public function ref_arte_pesca_red()
    {
        return $this->belongsTo(RefDenominacionArtePescaTipoRedes::Class,'arte_pesca_red');
    }

    public function ref_material_red()
    {
        return $this->belongsTo(RefMaterialesRedes::Class,'material_red');
    }

    public function ref_arte_pesca_anzuelo()
    {
        return $this->belongsTo(RefDenominacionArtePescaTipoSedalAnzuelos::Class,'arte_pesca_anzuelo');
    }

    public function ref_tipo_anzuelo()
    {
        return $this->belongsTo(RefTiposAnzuelos::Class,'tipo_anzuelo');
    }

    public function ref_material_linea_madre()
    {
        return $this->belongsTo(RefMaterialesLineaMadre::Class,'');
    }

    public function ref_material_bajante()
    {
        return $this->belongsTo(RefMaterialesBajantes::Class,'material_bajantes');
    }

    public function ref_arte_pesca_trampa()
    {
        return $this->belongsTo(RefDenominacionArtePescaTipoTrampasNasas::Class,'arte_pesca_trampa');
    }

    public function ref_materiales_trampa()
    {
        return $this->belongsTo(RefMaterialesPrincipalesTrampasNasas::Class,'materiales_trampas');
    }

    public function ref_tipo_artefactos_herir()
    {
        return $this->belongsTo(RefTiposArtefactosHerirAferrar::Class,'tipo_artefactos_herir');
    }

    public function ref_material_artefactos()
    {
        return $this->belongsTo(RefMaterialesArtefactos::Class,'material_artefactos');
    }

    public function ref_tipo_fad()
    {
        return $this->belongsTo(RefTiposFads::Class,'tipos_fad');
    }

    //
    public function vr_pictures()
    {
        return $this->hasMany(VesselPictures::Class, 'vessel_id');
    }
}

?>

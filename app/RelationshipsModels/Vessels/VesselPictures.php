<?php

namespace Uvi\RelationshipsModels\Vessels;

use Illuminate\Database\Eloquent\Model;

class VesselPictures extends Model{

    protected $table = "vessel_pictures";

    protected $fillable = [
        'picture',
        'picture_link',
        'picture_details',
    ];
}

?>

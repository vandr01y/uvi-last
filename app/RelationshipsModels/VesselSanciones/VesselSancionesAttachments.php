<?php

namespace Uvi\RelationshipsModels\VesselSanciones;

use Illuminate\Database\Eloquent\Model;

class VesselSancionesAttachments extends Model{

    protected $table = "vessel_sanciones_attachments";

    protected $fillable = [
        'file',
        'details'
    ];
}

?>

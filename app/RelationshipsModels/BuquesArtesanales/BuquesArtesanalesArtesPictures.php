<?php

namespace Uvi\RelationshipsModels\BuquesArtesanales;

use Illuminate\Database\Eloquent\Model;

class BuquesArtesanalesArtesPictures extends Model{

    protected $table = "buques_artesanales_artes_pictures";

    protected $fillable = [
        'nombre',
        'file',
        'status',
    ];
}

?>

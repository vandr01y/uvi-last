<?php

namespace Uvi\RelationshipsModels\BuquesArtesanales;

use Illuminate\Database\Eloquent\Model;

class BuquesArtesanalesPictures extends Model{

    protected $table = "buques_artesanales_pictures";

    protected $fillable = [
        'name',
        'file',
        'status',
    ];
}

?>

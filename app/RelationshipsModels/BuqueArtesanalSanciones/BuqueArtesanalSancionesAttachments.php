<?php

namespace Uvi\RelationshipsModels\BuqueArtesanalSanciones;

use Illuminate\Database\Eloquent\Model;

class BuqueArtesanalSancionesAttachments extends Model{

    protected $table = "buque_artesanal_sanciones_attachments";

    protected $fillable = [
        'file',
        'details'
    ];
}

?>

<?php

namespace Uvi;

use Illuminate\Database\Eloquent\Model;

// References
use Uvi\ReferencesModels\Sanciones\RefSancionesTiposInfracciones;
use Uvi\ReferencesModels\Sanciones\RefSancionesEstados;
// Relationships
use Uvi\Vessel;
use Uvi\RelationshipsModels\VesselSanciones\VesselSancionesAttachments;

class VesselSancion extends Model{

    protected $table = "vessel_sanciones";


    public function rel_vessel()
    {
        return $this->belongsTo(Vessel::Class, 'vessel_id');
    }

    public function ref_infraccion_proceso()
    {
        return $this->belongsTo(RefSancionesTiposInfracciones::Class, 'infraccion_proceso_id');
    }

    public function ref_sanciones_estados()
    {
        return $this->belongsTo(RefSancionesEstados::Class, 'estado_sancion_id');
    }

    public function rel_files()
    {
        return $this->hasMany(VesselSancionesAttachments::class, 'vessel_sancion_id');
    }
}

?>

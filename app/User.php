<?php

namespace Uvi;


use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Uvi\Rol;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'status', 'rol_id', 'profile_picture'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function rol()
    {
        //return $this->belongsTo(Rol::class); // Toma el metodo actual (rol) y le concatena '_id' y lo busca en el modelo especificado
        return $this->belongsTo(Rol::class, 'rol_id');
    }
}

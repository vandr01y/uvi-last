<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['auth', 'checkUserStatus']], function(){

    Route::get('/', 'HomeController@index');

    // SHOW
        // USERS
        Route::get('/home', 'HomeController@index')->name('home_path');
        Route::get('/users/profile', 'UsersController@showProfile')->name('show_profile');
        Route::get('/users/create', 'UsersController@create')->name('create_user_path');
        Route::get('/users/{user}/edit', 'UsersController@edit')->name('edit_user_path');
        Route::get('/users', 'UsersController@index')->name('users_list');

        // VESSELS
        Route::get('/vessels/create', 'VesselsController@create')->name('create_vessel_path');
        Route::get('/vessels/{vessel}/edit', 'VesselsController@edit')->name('edit_vessel_path');
        Route::get('/vessels/{vessel}/details', 'VesselsController@details')->name('details_vessel_path');
        Route::get('/vessels', 'VesselsController@index')->name('vessels_list');
        Route::get('/vessels/delete_image/{id}', 'VesselsController@deleteImage')->name('ajax_vessel_delete_image');

        // BUQUES ARTESANALES
        Route::get('/buques_artesanales/create', 'BuquesArtesanalesController@create')->name('create_buque_artesanal_path');
        Route::get('/buques_artesanales/{buque}/edit', 'BuquesArtesanalesController@edit')->name('edit_buque_artesanal_path');
        Route::get('/buques_artesanales', 'BuquesArtesanalesController@index')->name('buques_artesanales_list');
        Route::get('/buques_artesanales/delete_image/{imagen}', 'BuquesArtesanalesController@deleteImage')->name('ajax_buque_artesanal_delete_image');
        Route::get('/buques_artesanales/delete_image_arte/{imagen}', 'BuquesArtesanalesController@deleteImageArte')->name('ajax_buque_artesanal_delete_image_arte');

        // SANCIONES INDUSTRIALES
        Route::get('/sanciones', 'VesselSancionesController@index')->name('vessel_sanciones_list');
        Route::get('/sanciones/create', 'VesselSancionesController@create')->name('create_vessel_sancion_path');
        Route::get('/sanciones/{sancion}/edit', 'VesselSancionesController@edit')->name('edit_vessel_sancion_path');
        Route::get('/sanciones/download_attachment/{file}/', 'VesselSancionesController@downloadAttachment')->name('vessel_sancion_download_attachment');
        Route::get('/sanciones/download_attachments_zip/{sancion}/', 'VesselSancionesController@downloadAttachmentsZip')->name('vessel_sancion_download_attachments_zip');
        Route::get('/sanciones/delete_attachment/{file}', 'VesselSancionesController@deleteAttachment')->name('ajax_vessel_sancion_delete_attachment');

        // SANCIONES - ARTESANALES
        Route::get('/sanciones_buques_artesanales', 'BuquesArtesanalesSancionesController@index')->name('buques_artesanales_sanciones_list');
        Route::get('/sanciones_buques_artesanales/create', 'BuquesArtesanalesSancionesController@create')->name('create_buque_artesanal_sancion_path');
        Route::get('/sanciones_buques_artesanales/{sancion}/edit', 'BuquesArtesanalesSancionesController@edit')->name('edit_buque_artesanal_sancion_path');
        Route::get('/sanciones_buques_artesanales/download_attachment/{file}/', 'BuquesArtesanalesSancionesController@downloadAttachment')->name('buque_artesanal_sancion_download_attachment');
        Route::get('/sanciones_buques_artesanales/download_attachments_zip/{sancion}/', 'BuquesArtesanalesSancionesController@downloadAttachmentsZip')->name('buque_artesanal_sancion_download_attachments_zip');
        Route::get('/sanciones_buques_artesanales/delete_attachment/{file}', 'BuquesArtesanalesSancionesController@deleteAttachment')->name('ajax_buque_artesanal_sancion_delete_attachment');

    // CREATE - UPDATE
        // USERS
        Route::post('/users', 'UsersController@store')->name('store_user_path');
        Route::put('/users/{user}', 'UsersController@update')->name('update_user_path');
        Route::put('/users/profile/{user}', 'UsersController@updateProfile')->name('update_user_profile_path');
        Route::put('/users/profile/change_password/{user}', 'UsersController@updatePassword')->name('update_user_password_path');

        // VESSELS
        Route::post('/vessels', 'VesselsController@store')->name('store_vessel_path');
        Route::put('/vessels/{vessel}', 'VesselsController@update')->name('update_vessel_path');

        // BUQUES ARTESANALES
        Route::post('/buques_artesanales', 'BuquesArtesanalesController@store')->name('store_buque_artesanal_path');
        Route::put('/buques_artesanales/{buque}', 'BuquesArtesanalesController@update')->name('update_buque_artesanal_path');

        // SANCIONES - INDUSTRIALES
        Route::post('/sanciones', 'VesselSancionesController@store')->name('store_vessel_sancion_path');
        Route::put('/sanciones/{sancion}', 'VesselSancionesController@update')->name('update_vessel_sancion_path');

        // SANCIONES - ARTESANALES
        Route::post('/sanciones_buques_artesanales', 'BuquesArtesanalesSancionesController@store')->name('store_buque_artesanal_sancion_path');
        Route::put('/sanciones_buques_artesanales/{sancion}', 'BuquesArtesanalesSancionesController@update')->name('update_buque_artesanal_sancion_path');

        // --- APP ---
        Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
});
